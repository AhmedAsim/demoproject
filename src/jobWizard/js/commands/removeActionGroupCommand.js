import {CommandAbstract} from "../../../common/js/commandAbstract";

export class RemoveActionGroupCommand extends CommandAbstract{

    constructor(actionGroup){
        super();
        this.actionGroup = actionGroup;
        this.index = this.actionGroup.job.jobObj.jobActionGroups.indexOf(this.actionGroup.actionGroupObj);
        this.leftParent = this.actionGroup.html.left.parentNode;
        this.rightParent = this.actionGroup.html.right.parentNode;
        this.centerParent = this.actionGroup.html.center.parentNode;
        this.doo();
        this.addToStack();


    }

    doo(){

        // console.log(" :) ");
        this.leftParent.removeChild(this.actionGroup.html.left);
        this.rightParent.removeChild(this.actionGroup.html.right);
        this.centerParent.removeChild(this.actionGroup.html.center);
        this.actionGroup.job.jobObj.jobActionGroups.splice(this.index , 1);


    }

    undo(){

        this.actionGroup.job.jobObj.jobActionGroups.splice(this.index , 0 , this.actionGroup.actionGroupObj);

        if (this.leftParent.children.length === 0 || this.index > this.leftParent.children.length){

            // console.log("undo");
            this.leftParent.appendChild(this.actionGroup.html.left);
            this.rightParent.appendChild(this.actionGroup.html.right);
            this.centerParent.appendChild(this.actionGroup.html.center);

        }


        else {

            // console.log("undo :)");
            this.leftParent.insertBefore(this.actionGroup.html.left , this.leftParent.children[this.index]);
            this.rightParent.insertBefore(this.actionGroup.html.right , this.rightParent.children[this.index]);
            this.centerParent.insertBefore(this.actionGroup.html.center , this.centerParent.children[this.index]);
        }


    }
}