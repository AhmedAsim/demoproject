import {CommandAbstract} from "../../../common/js/commandAbstract";

export class RemoveActionCommand extends CommandAbstract{
    constructor(action){
        super();
        this.action = action;
        this.index  = this.action.actionGroup.actionGroupObj.jobActions.indexOf(this.action.actionObj);
        this.leftParent = this.action.html.left.parentNode;
        this.rightParent = this.action.html.right.parentNode;
        this.centerParent = this.action.html.center.parentNode;
        this.doo();
        this.addToStack();


    }

    doo(){

        this.leftParent.removeChild(this.action.html.left);
        this.rightParent.removeChild(this.action.html.right);
        this.centerParent.removeChild(this.action.html.center);
        this.action.actionGroup.actionGroupObj.jobActions.splice(this.index,1);
        // console.log(this.action.actionGroup.actionGroupObj.jobActions);

    }

    undo(){
        // console.log("action undo");
        this.action.actionGroup.actionGroupObj.jobActions.splice(this.index, 0, this.action.actionObj);

        if(this.index > this.leftParent.children.length || this.leftParent.children.length === 0)
        {
            this.leftParent.appendChild(this.action.html.left);
            this.rightParent.appendChild(this.action.html.right);
            this.centerParent.appendChild(this.action.html.center);

        }

        else{

            this.leftParent.insertBefore(this.action.html.left , this.leftParent.children[this.index]);
            this.rightParent.insertBefore(this.action.html.right , this.rightParent.children[this.index + 2]);
            this.centerParent.insertBefore(this.action.html.center , this.centerParent.children[this.index]);
        }


    }
}