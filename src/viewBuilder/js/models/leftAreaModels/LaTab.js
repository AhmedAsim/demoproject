import {Utility} from "../../../../common/js/utility";
import {LaFieldGroup} from "./LaFieldGroup";

export class LaTab {
	constructor(tabName, $json, parent) {
		this.$json = $json;
		this.tabName = tabName;
		this.$element = parent.appendChild(Utility.createElement("div", {"class": "la-tab"}));
		this.$header = this.$element.appendChild(Utility.createElement("div", {"class": "la-tab-header"}, this.tabName));
		console.log(this.tabName);
		this.$content = this.$element.appendChild(Utility.createElement("div", {"class": "la-tab-content"}));
		console.log(this.$json);
		this.children = $json["item"]
				.filter(value => Object.keys(value).length)
				.map((value, index) => new LaFieldGroup(value, index, this.$content));
		this.draw();
	}
	
	draw() {
	}
}