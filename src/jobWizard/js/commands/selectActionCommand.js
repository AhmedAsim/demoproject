import {JobWizardAbstract} from "../jobWizardAbstract";
import {Utility} from "../../../common/js/utility";
import {CommandAbstract} from "../../../common/js/commandAbstract";

export class SelectActionCommand extends CommandAbstract{

    constructor(action , addToStack = true){
        super();
        this.action = action;
        this.previousActive = false;
        this.isNew = this.doo();
        if (addToStack){
            this.addToStack(this.isNew);
        }


    }

    doo(){
        if(JobWizardAbstract.activeAction !== this.action.actionGroup){
            if (JobWizardAbstract.activeActionGroup){
                JobWizardAbstract.activeActionGroup._hideHtml();
                Utility.collapse(JobWizardAbstract.activeActionGroup.html.right.querySelector(".action-group-settings"));
            }
            this.action.actionGroup._showHtml();
            JobWizardAbstract.activeActionGroup = this.action.actionGroup;
        }
        if(JobWizardAbstract.activeJob !== this.action.actionGroup.job){
            if (JobWizardAbstract.activeJob){
                JobWizardAbstract.activeJob._hideHtml();
                Utility.collapse(JobWizardAbstract.activeJob.html.right.querySelector(".job-settings"));
            }

            this.action.actionGroup.job._showHtml();
            JobWizardAbstract.activeJob = this.action.actionGroup.job;
        }

        if(JobWizardAbstract.activeAction === false){
            this.previousActive = JobWizardAbstract.activeAction;
            JobWizardAbstract.activeAction = this.action;
            this.action._showHtml();
            Utility.expand(this.action.html.right.querySelector(".action-settings"));
            Utility.collapse(this.action.actionGroup.html.right.querySelector(".action-group-settings"));
            Utility.collapse(this.action.actionGroup.job.html.right.querySelector(".job-settings"));
            return true;
        }

        else if(JobWizardAbstract.activeAction !== this.action){
            this.previousActive = JobWizardAbstract.activeAction;
            JobWizardAbstract.activeAction._hideHtml();
            Utility.collapse(JobWizardAbstract.activeAction.html.right.querySelector(".action-settings"));
            JobWizardAbstract.activeAction = this.action;
            this.action._showHtml();
            Utility.expand(this.action.html.right.querySelector(".action-settings"));
            Utility.collapse(this.action.actionGroup.html.right.querySelector(".action-group-settings"));
            Utility.collapse(this.action.actionGroup.job.html.right.querySelector(".job-settings"));
            return true;
        }

        else{
            this.action._showHtml();
            Utility.expand(this.action.html.right.querySelector(".action-settings"));
            return false;

        }

    }

    undo(){

        this.action._hideHtml();
        Utility.collapse(this.action.html.right.querySelector(".action-settings"));

        JobWizardAbstract.activeAction = this.previousActive;
        JobWizardAbstract.activeActionGroup = this.previousActive.actionGroup;

        if(JobWizardAbstract.activeAction){
            JobWizardAbstract.activeAction._showHtml();
            Utility.expand(JobWizardAbstract.activeAction.html.right.querySelector(".action-settings"));
            Utility.collapse(JobWizardAbstract.activeActionGroup.html.right.querySelector(".action-group-settings"));
            Utility.collapse(JobWizardAbstract.activeJob.html.right.querySelector(".job-settings"));
        }
        else{
            this.action.actionGroup._showHtml();
            this.action.actionGroup.job._showHtml();
            Utility.expand(this.action.actionGroup.html.right.querySelector(".action-group-settings"));
        }
    }
}