import {Utility} from "./utility";

export class Footer {
    constructor(footerContainer, header, leftArea, rightArea) {
        this.header = header;
        this.leftArea = leftArea;
        this.rightArea = rightArea;
        this.html = footerContainer;
        this.draw();
    }

    draw() {
        let self = this;

        //region Footer Left element creation
        let footerLeft = Utility.createElement("div", {"class": "footer-left"});
        self.html.appendChild(footerLeft);
        //endregion

        // region Footer Center element creation
        let footerCenter = Utility.createElement("div", {"class": "footer-center"});
        self.html.appendChild(footerCenter);
        //todo the below lines of code needs json to be created , the are only created for demo
        let a1 = Utility.createElement("a", {"class": "window-link active", "href": "#"}, "Window 1");
        a1.appendChild(Utility.createElement("button", {"class": "close icon-times-B"}));
        let a2 = Utility.createElement("a", {"class": "window-link", "href": "#"}, "Window 2");
        a2.appendChild(Utility.createElement("button", {"class": "close icon-times-B"}));
        footerCenter.appendChild(a1);
        footerCenter.appendChild(a2);
        //endregion

        // region Footer Right element creation
        let footerRight = Utility.createElement("div", {"class": "footer-right"});
        self.html.appendChild(footerRight);

        let buttonsGroup = Utility.createElement("div", {"class": "buttons-group"});
        let addButtonGroup = false;


        if (this.header) {
            let headerToggleButton = Utility.createElement("button", {"class": "icon-bars-B", "id": "toggleHeader"});
            buttonsGroup.appendChild(headerToggleButton);
            headerToggleButton.addEventListener("click", function () {
                this.classList.toggle("active");
                self.header.classList.toggle("collapse");
            });
            addButtonGroup = true;
        }

        if (this.leftArea) {
            let leftToggleButton = Utility.createElement("button", {"class": "icon-cube-B", "id": "toggleLeft"});
            buttonsGroup.appendChild(leftToggleButton);
            leftToggleButton.addEventListener("click", function () {
                this.classList.toggle("active");
                self.leftArea.classList.toggle("collapse");
            });
            addButtonGroup = true;
        }

        if (this.rightArea) {
            let rightToggleButton = Utility.createElement("button", {"class": "icon-cogs-B", "id": "toggleRight"});
            buttonsGroup.appendChild(rightToggleButton);
            rightToggleButton.addEventListener("click", function () {
                this.classList.toggle("active");
                self.rightArea.classList.toggle("collapse");
            });
            addButtonGroup = true;
        }

        if (addButtonGroup) {
            footerRight.appendChild(buttonsGroup);
        }


        let notifications = Utility.createElement("a", {
            "href": "#",
            "class": "notifications icon-bell-B",
            "data-count": "4"
        });
        footerRight.appendChild(notifications);

        let user = Utility.createElement("div", {"class": "user"});
        footerRight.appendChild(user);

        let imageLink = Utility.createElement("a", {"href": "#", "class": "user-image"});
        user.appendChild(imageLink);
        imageLink.appendChild(Utility.createElement("img", {"src": "../images/user.png"}));

        user.appendChild(Utility.createElement("a", {"href": "#", "class": "user-name"}, "Tomas"));

        let logOutWrapper = Utility.createElement("div", {"class": "log-out-wrapper"});
        footerRight.appendChild(logOutWrapper);
        let logOutButton = Utility.createElement("a", {"class": "icon-angle-up-B"});
        logOutWrapper.appendChild(logOutButton);
        let logOutMenuBackground = Utility.createElement("div", {"class": "logout-menu-background"});
        self.html.appendChild(logOutMenuBackground);
        let logOutDropMenu = Utility.createElement("div", {"class": "log-out-menu"});
        logOutWrapper.appendChild(logOutDropMenu);
        let logOutMenuItem = Utility.createElement("a", {"href": "", "target": "_blank"}, "Log Out");
        logOutDropMenu.appendChild(logOutMenuItem);
        let logOutMenuOtherItem = Utility.createElement("a", {"href": "", "target": "_blank"}, "other item");
        logOutDropMenu.appendChild(logOutMenuOtherItem);

        //endregion

        //region Right elements behaviour
        logOutButton.addEventListener("click", function () {
            (logOutDropMenu.classList.contains("active")) ? (logOutDropMenu.classList.remove("active")) : (logOutDropMenu.classList.add("active"));
            (logOutMenuBackground.classList.contains("active")) ? (logOutMenuBackground.classList.remove("active")) : (logOutMenuBackground.classList.add("active"));
            (logOutButton.classList.contains("active")) ? (logOutButton.classList.remove("active")) : (logOutButton.classList.add("active"));
        });
        logOutMenuBackground.addEventListener("click", function () {
            logOutDropMenu.classList.remove("active");
            logOutMenuBackground.classList.remove("active");
            logOutButton.classList.remove("active");
        });
        //endregion
    }


}