import {CommandAbstract} from "../../../common/js/commandAbstract";

export class RemoveJobCommand extends CommandAbstract{
    constructor(job){
        super();
        this.job = job;
        this.index = this.job.parentObj.indexOf(this.job.jobObj);
        this.leftParent = this.job.html.left.parentNode;
        this.rightParent = this.job.html.right.parentNode;
        this.centerParent = this.job.html.center.parentNode;
        this.doo();
        this.addToStack();

    }

    doo(){
        // console.log("job remove command");
        this.leftParent.removeChild(this.job.html.left);
        this.rightParent.removeChild(this.job.html.right);
        this.centerParent.removeChild(this.job.html.center);
        this.job.parentObj.splice(this.index , 1);


    }

    undo(){
        // console.log("undo job remove command");
        this.job.parentObj.splice(this.index , 0, this.job.jobObj);

        if (this.leftParent.children.length === 0 || this.index > this.leftParent.children.length){
            // console.log("undo job remove command-if");

            this.leftParent.appendChild(this.job.html.left);
            this.rightParent.appendChild(this.job.html.right);
            this.centerParent.appendChild(this.job.html.center);
        }


        else {
            // console.log("undo job remove command-else");
            this.leftParent.insertBefore(this.job.html.left , this.leftParent.children[this.index]);
            this.rightParent.insertBefore(this.job.html.right , this.rightParent.children[this.index +1]);
            this.centerParent.insertBefore(this.job.html.center , this.centerParent.children[this.index]);




        }

    }
}