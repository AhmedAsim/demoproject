import {Utility} from "../../../common/js/utility";

export class OrderByRow {
    constructor(customObjects , parentTable) {
        this.customObjects = customObjects;
        this.parentTable = parentTable;
        this.html = this.draw();
    }

    draw() {
        let self = this;

        //region Elements Creation
        let orderByRow = Utility.createElement("tr"); //ToDo is it the same object as custom objects
        let orderBySelectCell = Utility.createElement("td");
        orderByRow.appendChild(orderBySelectCell);
        let deleteRowButton = Utility.createElement("div",{"class":"delete-row icon-times-B"});
        orderBySelectCell.appendChild(deleteRowButton);
        let orderBySelectWrapper = Utility.createElement("div",{"class":"order-by-wrapper"});
        orderBySelectCell.appendChild(orderBySelectWrapper);
        let orderBySelect = Utility.createElement("select");
        orderBySelectWrapper.appendChild(orderBySelect);
        let dummyOrderByOption = Utility.createElement("option",{"disabled":"", "selected":"", "value":""},"-- select option --");
        orderBySelect.appendChild(dummyOrderByOption);
        for (let orderByItem of self.customObjects.item){
            let orderByOption = Utility.createElement("option",{"value":orderByItem.name}, orderByItem.name);
            orderBySelect.appendChild(orderByOption);
        } //TODo OrderBy object?!!!

        let ascDescSelectCell = Utility.createElement("td");
        orderByRow.appendChild(ascDescSelectCell);
        let ascDescSelectWrapper = Utility.createElement("div",{"class":"asc-desc-wrapper"});
        ascDescSelectCell.appendChild(ascDescSelectWrapper);
        let ascDescSelect = Utility.createElement("select");
        ascDescSelectWrapper.appendChild(ascDescSelect);
        let dummyAscDescOption = Utility.createElement("option",{"disabled":"", "selected":"", "value":""},"-- select option --");
        ascDescSelect.appendChild(dummyAscDescOption);
        orderBySelect.addEventListener("change", function () {
            ascDescSelect.innerHTML = "";
            ascDescSelect.appendChild(dummyAscDescOption);
            let activeFieldsItems = [];
            for (let customItem of self.customObjects.item){
                if (customItem.name === orderBySelect.value){
                    activeFieldsItems = customItem.fields;
                }
            }
            if (activeFieldsItems.length > 0){
                for (let fieldItem of activeFieldsItems){
                    let fieldItemOption = Utility.createElement("option",{"value":fieldItem.name},fieldItem.name);
                    ascDescSelect.appendChild(fieldItemOption);
                }
            }
        });

        //endregion

        //region Behaviour
        deleteRowButton.addEventListener("click" , function () {
            self.parentTable.table.removeChild(self.html.orderByRow);
        });
        //endregion

        return {orderByRow : orderByRow}
    }
}