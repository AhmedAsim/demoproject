import {CriteriaConditionRow} from "../TablesRowTemp/criteriaConditionRow";

export class AddCriteriaConditionRow {
    constructor(customObjects , parentTable){
        this.parentTable = parentTable;
        this.newCriteriaConditionRow = new CriteriaConditionRow(customObjects , parentTable);
    }
    doo(){
        this.parentTable.table.insertBefore(this.newCriteriaConditionRow.html.criteriaConditionRow , this.parentTable.table.lastElementChild);
        this.parentTable.count += 1;
        this.newCriteriaConditionRow.html.criteriaConditionRow.querySelector(".seq-no-wrapper").innerHTML = this.parentTable.count;
    }

    undo(){
        this.parentTable.table.removeChild(this.newCriteriaConditionRow.html.criteriaConditionRow);
        this.parentTable.count -= 1;
        let count = this.parentTable.count;
        let seq = this.parentTable.table.querySelectorAll(".seq-no-wrapper");
        for (let i = seq.length - 1 ; i >= 0 ; i--) {
            seq[i].innerHTML = count;
            count -=1;
        }
    }
}