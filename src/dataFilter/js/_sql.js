@model
DataFilterWizardModel
@{
    Layout = "_LayoutDesigner";
}

@section
Styles
{
<
    link
    href = "~/css/data-filter-designer.min.css"
    rel = "stylesheet" / >
}

<
div
id = "centerContent" >
    < h3 > criteria_conditions < /h3>
    < table
cellspacing = "0"
cellpadding = "0"
id = "criteria-table" >
    < thead >
    < tr >
    < th
style = "width: 50px; min-width: 50px" >
    #
    < /th>
    < th
style = "width: 250px;" >
    < label
for= "select-custom-object-1" data - translation = "custom_objects" >
    custom_objects
    < /label>
    < /th>
    < th
style = "width: 250px;" >
    < label
for= "select-field-1" data - translation = "field" >
    field
    < /label>
    < /th>
    < th
style = "width: 150px;" >
    < label
for= "select-field-1" data - translation = "criteria_operator" >
    criteria_operator
    < /label>
    < /th>
    < th
style = "width: 150px;" >
    < label
for= "select-value-1" data - translation = "value" >
    value
    < /label>
    < /th>
    < th >
    < a
href = "javascript:;"
class
= "add-row" > add_link < /a>
    < /th>
    < /tr>
    < /thead>
    < /table>

    < input
id = "exporession"
class
= "expression"
type = "text"
name = "exporession" / >
    < h3 > join_fields < /h3>
    < table
cellspacing = "0"
cellpadding = "0"
id = "join-table" >
    < thead >
    < tr >
    < th
style = "width: 150px;" >
    < label
for= "select-join-object-left-1" data - translation = "custom_objects" >
    custom_objects
    < /label>
    < /th>
    < th
style = "width: 150px;" >
    < label
for= "select-join-field-left-1" data - translation = "field" >
    field
    < /label>
    < /th>
    < th
style = "width: 150px;" >
    < label
for= "select-join-object-right-1" data - translation = "custom_objects" >
    custom_objects
    < /label>
    < /th>
    < th
style = "width: 150px;" >
    < label
for= "select-join-field-right-1" data - translation = "field" >
    field
    < /label>
    < /th>
    < th >
    < a
href = "javascript:;"
class
= "add-row" > add_link < /a>
    < /th>
    < /tr>
    < /thead>
    < /table>

    < h3 > orderby_fields < /h3>
    < table
cellspacing = "0"
cellpadding = "0"
id = "orderby-table" >
    < thead >
    < tr >
    < th >
    < label
for= "select-orderby-object-1" data - translation = "order_by" >
    order_by
    < /label>
    < /th>
    < th >
    < label
for= "select-orderby-field-1" data - translation = "asc_desc" >
    asc_desc
    < /label>
    < /th>
    < th >
    < a
href = "javascript:;"
class
= "add-row" > add_link < /a>
    < /th>
    < /tr>
    < /thead>
    < /table>

    < h3 > select_fields < /h3>
    < table
cellspacing = "0"
cellpadding = "0"
id = "projection-table" >
    < thead >
    < tr >
    < th >
    < label
for= "select-projection-custom-object-1" data - translation = "custom_objects" >
    custom_objects
    < /label>
    < /th>
    < th >
    < label
for= "select-projection-field-1" data - translation = "field" >
    field
    < /label>
    < /th>
    < th >
    < label
data - translation = "veritcal_alignment" >
    veritcal_alignment
    < /label>
    < /th>
    < th >
    < label
data - translation = "horizontal_alignment" >
    horizontal_alignment
    < /label>
    < /th>
    < th >
    < label
data - translation = "width" >
    width
    < /label>
    < /th>
    < th >
    < a
href = "javascript:;"
class
= "add-row" > add_link < /a>
    < /th>
    < /tr>
    < /thead>
    < /table>
    < /div>

    < div
id = "rightPanel" >
    < div >
    < h3 > Settings < /h3>
    < div >
    < label
for= "filter-name" data - translation = "filter_name" > filter_name < /label><br / >
    < input
id = "filter-name"
name = "Name" / >
    < /div>
    < div >
    < label
for= "filter-description" data - translation = "filter_description" > filter_description < /label><br / >
    < input
id = "filter-description"
name = "Description" / >
    < /div>
    < div >
    < input
id = "filter-isActive"
type = "checkbox"
name = "IsActive" / >
    < label
for= "filter-isActive" data - translation = "is_active" > is_active < /label>
    < /div>
    < div >
    < label
for= "filter-sql" data - translation = "sql" > sql < /label><br / >
    < textarea
id = "filter-sql"
rows = "4"
name = "SQL" > < /textarea>
    < /div>
    < input
type = "button"
value = "save" / >
    < /div>

    < /div>

@section
Scripts
{
<
    script
    src = "~/js/data-filter-designer.min.js" > < /script>
        < script >


        doGet('/Menu/Data', function (req) {
            if (199 < req.status && req.status < 300) {
                window.menuJson = JSON.parse(req.responseText);
                window.commonView = new window.CommonView(window.menuJson, "Data Filter Designer", {
                    containers: {
                        content: document.getElementsByClassName('main')[0],
                    },
                });


                class TableDynamic {

                    constructor(tableId, rowTemplate, onAddRow, onRemovedRow, onChange) {
                        this.rowNo = 0;
                        this.tableId = tableId;
                        this.rowTemplate = rowTemplate;
                        this.onAddRow = onAddRow || function () {
                        };
                        this.onRemovedRow = onRemovedRow || function () {
                        };
                        this.onChange = onChange || function () {
                        };
                        document.getElementById(tableId).querySelector(".add-row").addEventListener('click',
                            (e) => {
                                this.addRow();
                            });
                    }

                    getSelectHtml(name, rowNo, url, parent, selectFirst) {
                        //  data-url="${url}"
                        // ${parent ? 'data-parent="' + parent + '"' : 'data-set-first-item="True"'}
                        return `<div class="select single-select" data-type="text"
                                         id="select-${name}-${rowNo}" name="select-${name}-${rowNo}"
                                         data-name="${name}-${rowNo}" data-name-text="${name}-${rowNo}-text"
                                         ${selectFirst ? 'data-set-first-item="True"' : ''}
                                         data-value="" data-text=""></div>`;
                    }

                    getElement(selector) {
                        return document.querySelector(selector || '#' + this.tableId);
                    }

                    addRow() {
                        ++this.rowNo;
                        let template = this.rowTemplate(this);
                        let row = document.createElement('tr');
                        row.setAttribute('id', this.tableId + '-row-' + this.rowNo);
                        row.classList.add('row');
                        row.innerHTML = template;
                        row.dataset.rowNumber = this.rowNo;
                        getElement(this.tableId).appendChild(row);
                        row.querySelector(".delete").addEventListener('click',
                            e => {
                                this.removeRow(row.dataset.rowNumber);
                            });
                        row.querySelectorAll(".changeable").forEach(e => {
                            e.addEventListener('change',
                                evt => {
                                    this.onChange();
                                });
                        })
                        this.updateSeqNo();
                        setTimeout(() => {
                                this.onAddRow(row, this);
                                this.onChange();
                            },
                            300);
                    }

                    removeRow(num) {
                        let element = getElement(this.tableId + '-row-' + num);
                        element.parentNode.removeChild(element);
                        this.updateSeqNo();
                        this.onChange();
                    }

                    updateSeqNo() {
                        getElement(this.tableId).querySelectorAll("tr td .seq-no").forEach((item, i) => {
                            item.innerHTML = i + 1;
                        });
                    }

                }

                let centerContent = getElement('centerContent');
                let rightPanel = getElement('rightPanel');

                let content = window.commonView.$content;

                let theMain = document.getElementsByClassName('main')[0];
                //theMain.appendChild(content.html);
                document.getElementsByClassName('center')[0].appendChild(centerContent);
                document.getElementsByClassName('right')[0].appendChild(rightPanel);

                getElement("exporession").addEventListener("change", generateSql);

                function initializeTables() {

                    let criteriaTable = new TableDynamic('criteria-table',
                        (self) => {
                            let rowNo = self.rowNo;
                            let template = `
                                                        <td> <span class="seq-no"></span> </td>
                                                        <td> ${self.getSelectHtml('custom-object', rowNo, '/CustomObject/SelectData')} </td>
                                                        <td> ${self.getSelectHtml('field', rowNo, '/Field/SelectData/<id>', 'custom-object-' + rowNo)} </td>
                                                        <td> ${self.getSelectHtml('criteria-operator', rowNo, '', '/field-' + rowNo, true)} </td>
                                                        <!-- <td> ${self.getSelectHtml('criteria-value', rowNo, '')} </td> -->
                                                        <td>  <input class="changeable" name="criteria-value-${rowNo}" id="criteria-value-${rowNo}" </td>
                                                        <td>
                                                            <a class="delete" href="javascript:;">delete</a>
                                                        </td>
                                                `;
                            return template;
                        },
                        (row, self) => {
                            row.querySelectorAll('.select.single-select').forEach(s => {
                                if (s.getAttribute('id').indexOf('custom-object') > 0) {

                                    let element = getSelect('custom-object-' + row.dataset.rowNumber);
                                    initSingleSelect(element, customObjects.map(c => {
                                        return {value: c.uid, text: c.name};
                                    }));
                                    onSelectChange('custom-object-' + row.dataset.rowNumber,
                                        function (v, n) {
                                            let fieldSelect = getSelect('field-' + row.dataset.rowNumber);
                                            //clearOnSelectChange(fieldSelect);
                                            let customObj = customObjects.find(s => s.uid == v);
                                            let values = customObj && customObj.fields && customObj.fields.map(c => {
                                                return {value: c.uid, text: c.name};
                                            });
                                            initSingleSelect(fieldSelect, values);
                                            self.onChange();
                                            onSelectChange('field-' + row.dataset.rowNumber,
                                                (fieldValue, n) => {

                                                    let fieldsTypeUid = customObj.fields &&
                                                        customObj.fields.find(f => f.uid == fieldValue).fieldTypeUid;
                                                    let element = getSelect('criteria-operator-' + row.dataset.rowNumber);
                                                    element.dataset.url = '/CriteriaOperator/SelectData/' + fieldsTypeUid;
                                                    onSelectChange('criteria-operator-' + row.dataset.rowNumber,
                                                        (op, n) => {
                                                            self.onChange();
                                                        });
                                                    getElement("criteria-value-" + row.dataset.rowNumber).addEventListener("change", self.onChange);
                                                    refreshSelect(element);
                                                    self.onChange();
                                                });
                                        });
                                }
                            });
                            // update where expression
                            let count = self.getElement().querySelectorAll(".row").length;
                            if (count > 1) {
                                getElement("exporession").value += " AND {" + count + "}";
                            } else {
                                getElement("exporession").value += "{" + count + "}";
                            }
                        }, undefined, generateSql);
                    criteriaTable.addRow();


                    let joinTable = new TableDynamic('join-table',
                        (self) => {
                            let rowNo = self.rowNo;
                            let template = `
                                                        <td> ${self.getSelectHtml('join-object-left', rowNo, '/CustomObject/SelectData')} </td>
                                                        <td> ${self.getSelectHtml('join-field-left', rowNo, '/Field/SelectData/<id>', 'join-object-left-' + rowNo)} </td>
                                                        <td> ${self.getSelectHtml('join-object-right', rowNo, '/CustomObject/SelectData')} </td>
                                                        <td> ${self.getSelectHtml('join-field-right', rowNo, '/Field/SelectData/<id>', 'join-object-right-' + rowNo)} </td>
                                                        <td>
                                                            <a class="delete" href="javascript:;">delete</a>
                                                        </td>
                                                `;
                            return template;
                        },
                        (row, self) => {
                            row.querySelectorAll('.select.single-select').forEach(s => {

                                if (s.getAttribute('id').indexOf('join-object-left') > 0) {

                                    let element = getSelect('join-object-left-' + row.dataset.rowNumber);
                                    initSingleSelect(element, customObjects.map(c => {
                                        return {value: c.uid, text: c.name};
                                    }));
                                    self.onChange();
                                    onSelectChange('join-object-left-' + row.dataset.rowNumber,
                                        function (v, n) {
                                            let fieldSelect = getSelect('join-field-left-' + row.dataset.rowNumber);
                                            //clearOnSelectChange(fieldSelect);
                                            let customObj = customObjects.find(s => s.uid == v);
                                            let values = customObj && customObj.fields && customObj.fields.map(c => {
                                                return {value: c.uid, text: c.name};
                                            });
                                            initSingleSelect(fieldSelect, values);
                                            self.onChange();
                                            onSelectChange('join-field-left-' + row.dataset.rowNumber, self.onChange);
                                        });
                                }

                                if (s.getAttribute('id').indexOf('join-object-right') > 0) {

                                    let element = getSelect('join-object-right-' + row.dataset.rowNumber);
                                    initSingleSelect(element, customObjects.map(c => {
                                        return {value: c.uid, text: c.name};
                                    }));
                                    self.onChange();
                                    onSelectChange('join-object-right-' + row.dataset.rowNumber,
                                        function (v, n) {
                                            let fieldSelect = getSelect('join-field-right-' + row.dataset.rowNumber);
                                            //clearOnSelectChange(fieldSelect);
                                            let customObj = customObjects.find(s => s.uid == v);
                                            let values = customObj && customObj.fields && customObj.fields.map(c => {
                                                return {value: c.uid, text: c.name};
                                            });
                                            initSingleSelect(fieldSelect, values);
                                            self.onChange();
                                            onSelectChange('join-field-right-' + row.dataset.rowNumber, self.onChange);
                                        });
                                }
                            });
                        }, undefined, generateSql);
                    joinTable.addRow();


                    let orderByTable = new TableDynamic('orderby-table',
                        (self) => {
                            let rowNo = self.rowNo;
                            let template = `
                                                        <td> ${self.getSelectHtml('orderby-object', rowNo, '/CustomObject/SelectData')} </td>
                                                        <td> ${self.getSelectHtml('orderby-field', rowNo, '/Field/SelectData/<id>')} </td>
                                                        <td>
                                                            <a class="delete" href="javascript:;">delete</a>
                                                        </td>
                                                `;
                            return template;
                        },
                        (row, self) => {
                            row.querySelectorAll('.select.single-select').forEach(s => {

                                if (s.getAttribute('id').indexOf('orderby-object') > 0) {

                                    let element = getSelect('orderby-object-' + row.dataset.rowNumber);
                                    initSingleSelect(element, customObjects.map(c => {
                                        return {value: c.uid, text: c.name};
                                    }));
                                    self.onChange();
                                    onSelectChange('orderby-object-' + row.dataset.rowNumber,
                                        function (v, n) {
                                            let fieldSelect = getSelect('orderby-field-' + row.dataset.rowNumber);
                                            //clearOnSelectChange(fieldSelect);
                                            let customObj = customObjects.find(s => s.uid == v);
                                            let values = customObj && customObj.fields && customObj.fields.map(c => {
                                                return {value: c.uid, text: c.name};
                                            });
                                            initSingleSelect(fieldSelect, values);
                                            self.onChange();
                                            onSelectChange('orderby-field-' + row.dataset.rowNumber, self.onChange);
                                        });
                                }
                            });
                        }, undefined, generateSql);
                    orderByTable.addRow();


                    let projectionTable = new TableDynamic('projection-table',
                        (self) => {
                            let rowNo = self.rowNo;
                            let template = `
                                                        <td> ${self.getSelectHtml('projection-custom-object',
                                rowNo,
                                '/CustomObject/SelectData')} </td>
                                                        <td> ${self.getSelectHtml('projection-field', rowNo, '/Field/SelectData/<id>')} </td>
                                                        <td>
                                                               <label><input type="radio" class="changeable" name="projection-vertical-alighnemnt-${rowNo}" id="projection-vertical-alighnemnt-${rowNo}" />Left</label>
                                                               <label><input type="radio" class="changeable" name="projection-vertical-alighnemnt-${rowNo}" id="projection-vertical-alighnemnt-${rowNo}" />Center</label>
                                                               <label><input type="radio" class="changeable" name="projection-vertical-alighnemnt-${rowNo}" id="projection-vertical-alighnemnt-${rowNo}" />Right</label>
                                                               <label><input type="radio" class="changeable" name="projection-vertical-alighnemnt-${rowNo}" id="projection-vertical-alighnemnt-${rowNo}" />Justify</label>
                                                        </td>
                                                        <td>
                                                               <label><input type="radio" class="changeable" name="projection-horisontal-alighnemnt-${rowNo}" id="projection-horisontal-alighnemnt-${rowNo}" />Top</label>
                                                               <label><input type="radio" class="changeable" name="projection-horisontal-alighnemnt-${rowNo}" id="projection-horisontal-alighnemnt-${rowNo}" />Middle</label>
                                                               <label><input type="radio" class="changeable" name="projection-horisontal-alighnemnt-${rowNo}" id="projection-horisontal-alighnemnt-${rowNo}" />Bottom</label>
                                                        </td>
                                                        <td>
                                                               <input type="number" class="changeable" name="projection-width-${rowNo}" id="projection-width-${rowNo}" />
                                                        </td>
                                                        <td>
                                                            <a class="delete" href="javascript:;">delete</a>
                                                        </td>
                                                `;
                            return template;
                        },
                        (row, self) => {
                            row.querySelectorAll('.select.single-select').forEach(s => {
                                if (s.getAttribute('id').indexOf('projection-custom-object') > 0) {

                                    let element = getSelect('projection-custom-object-' + row.dataset.rowNumber);
                                    initSingleSelect(element, customObjects.map(c => {
                                        return {value: c.uid, text: c.name};
                                    }));
                                    self.onChange();
                                    onSelectChange('projection-custom-object-' + row.dataset.rowNumber,
                                        function (v, n) {
                                            let fieldSelect = getSelect('projection-field-' + row.dataset.rowNumber);
                                            //clearOnSelectChange(fieldSelect);
                                            let customObj = customObjects.find(s => s.uid == v);
                                            let values = customObj && customObj.fields && customObj.fields.map(c => {
                                                return {value: c.uid, text: c.name};
                                            });
                                            initSingleSelect(fieldSelect, values);
                                            self.onChange();
                                            onSelectChange('projection-field-' + row.dataset.rowNumber, self.onChange);
                                        });
                                }

                            });
                        }, undefined, generateSql);
                    projectionTable.addRow();
                    return {
                        projectionTable,
                        criteriaTable,
                        orderByTable,
                        joinTable,
                    };
                }

                function generateSql() {
                    try {

                        let expressionInput = getElement("exporession");
                        let whereExp = expressionInput.value;

                        let sqlInput = getElement("filter-sql");

                        let from = '';
                        let joins = [];
                        designerTables.joinTable.getElement()
                            .querySelectorAll('.row')
                            .forEach(e => {
                                let objL = e.querySelector('[data-name^="join-object-left"]').dataset.text;
                                let colL = e.querySelector('[data-name^="join-field-left"]').dataset.text;


                                let objR = e.querySelector('[data-name^="join-object-right"]').dataset.text;
                                let colR = e.querySelector('[data-name^="join-field-right"]').dataset.text;
                                if (!from) {
                                    from = objL;
                                }
                                joins.push(`INNER JOIN  "${objL}" ON "${objL}"."${colL}" = "${objR}"."${colR}"`);
                            });

                        let projections = [];
                        designerTables.projectionTable.getElement()
                            .querySelectorAll('.row')
                            .forEach(e => {
                                let obj = e.querySelector('[data-name^="projection-custom-object"]').dataset.text;
                                let col = e.querySelector('[data-name^="projection-field"]').dataset.text;
                                if (!from) {
                                    from = obj;
                                }
                                projections.push(`"${obj}"."${col}"`);
                            });

                        let criterias = [];
                        designerTables.criteriaTable.getElement()
                            .querySelectorAll('.row')
                            .forEach(e => {
                                let obj = e.querySelector('[data-name^="custom-object"]').dataset.text;
                                let col = e.querySelector('[data-name^="field"]').dataset.text;
                                let op = e.querySelector('[data-name^="criteria-operator"]').dataset.text;
                                let val = e.querySelector('[name^="criteria-value"]').value;
                                if (!from) {
                                    from = obj;
                                }
                                criterias.push(`"${obj}"."${col}" ${op} "${val}" `);
                            });

                        let orderBy = [];
                        designerTables.orderByTable.getElement()
                            .querySelectorAll('.row')
                            .forEach(e => {
                                let obj = e.querySelector('[data-name^="orderby-object-"]').dataset.text;
                                let col = e.querySelector('[data-name^="orderby-field-"]').dataset.text;

                                orderBy.push(`"${obj}"."${col}"`);
                            });

                        let query = 'SELECT';
                        if (projections.length > 0) {
                            query += ' ' + projections.join(',') + ' ';
                        } else {
                            query += ' * ';
                        }
                        query += ' FROM "' + from + '" \n';
                        if (joins.length > 0) {
                            query += joins.join("\n");
                        }
                        query += ' \n';
                        if (!whereExp) {
                            whereExp = criterias.join(" AND ");
                        } else {
                            criterias.forEach((item, i) => {
                                whereExp =
                                    whereExp.replace('{' + (i + 1) + '}', item); //+ (i == criterias.length - 1 ? '' : ' AND ');
                            });
                            whereExp = whereExp.replace(new RegExp("AND " + '$', ""));
                        }
                        query += "WHERE " + whereExp + ' \n';
                        if (orderBy.length > 0) {
                            query += 'ORDER BY ' + orderBy.join(",");
                        }
                        sqlInput.innerHTML = query;
                    }
                    catch (e) {
                        console.error(e);
                    }
                }


                var customObjects = [];
                var designerTables = {};
                doGet('/CustomObject/DataWithFieldsLookup',
                    function (req) {
                        if (199 < req.status && req.status < 300) {
                            customObjects = JSON.parse(req.responseText).item;
                            console.log(customObjects);
                            designerTables = initializeTables();
                        }
                    }
                );

            }
        });


<
    /script>
}
