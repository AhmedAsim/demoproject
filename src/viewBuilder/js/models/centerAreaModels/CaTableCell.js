import {Utility} from "../../../../common/js/utility";
import {CaField} from "./CaField";
import {LaField} from "../leftAreaModels/LaField";
import {CaTable} from "./CaTable";

export class CaTableCell {
	constructor($parent, parentClass) {
		this.$parent = $parent;
		// this.rowIndex = rowIndex;
		// this.columnIndex = columnIndex;
		this.parentClass = parentClass;
		
		this.fieldClass = null;
		
		this.selected = false;
		
		this.dropAllowed = true;
		
		this.mergedTo = null;
		
		this.mergedCellsSet = new Set();
		
		this.rowSpan = 1;
		this.colSpan = 1;
		
		this.$caElement = null;
		
		this.createCaElement();
		
		this.$parent.appendChild(this.$caElement);
		
		this.setDragDropFunctionality();
		
		this.setDragSelectFunctionality();
		
	}
	
	createCaElement() {
		this.$caElement = Utility.createElement("TD", {rowspan: 1, colspan: 1, draggable: false});
		
		
	}
	
	setDragDropFunctionality() {
		
		this.$caElement.addEventListener("dragstart", (e) => {
			if (!e.target.closest(".ca-field")) {
				e.preventDefault();
			}
		});
		this.$caElement.addEventListener("dragenter", (e) => {
			e.preventDefault();
			if (!this.dropAllowed) {
				e.dataTransfer.dropEffect = "none";
			}
			
		});
		this.$caElement.addEventListener("dragover", (e) => {
			e.preventDefault();
			if (!this.dropAllowed) {
				e.dataTransfer.dropEffect = "none";
			}
			
		});
		this.$caElement.addEventListener("dragleave", (e) => {
			e.preventDefault();
			if (!this.dropAllowed) {
				e.dataTransfer.dropEffect = "none";
				
			}
		});
		this.$caElement.addEventListener("drop", (e) => {
			e.preventDefault();
			if (!this.dropAllowed || !ViewBuilder.draggedField) {
				return;
			}
			
			
			let draggedField = ViewBuilder.draggedField;
			
			let draggedJson = draggedField.$json;
			
			if (draggedField instanceof LaField) {
				
				
				let newFieldJson = {
					fieldUid: draggedJson.uid||'',
					formulaTypeName: draggedJson.formulaTypeName||'',
					formulaName: draggedJson.formulaName||'',
					label: draggedJson.name||'',
					description: draggedJson.description||'',
					visibilityFormula: draggedJson.visibilityFormula||'',
					viewItemType:draggedJson.viewItemType||'',
					rowIndex: this.rowIndex,
					columnIndex: this.columnIndex,
					rowSpan: this.rowSpan,
					colSpan: this.colSpan,
					isCollapsed: draggedJson.isCollapsed||false,
					fieldName: draggedJson.fieldName||'',
					customObjectUid: draggedJson.customObjectUid||'',
					customObjectName: draggedJson.customObjectName||'',
					
					templateUid: draggedJson.templateUid||'',
					templateName: draggedJson.templateName||'',
					
					fxFieldTypename: draggedJson.fxFieldTypename||'',
					fxFieldTypeUid: draggedJson.fxFieldTypeUid||'',
					fxFieldFormula: draggedJson.fxFieldFormula||'',
					fxFieldName: draggedJson.fxFieldName||'',
				};
				
				draggedField.hide();
				
				this.parentClass.$json.viewItems.push(newFieldJson);
				this.parentClass.caTableFieldsArray.push(new CaField(newFieldJson, this.parentClass, draggedField));
				
			} else if (draggedField instanceof CaField) {
				
				draggedJson.rowIndex = this.rowIndex;
				draggedJson.columnIndex = this.columnIndex;
				draggedJson.rowSpan = this.rowSpan;
				draggedJson.colSpan = this.colSpan;
				draggedField.parentCellClass.dropAllowed = true;
				draggedField.$raSetting.querySelector('input[name="row_index"]').value=this.rowIndex;
				draggedField.$raSetting.querySelector('input[name="column_index"]').value=this.columnIndex;
				this.$caElement.appendChild(draggedField.$caElement);
				
				draggedField.parentCellClass = this;
				draggedField.parentTableClass = this.parentClass;
				
				this.dropAllowed = false;
				
			}
			
			
		});
		
		
	}
	
	setColSpan(value) {
		this.$caElement.setAttribute("colspan", value);
		this.colSpan = value;
	}
	
	setRowSpan(value) {
		this.$caElement.setAttribute("rowspan", value);
		this.rowSpan = value;
	}
	
	hide() {
		this.$caElement.classList.add("d-none");
	}
	
	show() {
		this.$caElement.classList.remove("d-none");
	}
	
	toggleVisibility() {
		this.$caElement.classList.toggle("d-none");
	}
	
	setDragSelectFunctionality() {
		// this.$caElement.addEventListener("click", (event) => );
		
		this.$caElement.addEventListener("mouseenter", (event) => {
			if (!CaTable.marqueeSelectMode) {}
			// this.parentClass.updateMarqueeSelectedCells(this.rowIndex);
			
		});
		this.$caElement.addEventListener("mouseleave", (event) => {
			if (!CaTable.marqueeSelectMode) {}
			
		});
		
		
	}
	
	get rowIndex(){
		return this.$parent.rowIndex-1;
	}
	
	get columnIndex(){
		return this.$caElement.cellIndex-1;
	}
	
	
	select() {
		this.$caElement.classList.add("selected");
		this.selected = true;
	}
	
	deSelect() {
		this.$caElement.classList.remove("selected");
		this.selected = false;
	}
}