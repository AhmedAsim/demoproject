import '../../fonts/fa-solid-900.woff'
import "../css/main.scss";
import {Utility} from "../../common/js/utility";
import {Job} from "./job";
import {CommonView} from "../../common/js/commonView";
import {CommandAbstract} from "../../common/js/commandAbstract";
import {AddJobWithSelection} from "./commands/addJobWithSelection";

class JobWizard {

    constructor(jobsObj, headerJson, appName, options={}) {
        
        let commonView = new CommonView(headerJson, appName,  options);
        
        this.jobsObj = jobsObj.item;
        this.ra = commonView.rightArea;
        this.la = commonView.leftArea;
        this.da = commonView.centerArea.appendChild(Utility.createElement('div',{'class':'position-wrapper'}));
        this.undoBtn = "";
        this.redoBtn = "";
        this.jobsArray = [];
        for (let i = 0; i < this.jobsObj.length; i++) {

            let newJob = new Job(this.jobsObj, this.jobsObj[i]);

            this.jobsArray.push(newJob.html);

        }
      
        this.$style = Utility.createElement('style',{}, ".canvas .jobs {grid-template-columns: repeat(3, auto);}.canvas .action-groups {grid-template-columns: repeat(3, auto);}.canvas .actions {grid-template-columns: repeat(2, auto);}");
        
        document.getElementsByTagName('head')[0].appendChild(this.$style);
        
        this.draw();
    
        //region undo and redo
        document.getElementsByTagName("body")[0].addEventListener("keydown", function (e) {
            const zKey = 90;
        
            if ((e.ctrlKey || e.metaKey) && e.keyCode === zKey) {
                e.preventDefault();
                CommandAbstract.undo();
            }
        
        });
        document.getElementsByTagName("body")[0].addEventListener("keydown", function (e) {
            const yKey = 89;
        
            if ((e.ctrlKey || e.metaKey) && e.keyCode === yKey) {
                e.preventDefault();
                CommandAbstract.redo();
            }
        
        
        });
    
        this.redoBtn.addEventListener("click", CommandAbstract.redo);
        this.undoBtn.addEventListener("click", CommandAbstract.undo);
        //endregion undo and redo
      
    
    }

    draw() {

        let self = this;

        // region Right Area
        let raHeader = Utility.createElement("div", {"class": "ra-header"});
        let raName = Utility.createElement("div", {"class": "ra-name"}, "Settings");
        let raTools = Utility.createElement("div", {"class": "ra-tools"});
        let raFilter = Utility.createElement("input", {"type": "text", "placeholder": "filter..."});
        let raFilterClearBtn = Utility.createElement("div", {"class": "filter-clear-btn icon-times-B"});

        self.undoBtn = Utility.createElement("button", {"class": "undo icon-undo-B"});
        self.redoBtn = Utility.createElement("button", {"class": "redo icon-redo-B"});
        let saveButton = Utility.createElement("button", {"class": "save"}, "Save");
        raHeader.appendChild(raName);
        raHeader.appendChild(self.undoBtn);
        raHeader.appendChild(self.redoBtn);
        raHeader.appendChild(saveButton);
        raTools.appendChild(raFilter);
        raTools.appendChild(raFilterClearBtn);
        self.ra.appendChild(raHeader);
        self.ra.appendChild(raTools);

        let raContainer = Utility.createElement("div", {"class": "ra-container"});
        let raLayoutHeader = Utility.createElement("div", {"class": "layout-header"}, "Designer Settings");
        let raLayoutSettings = Utility.createElement("div", {"class": "layout-settings"});
        let jobGrid = Utility.createElement("input", {"type": "number", "id": "job-grid"});
        let actionGroupGrid = Utility.createElement("input", {"type": "number", "id": "ag-grid"});
        let actionGrid = Utility.createElement("input", {"type": "number", "id": "action-grid"});
        raLayoutSettings.appendChild(Utility.createElement("label", {"for": "job-grid"}, "Job Columns No."));
        raLayoutSettings.appendChild(jobGrid);
        raLayoutSettings.appendChild(Utility.createElement("label", {"for": "ag-grid"}, "Action Groups Columns No."));
        raLayoutSettings.appendChild(actionGroupGrid);
        raLayoutSettings.appendChild(Utility.createElement("label", {"for": "action-grid"}, "Action Columns No."));
        raLayoutSettings.appendChild(actionGrid);
        raContainer.appendChild(raLayoutHeader);
        raContainer.appendChild(raLayoutSettings);

        for (let i = 0; i < self.jobsArray.length; i++) {
            raContainer.appendChild(self.jobsArray[i].right);
        }
        // endregion Right Area

        // region Drawing Area
        let daHeader = Utility.createElement("div", {"class": "da-header"});
        let daAddJobButton = Utility.createElement("div", {"class": "add-job icon-plus-B"});
        daAddJobButton.appendChild(Utility.createElement("div", {}, "add job"));
        let daAppHeader = Utility.createElement("div", {"class": "app-name"}, "Job Wizard");
        daHeader.appendChild(daAppHeader);
        daHeader.appendChild(daAddJobButton);
        self.da.appendChild(daHeader);
        let daContainer = Utility.createElement("div", {"class": "da-container"});
        let daCanvas = Utility.createElement("div", {"class": "canvas"});
        daContainer.appendChild(daCanvas);
        let jobs = Utility.createElement("jobs", {"class": "jobs"});
        daCanvas.appendChild(jobs);
        for (let i = 0; i < self.jobsArray.length; i++) {
            jobs.appendChild(self.jobsArray[i].center);
        }
        // endregion Drawing Area

        // region Left Area
        // let laHeader = Utility.createElement("div", {"class":"la-header"},"Job Wizard");
        let laTools = Utility.createElement("div", {"class": "la-tools"});
        let laFilter = Utility.createElement("input", {"type": "text", "placeholder": "filter..."});
        let laFilterClearBtn = Utility.createElement("div", {"class": "filter-clear-btn icon-times-B"});
        // let addJobButton = Utility.createElement("div",{"class":"add-job"},"Add New Job");
        laTools.appendChild(laFilter);
        laTools.appendChild(laFilterClearBtn);
        // laTools.appendChild(addJobButton);
        // self.la.appendChild(laHeader);
        self.la.appendChild(laTools);
        let laContainer = Utility.createElement("div", {"class": "la-container"});
        for (let i = 0; i < self.jobsArray.length; i++) {
            laContainer.appendChild(self.jobsArray[i].left);
        }
        // endregion Left Area

        // region Right Area Behaviour
        raLayoutHeader.addEventListener("click", function () {
            Utility.slideToggle(raLayoutSettings);
        });
        jobGrid.addEventListener("keyup", function () {
            if (this.value > 0)
                self.$style.sheet.cssRules[0].style.gridTemplateColumns = "repeat(" + parseInt(this.value) + ",auto)";
        });
        actionGroupGrid.addEventListener("keyup", function () {
            if (this.value > 0)
                self.$style.sheet.cssRules[1].style.gridTemplateColumns = "repeat(" + parseInt(this.value) + ",auto)";
        });
        actionGrid.addEventListener("keyup", function () {
            if (this.value > 0)
                self.$style.sheet.cssRules[2].style.gridTemplateColumns = "repeat(" + parseInt(this.value) + ",auto)";
        });

        //region Filter Behaviour
        let raOldMatched = [];
        raFilter.addEventListener("keyup", function () {
            let raItemsToFilter = raContainer.querySelectorAll(".job-wrapper.active label"); //needs some modification
            let value = raFilter.value.trim().toLowerCase();
            if (value) {
                raContainer.classList.add("filtering-mode");
                raFilterClearBtn.classList.add("active");
                for (let matchedElement of raOldMatched) {
                    matchedElement.classList.remove("matched");
                }
                for (let item of raItemsToFilter) {
                    if (item.textContent.toLowerCase().includes(value)) {
                        item.classList.add("matched");
                        raOldMatched.push(item);
                        item.nextSibling.classList.add("matched");
                        raOldMatched.push(item.nextSibling);
                        let parent = item.closest(".job-wrapper.active");
                        parent.classList.add("matched");
                        raOldMatched.push(parent);
                    }
                }
            } else {
                raContainer.classList.remove("filtering-mode");
                raFilterClearBtn.classList.remove("active");
                for (let matchedElement of raOldMatched) {
                    matchedElement.classList.remove("matched");
                }
            }

        });

        raFilterClearBtn.addEventListener("click", function () {
            raFilterClearBtn.classList.remove("active");
            raContainer.classList.remove("filtering-mode");
            raFilter.value = "";
            for (let matchedElement of raOldMatched) {
                matchedElement.classList.remove("matched");
            }
        });


        //endregion
        // endregion Right Area Behaviour


        //region Left Area Behaviour

        //region Filter Behaviour
        let laOldMatched = [];
        laFilter.addEventListener("keyup", function () {
            let jobsToFilter = laContainer.querySelectorAll(".la-job");
            let actionGroupsToFilter = laContainer.querySelectorAll(".action-group");
            let actionsToFilter = laContainer.querySelectorAll(".action");
            let value = this.value.trim().toLowerCase();
            if (value) {
                laContainer.classList.add("filtering-mode");
                laFilterClearBtn.classList.add("active");
                for (let matchedElement of laOldMatched) {
                    matchedElement.classList.remove("matched");
                }

                for (let job of jobsToFilter) {
                    if (job.getElementsByTagName("h3")[0].textContent.toLowerCase().includes(value)) {
                        job.classList.add("matched");
                        laOldMatched.push(job);
                    }
                }
                for (let actionGroup of actionGroupsToFilter) {
                    if (actionGroup.getElementsByTagName("h4")[0].textContent.toLowerCase().includes(value)) {
                        actionGroup.classList.add("matched");
                        laOldMatched.push(actionGroup);
                    }
                }
                for (let action of actionsToFilter) {
                    if (action.getElementsByTagName("h5")[0].textContent.toLowerCase().includes(value)) {
                        action.classList.add("matched");
                        laOldMatched.push(action);
                    }
                }

            } else {
                laContainer.classList.remove("filtering-mode");
                laFilterClearBtn.classList.remove("active");
                for (let matchedElement of laOldMatched) {
                    matchedElement.classList.remove("matched");
                }

            }


        });

        laFilterClearBtn.addEventListener("click", () => {
            laContainer.classList.remove("filtering-mode");
            laFilterClearBtn.classList.remove("active");
            laFilter.value = "";
            for (let matchedElement of laOldMatched) {
                matchedElement.classList.remove("matched");
            }

        });


        //endregion


        //endregion Left Area Behaviour

        //region Draw Area Behaviour

        daAddJobButton.addEventListener("click", function (event) {
            event.stopPropagation();
            new AddJobWithSelection(self);


        });

        //endregion


        // region Tarshe2
        self.ra.appendChild(raContainer);
        self.da.appendChild(daContainer);
        self.la.appendChild(laContainer);
        // endregion

        return {left: laContainer, right: raContainer, center: daCanvas};
    }

}

window.JobWizard = JobWizard;

//
// window.onload = function () {
//     new JobWizard(window.jobsJSON, window.headerJSON, 'Job Wizard');
// };
//
