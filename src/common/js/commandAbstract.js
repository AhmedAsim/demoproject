export class CommandAbstract {


    addToStack(push = true ){

        //remove next commands at entering of new command
        CommandAbstract.commandStack.splice(CommandAbstract.pointer + 1 , CommandAbstract.maxStackSize);

        //adding our new command
        if(push)
        {
            CommandAbstract.commandStack.push(this);
        }

        //limiting stack size
        CommandAbstract.commandStack = CommandAbstract.commandStack.slice(-CommandAbstract.maxStackSize);

        //updating pointer of command stack
        CommandAbstract.pointer = CommandAbstract.commandStack.length -1;
    }

    static undo(){
        if (CommandAbstract.pointer > -1){
            CommandAbstract.commandStack[CommandAbstract.pointer].undo();
            CommandAbstract.pointer -= 1;
        }
    }

    static redo(){
        if (CommandAbstract.pointer < CommandAbstract.commandStack.length - 1){
           CommandAbstract.pointer += 1 ;
           CommandAbstract.commandStack[CommandAbstract.pointer].doo();

        }

    }


}

CommandAbstract.commandStack = [];
CommandAbstract.pointer = -1;
CommandAbstract.maxStackSize = 50;