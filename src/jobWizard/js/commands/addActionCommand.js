import {Action} from "../action";
import {ActionGroup} from "../actionGroup";
import {CommandAbstract} from "../../../common/js/commandAbstract";


export class AddActionCommand  extends CommandAbstract{

    constructor(actionGroup){
        super();
        this.newAction = new Action(actionGroup , ActionGroup.getDefaultAction());
        this.leftParent = this.newAction.actionGroup.html.left.children[1];
        this.rightParent = this.newAction.actionGroup.html.right;
        this.centerParent = this.newAction.actionGroup.html.center.children[1];
        this.doo();
        this.addToStack();
    }

    doo(){
        // console.log(this.newAction);
        //we need to check if we create a new action or not !!!
        this.newAction.actionGroup.actionGroupObj.jobActions.push(this.newAction.actionObj);
        this.leftParent.appendChild(this.newAction.html.left);
        this.rightParent.appendChild(this.newAction.html.right);
        this.centerParent.appendChild(this.newAction.html.center);
    }

    undo(){
        this.newAction.actionGroup.actionGroupObj.jobActions.splice(-1 , 1);
        this.leftParent.removeChild(this.newAction.html.left);
        this.rightParent.removeChild(this.newAction.html.right);
        this.centerParent.removeChild(this.newAction.html.center)


    }
}