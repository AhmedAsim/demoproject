import {Utility} from "../../../common/js/utility";

export class CriteriaConditionRow {
    constructor(customObjects , parentTable){
        this.customObjects = customObjects;
        this.parentTable = parentTable;
        this.html = this.draw();
    }

    draw(){
        let self = this;

        //region Elements Creation
        let criteriaConditionRow = Utility.createElement("tr"); //ToDo conditions json or array to decide number of table rows????!!

        let seqNumber = Utility.createElement("td",{}); //ToDo seq number ??!!
        criteriaConditionRow.appendChild(seqNumber);
        let seqNoWrapper = Utility.createElement("div" , {"class":"seq-no-wrapper"} , 1);
        seqNumber.appendChild(seqNoWrapper);
        let deleteRowButton = Utility.createElement("div",{"class":"delete-row icon-times-B"});
        seqNumber.appendChild(deleteRowButton);
        let customObjectSelectCell = Utility.createElement("td");
        criteriaConditionRow.appendChild(customObjectSelectCell);
        let customObjectSelectWrapper = Utility.createElement("div",{"class":"custom-object-select-wrapper"});
        customObjectSelectCell.appendChild(customObjectSelectWrapper);
        let customObjectSelect = Utility.createElement("select");
        customObjectSelectWrapper.appendChild(customObjectSelect);
        let dummyCustomOption = Utility.createElement("option",{"disabled":"", "selected":"", "value":""},"-- select option --");
        customObjectSelect.appendChild(dummyCustomOption);
        for (let customItem of self.customObjects.item){
            let customItemOption = Utility.createElement("option",{"value":customItem.name}, customItem.name);
            customObjectSelect.appendChild(customItemOption);
        }

        let fieldSelectCell = Utility.createElement("td");
        criteriaConditionRow.appendChild(fieldSelectCell);
        let fieldSelectWrapper = Utility.createElement("div",{"class":"field-select-wrapper"});
        fieldSelectCell.appendChild(fieldSelectWrapper);
        let fieldSelect = Utility.createElement("select");
        fieldSelectWrapper.appendChild(fieldSelect);
        let dummyFieldOption = Utility.createElement("option",{"disabled":"", "selected":"", "value":""},"-- select option --");
        fieldSelect.appendChild(dummyFieldOption);
        customObjectSelect.addEventListener("change", function () {
            fieldSelect.innerHTML = "";
            fieldSelect.appendChild(dummyFieldOption);
            let activeFieldsItems = [];
            for (let customItem of self.customObjects.item){
                if (customItem.name === customObjectSelect.value){
                    activeFieldsItems = customItem.fields;
                }
            }
            if (activeFieldsItems.length > 0){
                for (let fieldItem of activeFieldsItems){
                    let fieldItemOption = Utility.createElement("option",{"value":fieldItem.name},fieldItem.name);
                    fieldSelect.appendChild(fieldItemOption);
                }
            }
        });


        let criteriaOperatorCell = Utility.createElement("td");
        criteriaConditionRow.appendChild(criteriaOperatorCell);
        let criteriaOperatorWrapper = Utility.createElement("div",{"class":"operator-select-wrapper"});
        criteriaOperatorCell.appendChild(criteriaOperatorWrapper);
        let criteriaOperatorSelect = Utility.createElement("select");
        criteriaOperatorWrapper.appendChild(criteriaOperatorSelect);
        let dummyOperatorOption = Utility.createElement("option",{"disabled":"", "selected":"", "value":""},"-- select option --");
        criteriaOperatorSelect.appendChild(dummyOperatorOption);
        let criteriaOperatorArray = ["contain","not contain","start with","end with"];
        fieldSelect.addEventListener("change", function () {
            if (fieldSelect.value) {
                criteriaOperatorSelect.innerHTML = "";
                criteriaOperatorSelect.appendChild(dummyOperatorOption);
                for (let operatorItem of criteriaOperatorArray) {
                    let criteriaOperatorOption = Utility.createElement("option",{"value":operatorItem},operatorItem);
                    criteriaOperatorSelect.appendChild(criteriaOperatorOption);
                }
            }
        });



        let criteriaValueCell = Utility.createElement("td");
        criteriaConditionRow.appendChild(criteriaValueCell);
        let valueWrapper = Utility.createElement("div",{"class":"value-wrapper"});
        criteriaValueCell.appendChild(valueWrapper);
        let valueInput = Utility.createElement("input",{"type":"text"});
        valueWrapper.appendChild(valueInput);

        //endregion


        //region Behaviour
        deleteRowButton.addEventListener("click" , function () {
            self.parentTable.table.removeChild(self.html.criteriaConditionRow);
            self.parentTable.count -= 1;
            let count = self.parentTable.count;
            let seq = self.parentTable.table.querySelectorAll(".seq-no-wrapper");
            for (let i = seq.length - 1 ; i >= 0 ; i--) {
                seq[i].innerHTML = count;
                count -=1;
            }
        });
        //endregion

        return {criteriaConditionRow : criteriaConditionRow}
    }
}