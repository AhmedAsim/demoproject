import {JobWizardAbstract} from "../jobWizardAbstract";
import {Utility} from "../../../common/js/utility";
import {CommandAbstract} from "../../../common/js/commandAbstract";



export class SelectJobCommand extends CommandAbstract{

    constructor(job , addToStack = true){
        super();
        this.job = job;
        this.previousActive = false;
        this.previousActiveAG = JobWizardAbstract.activeActionGroup;
        this.previousActiveAction = JobWizardAbstract.activeAction;
        this.isNew = this.doo();
        if (addToStack){
            this.addToStack(this.isNew);
        }
    }

    doo(){

        if(JobWizardAbstract.activeActionGroup){
            JobWizardAbstract.activeActionGroup._hideHtml();
            Utility.collapse(JobWizardAbstract.activeActionGroup.html.right.querySelector(".action-group-settings"));
            JobWizardAbstract.activeActionGroup = false;
        }
        if(JobWizardAbstract.activeAction){
            JobWizardAbstract.activeAction._hideHtml();
            Utility.collapse(JobWizardAbstract.activeAction.html.right.querySelector(".action-settings"));
            JobWizardAbstract.activeAction = false;
        }

        if(JobWizardAbstract.activeJob === false){


            this.previousActive = JobWizardAbstract.activeJob;
            JobWizardAbstract.activeJob = this.job;
            this.job._showHtml();
            Utility.expand(this.job.html.right.querySelector(".job-settings"));
            return true;
        }
        else if(JobWizardAbstract.activeJob !== this.job){

            this.previousActive = JobWizardAbstract.activeJob;
            JobWizardAbstract.activeJob._hideHtml();
            Utility.collapse(JobWizardAbstract.activeJob.html.right.querySelector(".job-settings"));
            JobWizardAbstract.activeJob = this.job;
            this.job._showHtml();
            Utility.expand(this.job.html.right.querySelector(".job-settings"));
            return true;
        }
        else{
            this.job._showHtml();
            Utility.expand(this.job.html.right.querySelector(".job-settings"));
            return false;
        }
    }

    undo(){

        this.job._hideHtml();
        Utility.collapse(this.job.html.right.querySelector(".job-settings"));
        JobWizardAbstract.activeJob = this.previousActive;

        if (JobWizardAbstract.activeJob){
            JobWizardAbstract.activeJob._showHtml();
            Utility.expand(JobWizardAbstract.activeJob.html.right.querySelector(".job-settings"));
        if (this.previousActiveAction) {
            this.previousActiveAction._showHtml();
            Utility.expand(this.previousActiveAction.html.right.querySelector(".action-settings"));
            this.previousActiveAG._showHtml();
            Utility.collapse(this.previousActiveAG.html.right.querySelector(".action-group-settings"));

            Utility.collapse(this.previousActive.html.right.querySelector(".job-settings"));
        }
        else if (this.previousActiveAG){
            this.previousActiveAG._showHtml();
            Utility.expand(this.previousActiveAG.html.right.querySelector(".action-group-settings"));
            Utility.collapse(this.previousActive.html.right.querySelector(".job-settings"));
        }

        }
    }
}
