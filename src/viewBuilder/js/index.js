import "../../fonts/fa-solid-900.woff";
import "../css/index.scss";
import {Utility} from "../../common/js/utility";
import {CommonView} from "../../common/js/commonView";
import {CommandAbstract} from "../../common/js/commandAbstract";
import {LaFieldGroup} from "./models/leftAreaModels/LaFieldGroup";
import {CaSection} from "./models/centerAreaModels/CaSection";
import {ViewSettingsFields} from "./models/rightAreaModels/view-settings-fields";
// import {AddJobWithSelection} from "./commands/addJobWithSelection";

window.ViewBuilder = class ViewBuilder {
	
	constructor(appName, $laCustomFieldsJson, $laTemplatesJson, $caJson, headerJson, options = {}) {
		
		let commonView = new CommonView(headerJson, appName, options);
		this.$laCustomFieldsJson = $laCustomFieldsJson;
		this.$laTemplatesJson = $laTemplatesJson;
		this.$caJson = $caJson;
		ViewBuilder.$caJson = $caJson;
		this.$laFixedFieldsJson = {
			"isOk": true,
			"messages": [],
			"item": [
				{
					"organizationUid": "00000000-0000-0000-0000-000000000000",
					"organizationName": null,
					"description": "",
					"pluralName": "Orders",
					"formulaName": "ORDER",
					"displayIndex": 0,
					"fieldCount": 5,
					"defaultTabSectionColumnCount": 5,
					"isActive": true,
					"fields": [
						{
							"fieldTypeUid": "fe56be92-7f53-4104-8aae-03d644a79cd9",
							"fieldTypeName": "Formula",
							"formulaName": "Text",
							"description": "",
							"infoText": "",
							"defaultValueFormula": "",
							"visibilityFormula": "",
							"uid": "cd286da07a778479",
							"name": "No",
							"createdBy": "00000000-0000-0000-0000-000000000000",
							"createdByName": null,
							"createdAt": "0001-01-01T00:00:00",
							"updatedBy": null,
							"updatedByName": null,
							"updatedAt": null,
						},
						{
							"fieldTypeUid": "d454eb63-3d44-4f5d-81fa-0dfe9db075cf",
							"fieldTypeName": "LookupList",
							"formulaName": "Checkbox",
							"description": "",
							"infoText": "",
							"defaultValueFormula": "",
							"visibilityFormula": "",
							"uid": "b68f5e14-ff43-4ead-9ecc-3esd5fde",
							"name": "Product",
							"createdBy": "00000000-0000-0000-0000-000000000000",
							"createdByName": null,
							"createdAt": "0001-01-01T00:00:00",
							"updatedBy": null,
							"updatedByName": null,
							"updatedAt": null,
						},
						{
							"fieldTypeUid": "d454eb63-3d44-4f5d-81fa-0dfe9db075cf",
							"fieldTypeName": "LookupList",
							"formulaName": "Datetime",
							"description": "",
							"infoText": "",
							"defaultValueFormula": "",
							"visibilityFormula": "",
							"uid": "b68f5e14-ff43-4ead-9ewr8c83a5fde",
							"name": "Product",
							"createdBy": "00000000-0000-0000-0000-000000000000",
							"createdByName": null,
							"createdAt": "0001-01-01T00:00:00",
							"updatedBy": null,
							"updatedByName": null,
							"updatedAt": null,
						},
						{
							"fieldTypeUid": "d454eb63-3d44-4f5d-81fa-0dfe9db075cf",
							"fieldTypeName": "LookupList",
							"formulaName": "select",
							"description": "",
							"infoText": "",
							"defaultValueFormula": "",
							"visibilityFormula": "",
							"uid": "b68f5e14-ff4iye08c83a5fde",
							"name": "Product",
							"createdBy": "00000000-0000-0000-0000-000000000000",
							"createdByName": null,
							"createdAt": "0001-01-01T00:00:00",
							"updatedBy": null,
							"updatedByName": null,
							"updatedAt": null,
						},
						{
							"fieldTypeUid": "d454eb63-3d44-4f5d-81fa-0dfe9db075cf",
							"fieldTypeName": "LookupList",
							"formulaName": "Button",
							"description": "",
							"infoText": "",
							"defaultValueFormula": "",
							"visibilityFormula": "",
							"uid": "b68f5e14-ff43-4ead-9ecc-3khl3a5fde",
							"name": "Product",
							"createdBy": "00000000-0000-0000-0000-000000000000",
							"createdByName": null,
							"createdAt": "0001-01-01T00:00:00",
							"updatedBy": null,
							"updatedByName": null,
							"updatedAt": null,
						},
					],
					"uid": "0ef33293-d581-4de6-b302-13ade8529444",
					"name": "Order",
					"createdBy": "00000000-0000-0000-0000-000000000000",
					"createdByName": null,
					"createdAt": "2019-06-26T18:08:11.379734",
					"updatedBy": null,
					"updatedByName": null,
					"updatedAt": "2019-06-26T18:13:13.644263",
				},
			],
		};
		
		this.$laCustomFieldsJson.item = this.$laCustomFieldsJson.item.filter(value => Object.keys(value).length);
		this.$laTemplatesJson.item = this.$laTemplatesJson.item.filter(value => Object.keys(value).length);
		this.$laFixedFieldsJson.item = this.$laFixedFieldsJson.item.filter(value => Object.keys(value).length);
		
		
		this.$ra = commonView.rightArea;
		this.$la = commonView.leftArea;
		this.$ca = commonView.centerArea;
		this.$undoBtn = "";
		this.$redoBtn = "";
		this.SectionsArray = [];
		this.appName = appName;
		
		this.$laCustomFieldsJson = $laCustomFieldsJson;
		
		
		//todo check if you need this style
		this.$style = Utility.createElement("style", {}, ".canvas .sections {grid-template-columns: repeat(1, auto);}.canvas .tabs {grid-template-columns: repeat(1, auto);}.canvas .tables {grid-template-columns: repeat(1, auto);}");
		
		document.getElementsByTagName("head")[0].appendChild(this.$style);
		
		this.$containers = this.draw();
		
		this._generateLaTabs();
		
		this._generateLaTabsContent();
		
		//todo use the new key api
		
		//region undo and redo
		document.getElementsByTagName("body")[0].addEventListener("keydown", function (e) {
			const zKey = 90;
			
			if ((e.ctrlKey || e.metaKey) && e.keyCode === zKey) {
				e.preventDefault();
				CommandAbstract.undo();
			}
			
		});
		document.getElementsByTagName("body")[0].addEventListener("keydown", function (e) {
			const yKey = 89;
			
			if ((e.ctrlKey || e.metaKey) && e.keyCode === yKey) {
				e.preventDefault();
				CommandAbstract.redo();
			}
			
			
		});
		
		this.$redoBtn.addEventListener("click", CommandAbstract.redo);
		this.$undoBtn.addEventListener("click", CommandAbstract.undo);
		//endregion undo and redo
		
		
	}
	
	
	draw() {
		
		let self = this;
		
		// region Right Area
		let $raHeader = Utility.createElement("div", {"class": "ra-header"});
		let $raName = Utility.createElement("div", {"class": "ra-name"}, "Settings");
		let $raTools = Utility.createElement("div", {"class": "ra-tools"});
		let $raFilter = Utility.createElement("input", {"type": "text", "placeholder": "filter..."});
		let $raFilterClearBtn = Utility.createElement("div", {"class": "filter-clear-btn icon-times-B"});
		
		self.$undoBtn = Utility.createElement("button", {"class": "undo icon-undo-B"});
		self.$redoBtn = Utility.createElement("button", {"class": "redo icon-redo-B"});
		let $saveButton = Utility.createElement("button", {"class": "save"}, "Save");
		$raHeader.appendChild($raName);
		$raHeader.appendChild(self.$undoBtn);
		$raHeader.appendChild(self.$redoBtn);
		$raHeader.appendChild($saveButton);
		$raTools.appendChild($raFilter);
		$raTools.appendChild($raFilterClearBtn);
		self.$ra.appendChild($raHeader);
		self.$ra.appendChild($raTools);
		
		let $raContainer = Utility.createElement("div", {"class": "ra-container"});
		let $raLayoutHeader = Utility.createElement("div", {"class": "layout-header"}, "Designer Settings");
		let $raLayoutSettings = Utility.createElement("div", {"class": "layout-settings"});
		
		let $viewName = Utility.createElement("input", {"type": "text", "id": "view-name"});
		let $viewDescription = Utility.createElement("input", {"type": "textarea", "id": "view-description"});
		
		
		let $sectionsGrid = Utility.createElement("input", {type: "number", min: 1, value: 1, id: "sections-grid"});
		let $tabsGrid = Utility.createElement("input", {type: "number", min: 1, value: 1, id: "tabs-grid"});
		let $tablesGrid = Utility.createElement("input", {type: "number", min: 1, value: 1, id: "tables-grid"});
		
		$raLayoutSettings.appendChild(Utility.createElement("label", {"for": "view-name"}, "Name"));
		$raLayoutSettings.appendChild($viewName);
		$raLayoutSettings.appendChild(Utility.createElement("label", {"for": "view-description"}, "Description"));
		$raLayoutSettings.appendChild($viewDescription);
		
		$raLayoutSettings.appendChild(Utility.createElement("label", {"for": "sections-grid"}, "Sections Columns No."));
		$raLayoutSettings.appendChild($sectionsGrid);
		$raLayoutSettings.appendChild(Utility.createElement("label", {"for": "tabs-grid"}, "Tabs Columns No."));
		$raLayoutSettings.appendChild($tabsGrid);
		$raLayoutSettings.appendChild(Utility.createElement("label", {"for": "tables-grid"}, "Tables Columns No."));
		$raLayoutSettings.appendChild($tablesGrid);
		$raContainer.appendChild($raLayoutHeader);
		$raContainer.appendChild($raLayoutSettings);
		this.$raSettingWrapper = $raContainer.appendChild(Utility.createElement("DIV", {}));
		
		
		$viewName.addEventListener("input", function () {
			self.$ca.querySelector(".app-name").innerHTML = this.value;
		});
		
		
		// for (let i = 0; i < self.jobsArray.length; i++) {
		//   $raContainer.appendChild(self.jobsArray[i].right);
		// }
		// endregion Right Area
		
		// region Center Area
		
		
		let $caWrapper = Utility.createElement("div", {"class": "position-wrapper"});
		let $caHeader = Utility.createElement("div", {"class": "ca-header"});
		let $caAddSectionButton = Utility.createElement("div", {"class": "add-section"}, "Add Section");
		$caAddSectionButton.prepend(Utility.createElement("div", {class: " icon-plus-B"}));
		ViewBuilder.$mergeButton = Utility.createElement("div", {"class": "merge disabled icon-object-group-B"});
		ViewBuilder.$unmergeButton = Utility.createElement("div", {"class": "merge disabled icon-object-ungroup-B"});
		let $caViewHeaderName = Utility.createElement("div", {"class": "app-name"}, self.$caJson.name);
		$caHeader.appendChild($caViewHeaderName);
		$caHeader.appendChild(ViewBuilder.$unmergeButton);
		$caHeader.appendChild(ViewBuilder.$mergeButton);
		$caHeader.appendChild($caAddSectionButton);
		self.$ca.appendChild($caWrapper);
		let $caContainer = Utility.createElement("div", {"class": "ca-container"});
		
		$caWrapper.appendChild($caHeader);
		$caWrapper.appendChild($caContainer);
		
		let $caCanvas = Utility.createElement("div", {"class": "canvas"});
		$caContainer.appendChild($caCanvas);
		$caCanvas.innerHTML = "";
		
		let $sections = $caCanvas.appendChild(Utility.createElement("DIV", {"class": "sections"}));
		
		self.caSectionsArray = [];
		
		for (let i = 0; i < self.$caJson.sections.length; i++) {
			self.caSectionsArray.push(new CaSection(self.$caJson.sections[i], $sections, self));
		}
		
		$caAddSectionButton.addEventListener("click", () => {
			self.caSectionsArray.push(new CaSection(CaSection.baseJson, $sections, self));
		});
		
		
		// this.caSectionsArray = self.$caJson.sections
		//   .filter(value => Object.keys(value).length)
		//   .map(($sectionJson, index)=> new CaSection($sectionJson, $sections, index));
		//
		//
		
		// endregion Center Area
		
		//region Left Area
		
		let laHeader = Utility.createElement("div", {"class": "la-header"});
		laHeader.appendChild(Utility.createElement("div", {"class": "la-name"}, self.appName));
		let laTools = Utility.createElement("div", {"class": "la-tools"});
		let laFilter = Utility.createElement("input", {"type": "text", "placeholder": "filter..."});
		let laFilterClearBtn = Utility.createElement("div", {"class": "filter-clear-btn icon-times-B"});
		// let addJobButton = Utility.createElement("div",{"class":"add-job"},"Add New Job");
		laTools.appendChild(laFilter);
		laTools.appendChild(laFilterClearBtn);
		// laTools.appendChild(addJobButton);
		self.$la.appendChild(laHeader);
		self.$la.appendChild(laTools);
		let $laContainer = Utility.createElement("div", {"class": "la-container"});
		
		// let laTabsContainer = $laContainer.appendChild(Utility.createElement("div", {"class": "la-tabs-container"}));
		
		// let laCustomFieldsHeader = $laContainer.appendChild(Utility.createElement("div", {"class": "la-tab-header la-custom-fields"},'Custom Fields'));
		// laCustomFieldsHeader.prepend(Utility.createElement("div", {"class": "icon icon-plus-B"}));
		// laCustomFieldsHeader.appendChild(Utility.createElement("div", {"class": "number"},'2'));
		//
		// let laCustomFieldsBody = $laContainer.appendChild(Utility.createElement("div", {"class": "la-tab-body"},));
		//
		//
		// let laTemplates = $laContainer.appendChild(Utility.createElement("div", {"class": "la-tab-header la-templates"}, 'Templates'));
		//
		// let laFixedFields = $laContainer.appendChild(Utility.createElement("div", {"class": "la-tab-header la-fixed-fields"}, 'Fixed Fields'));
		// laTemplates.appendChild(Utility.createElement("div", {"class": "number"},'3'));
		//
		// for (let i = 0; i < self.jobsArray.length; i++) {
		//   $laContainer.appendChild(self.jobsArray[i].left);
		// }
		// endregion Left Area
		
		// region Right Area Behaviour
		$raLayoutHeader.addEventListener("click", () => {
			Utility.slideToggle($raLayoutSettings);
		});
		$sectionsGrid.addEventListener("keyup", function () {
			if (this.value > 0)
				self.$style.sheet.cssRules[0].style.gridTemplateColumns = "repeat(" + parseInt(this.value) + ",auto)";
		});
		$tabsGrid.addEventListener("keyup", function () {
			if (this.value > 0)
				self.$style.sheet.cssRules[1].style.gridTemplateColumns = "repeat(" + parseInt(this.value) + ",auto)";
		});
		$tablesGrid.addEventListener("keyup", function () {
			if (this.value > 0)
				self.$style.sheet.cssRules[2].style.gridTemplateColumns = "repeat(" + parseInt(this.value) + ",auto)";
		});
		
		//region Filter Behaviour
		let raOldMatched = [];
		$raFilter.addEventListener("keyup", function () {
			let raItemsToFilter = $raContainer.querySelectorAll(".job-wrapper.active label"); //needs some modification
			let value = $raFilter.value.trim().toLowerCase();
			if (value) {
				$raContainer.classList.add("filtering-mode");
				$raFilterClearBtn.classList.add("active");
				for (let matchedElement of raOldMatched) {
					matchedElement.classList.remove("matched");
				}
				for (let item of raItemsToFilter) {
					if (item.textContent.toLowerCase().includes(value)) {
						item.classList.add("matched");
						raOldMatched.push(item);
						item.nextSibling.classList.add("matched");
						raOldMatched.push(item.nextSibling);
						let parent = item.closest(".job-wrapper.active");
						parent.classList.add("matched");
						raOldMatched.push(parent);
					}
				}
			} else {
				$raContainer.classList.remove("filtering-mode");
				$raFilterClearBtn.classList.remove("active");
				for (let matchedElement of raOldMatched) {
					matchedElement.classList.remove("matched");
				}
			}
			
		});
		
		$raFilterClearBtn.addEventListener("click", () => {
			$raFilterClearBtn.classList.remove("active");
			$raContainer.classList.remove("filtering-mode");
			$raFilter.value = "";
			for (let matchedElement of raOldMatched) {
				matchedElement.classList.remove("matched");
			}
		});
		
		
		//endregion
		// endregion Right Area Behaviour
		
		
		//region Left Area Behaviour
		
		//region Filter Behaviour
		let laOldMatched = [];
		laFilter.addEventListener("keyup", function () {
			let jobsToFilter = $laContainer.querySelectorAll(".la-job");
			let actionGroupsToFilter = $laContainer.querySelectorAll(".action-group");
			let actionsToFilter = $laContainer.querySelectorAll(".action");
			let value = this.value.trim().toLowerCase();
			if (value) {
				$laContainer.classList.add("filtering-mode");
				laFilterClearBtn.classList.add("active");
				for (let matchedElement of laOldMatched) {
					matchedElement.classList.remove("matched");
				}
				
				for (let job of jobsToFilter) {
					if (job.getElementsByTagName("h3")[0].textContent.toLowerCase().includes(value)) {
						job.classList.add("matched");
						laOldMatched.push(job);
					}
				}
				for (let actionGroup of actionGroupsToFilter) {
					if (actionGroup.getElementsByTagName("h4")[0].textContent.toLowerCase().includes(value)) {
						actionGroup.classList.add("matched");
						laOldMatched.push(actionGroup);
					}
				}
				for (let action of actionsToFilter) {
					if (action.getElementsByTagName("h5")[0].textContent.toLowerCase().includes(value)) {
						action.classList.add("matched");
						laOldMatched.push(action);
					}
				}
				
			} else {
				$laContainer.classList.remove("filtering-mode");
				laFilterClearBtn.classList.remove("active");
				for (let matchedElement of laOldMatched) {
					matchedElement.classList.remove("matched");
				}
				
			}
			
			
		});
		
		laFilterClearBtn.addEventListener("click", () => {
			$laContainer.classList.remove("filtering-mode");
			laFilterClearBtn.classList.remove("active");
			laFilter.value = "";
			for (let matchedElement of laOldMatched) {
				matchedElement.classList.remove("matched");
			}
			
		});
		
		
		//endregion
		
		
		//endregion Left Area Behaviour
		
		// //region Draw Area Behaviour
		//
		// $caAddSectionButton.addEventListener("click", function (event) {
		//   event.stopPropagation();
		//   // new AddJobWithSelection(self);
		// });
		//
		// //endregion
		
		
		// region Tarshe2
		self.$ra.appendChild($raContainer);
		self.$la.appendChild($laContainer);
		
		
		// endregion
		
		return {
			left: $laContainer,
			right: $raContainer,
			center: $caCanvas,
		};
	}
	
	_generateLaTabs() {
		let self = this;
		this.laTabsList = [];
		this.laActiveTabIndex = 0;
		this.$laTabs = this.$containers.left.appendChild(Utility.createElement("div", {"class": "la-tabs"}));
		this.$laTabs.style.width = this.$containers.left.scrollHeight + "px";
		
		// Creating the Tabs header add append them to laTabsList
		this.laTabsList.push(
				this.$laCustomFieldsTab = this.$laTabs.appendChild(Utility.createElement("div", {"class": "la-tab la-tab-custom-fields active", "data-index": 0})),
				this.$laTemplatesTab = this.$laTabs.appendChild(Utility.createElement("div", {"class": "la-tab la-tab-templates", "data-index": 1})),
				
				this.$laFixedFieldsTab = this.$laTabs.appendChild(Utility.createElement("div", {"class": "la-tab la-tab-fixed-fields", "data-index": 2})),
		);
		
		this.$laCustomFieldsTab.appendChild(Utility.createElement("a", {"class": "icon icon-plus-B", target: "_blank", "href": "/CustomObject/New"}));
		this.$laCustomFieldsTab.appendChild(Utility.createElement("span", {"class": "name"}, "Custom Fields"));
		this.$laCustomFieldsTab.appendChild(Utility.createElement("div", {"class": "number"}, this.$laCustomFieldsJson["item"].length));
		this.$laTemplatesTab.appendChild(Utility.createElement("a", {"class": "icon icon-plus-B", target: "_blank", "href": "/Template/New"}));
		this.$laTemplatesTab.appendChild(Utility.createElement("span", {"class": "name"}, "Templates"));
		this.$laTemplatesTab.appendChild(Utility.createElement("div", {"class": "number"}, this.$laTemplatesJson["item"][0]["fieldCount"]));
		this.$laFixedFieldsTab.appendChild(Utility.createElement("span", {"class": "name"}, "FX Fields"));
		this.$laFixedFieldsTab.appendChild(Utility.createElement("div", {"class": "number"}, this.$laFixedFieldsJson["item"][0]["fieldCount"]));
		
		// Add click lister to the tabs
		this.$laTabs.on("click", ".la-tab", function () {
			if (self.laActiveTabIndex === +this.dataset.index) {return;}
			
			self.laTabsList[self.laActiveTabIndex].classList.remove("active");
			Utility.slideUp(self.laTabsContentList[self.laActiveTabIndex]);
			
			self.laActiveTabIndex = +this.dataset.index;
			
			this.classList.add("active");
			Utility.slideDown(self.laTabsContentList[self.laActiveTabIndex]);
		});
		
	}
	
	_generateLaTabsContent() {
		this.laTabsContentList = [];
		
		this.laTabsContentList.push(
				this.$laCustomFieldsTabContent = this.$containers.left.appendChild(Utility.createElement("div", {
					"class": "la-tab-content la-tab-content-custom-fields active",
					"data-index": 0,
				})),
				this.$laTemplatesTabContent = this.$containers.left.appendChild(Utility.createElement("div", {"class": "la-tab-content la-tab-content-templates", "data-index": 1})),
				this.$laFixedFieldsTabContent = this.$containers.left.appendChild(Utility.createElement("div", {"class": "la-tab-content la-tab-content-fixed-fields", "data-index": 2})),
		);
		
		this._generateLaCustomFields();
		this._generateLaTemplatesFields();
		this._generateLaFixedFields();
		
	}
	
	_generateLaCustomFields() {
		let self = this;
		this.laActiveCustomFieldsIndex = 0;
		
		this.laCustomFieldsList = this.$laCustomFieldsJson["item"]
				.map((value, index) => new LaFieldGroup(value, index, index === 0, true, this.$laCustomFieldsTabContent));
		
		// Add click lister to the tabs
		this.$laCustomFieldsTabContent.on("click", ".la-field-group", function (e) {
			if (!e.target.matches(".la-field-group-header")) {return;}
			if (self.laActiveCustomFieldsIndex === +this.dataset.index) {
				Utility.slideToggle(self.laCustomFieldsList[self.laActiveCustomFieldsIndex].$content);
				return;
			}
			if (this.classList.contains("active")) {
				self.laActiveCustomFieldsIndex = +this.dataset.index;
				Utility.slideToggle(self.laCustomFieldsList[self.laActiveCustomFieldsIndex].$content);
				return;
			}
			
			Utility.slideUp(self.laCustomFieldsList[self.laActiveCustomFieldsIndex].$content);
			self.laCustomFieldsList[self.laActiveCustomFieldsIndex].$element.classList.remove("active");
			
			self.laActiveCustomFieldsIndex = +this.dataset.index;
			
			Utility.slideDown(self.laCustomFieldsList[self.laActiveCustomFieldsIndex].$content);
			self.laCustomFieldsList[self.laActiveCustomFieldsIndex].$element.classList.add("active");
			
		});
	}
	
	_generateLaTemplatesFields() {
		let self = this;
		this.laActiveTemplatesIndex = 0;
		
		this.laTemplatesList = this.$laTemplatesJson["item"]
				.map((value, index) => new LaFieldGroup(value, index, true, false, this.$laTemplatesTabContent));
	}
	
	_generateLaFixedFields() {
		let self = this;
		this.laActiveFixedFieldsIndex = 0;
		
		this.laFixedFieldsList = this.$laFixedFieldsJson["item"]
				.map((value, index) => new LaFieldGroup(value, index, true, false, this.$laFixedFieldsTabContent));
	}
};

ViewBuilder.draggedField = null;
ViewBuilder.raSettingsFields = new ViewSettingsFields();