import {Utility} from "../../../common/js/utility";

export class JoinFieldsRow {
    constructor(customObjects , parentTable) {
        this.customObjects = customObjects;
        this.parentTable = parentTable;
        this.html = this.draw();
    }

    draw() {
        let self = this;

        //region Elements Creation

        let joinFieldsRow = Utility.createElement("tr");

        let customObjectSelectCell_1 = Utility.createElement("td");
        joinFieldsRow.appendChild(customObjectSelectCell_1);
        let deleteRowButton = Utility.createElement("div",{"class":"delete-row icon-times-B"});
        customObjectSelectCell_1.appendChild(deleteRowButton);
        let customObjectSelectWrapper_1 = Utility.createElement("div",{"class":"custom-object-select-wrapper-1"});
        customObjectSelectCell_1.appendChild(customObjectSelectWrapper_1);
        let customObjectSelect_1 = Utility.createElement("select");
        customObjectSelectWrapper_1.appendChild(customObjectSelect_1);
        let dummyCustomOption_1 = Utility.createElement("option",{"disabled":"", "selected":"", "value":""},"-- select option --");
        customObjectSelect_1.appendChild(dummyCustomOption_1);
        for (let customItem of self.customObjects.item){
            let customItemOption = Utility.createElement("option",{"value":customItem.name}, customItem.name);
            customObjectSelect_1.appendChild(customItemOption);
        }

        let fieldSelectCell_1 = Utility.createElement("td");
        joinFieldsRow.appendChild(fieldSelectCell_1);
        let fieldSelectWrapper_1 = Utility.createElement("div",{"class":"field-select-wrapper-1"});
        fieldSelectCell_1.appendChild(fieldSelectWrapper_1);
        let fieldSelect_1 = Utility.createElement("select");
        fieldSelectWrapper_1.appendChild(fieldSelect_1);
        let dummyFieldOption_1 = Utility.createElement("option",{"disabled":"", "selected":"", "value":""},"-- select option --");
        fieldSelect_1.appendChild(dummyFieldOption_1);
        customObjectSelect_1.addEventListener("change", function () {
            fieldSelect_1.innerHTML = "";
            fieldSelect_1.appendChild(dummyFieldOption_1);
            let activeFieldsItems = [];
            for (let customItem of self.customObjects.item){
                if (customItem.name === customObjectSelect_1.value){
                    activeFieldsItems = customItem.fields;
                }
            }
            if (activeFieldsItems.length > 0){
                for (let fieldItem of activeFieldsItems){
                    let fieldItemOption = Utility.createElement("option",{"value":fieldItem.name},fieldItem.name);
                    fieldSelect_1.appendChild(fieldItemOption);
                }
            }
        });



        let customObjectSelectCell_2 = Utility.createElement("td");
        joinFieldsRow.appendChild(customObjectSelectCell_2);
        let customObjectSelectWrapper_2 = Utility.createElement("div",{"class":"custom-object-select-wrapper-2"});
        customObjectSelectCell_2.appendChild(customObjectSelectWrapper_2);
        let customObjectSelect_2 = Utility.createElement("select");
        customObjectSelectWrapper_2.appendChild(customObjectSelect_2);
        let dummyCustomOption_2 = Utility.createElement("option",{"disabled":"", "selected":"", "value":""},"-- select option --");
        customObjectSelect_2.appendChild(dummyCustomOption_2);
        fieldSelect_1.addEventListener("change", function () {
            customObjectSelect_2.innerHTML = "";
            customObjectSelect_2.appendChild(dummyCustomOption_2);
            if (fieldSelect_1.value){
                for (let customItem of self.customObjects.item){
                    let customItemOption = Utility.createElement("option",{"value":customItem.name}, customItem.name);
                    customObjectSelect_2.appendChild(customItemOption);
                }
            }

        });




        let fieldSelectCell_2 = Utility.createElement("td");
        joinFieldsRow.appendChild(fieldSelectCell_2);
        let fieldSelectWrapper_2 = Utility.createElement("div",{"class":"field-select-wrapper-2"});
        fieldSelectCell_2.appendChild(fieldSelectWrapper_2);
        let fieldSelect_2 = Utility.createElement("select");
        fieldSelectWrapper_2.appendChild(fieldSelect_2);
        let dummyFieldOption_2 = Utility.createElement("option",{"disabled":"", "selected":"", "value":""},"-- select option --");
        fieldSelect_2.appendChild(dummyFieldOption_2);
        customObjectSelect_2.addEventListener("change", function () {
            fieldSelect_2.innerHTML = "";
            fieldSelect_2.appendChild(dummyFieldOption_2);
            let activeFieldsItems = [];
            for (let customItem of self.customObjects.item){
                if (customItem.name === customObjectSelect_1.value){
                    activeFieldsItems = customItem.fields;
                }
            }
            if (activeFieldsItems.length > 0){
                for (let fieldItem of activeFieldsItems){
                    let fieldItemOption = Utility.createElement("option",{"value":fieldItem.name},fieldItem.name);
                    fieldSelect_2.appendChild(fieldItemOption);
                }
            }
        });

        //endregion


        //region Behaviour
        deleteRowButton.addEventListener("click" , function () {
            self.parentTable.table.removeChild(self.html.joinFieldsRow);
        });
        //endregion

        return {joinFieldsRow : joinFieldsRow}
    }
}