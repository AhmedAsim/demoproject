import {Utility} from "../../common/js/utility";
import {JoinFieldsRow} from "./TablesRowTemp/joinFieldsRow";
import {AddJoinFieldsRow} from "./commands/addJoinFieldsRow";

export class JoinFieldsTable {
    constructor(customObjects){
        this.customObjects = customObjects;
        this.table = "";
        this.joinFieldsFrom = "" ;
        this.joinFields = [];
        this.html = this.draw();
    }

    draw(){
        let self = this;
        let joinFieldsTable = Utility.createElement("div",{"class":"section"});

        //region Header Elements
        let header = Utility.createElement("div",{"class":"header"});
        joinFieldsTable.appendChild(header);
        let collapseButton = Utility.createElement("button",{"class":"collapse icon-caret-down-B"});
        let headerName = Utility.createElement("div",{"class":"name"},"Join Fields" );
        let addButton = Utility.createElement("button",{"class":"icon-plus-B"});
        header.appendChild(collapseButton);
        header.appendChild(headerName);
        header.appendChild(addButton);
        //endregion

        //region Table Elements
        let tableCollapseWrapper = Utility.createElement("div",{"class":"table-collapse-wrapper"});
        joinFieldsTable.appendChild(tableCollapseWrapper);
        let tableWrapper = Utility.createElement("div",{"class":"table-wrapper"});
        tableCollapseWrapper.appendChild(tableWrapper);
        let table = Utility.createElement("table");
        tableWrapper.appendChild(table);
        self.table = table;

        let tableHeaderRow = Utility.createElement("tr");
        table.appendChild(tableHeaderRow);
        let customObjectsHeader1 = Utility.createElement("th",{},"Custom Objects1");
        let fieldHeader1 = Utility.createElement("th",{},"Field1");
        let customObjectsHeader2 = Utility.createElement("th",{},"Custom Objects2");
        let fieldHeader2 = Utility.createElement("th",{},"Field2");
        tableHeaderRow.appendChild(customObjectsHeader1);
        tableHeaderRow.appendChild(fieldHeader1);
        tableHeaderRow.appendChild(customObjectsHeader2);
        tableHeaderRow.appendChild(fieldHeader2);

        table.appendChild((new JoinFieldsRow(self.customObjects , self)).html.joinFieldsRow);
        //endregion

        // region Header Behaviour
        collapseButton.addEventListener("click" , function () {
            Utility.slideToggle(tableCollapseWrapper);
            collapseButton.classList.toggle("active");
        });
        //endregion

        //region Table Behaviour

        let rows = table.getElementsByTagName('tr');
        let deleteRowButtons = table.querySelectorAll(".delete-row");
        let sqlHandler = function(){
            self.joinFieldsFrom = "" ;
            self.joinFields = [];

            for (let  i = 1; i < rows.length ; i++){
                let objL = rows[i].querySelector(".custom-object-select-wrapper-1 select").value;
                let colL = rows[i].querySelector(".field-select-wrapper-1 select").value;
                let objR = rows[i].querySelector(".custom-object-select-wrapper-2 select").value;
                let colR = rows[i].querySelector(".field-select-wrapper-2 select").value;
                if (!self.joinFieldsFrom){
                    self.joinFieldsFrom = objL;
                }
                self.joinFields.push(`INNER JOIN  "${objL}" ON "${objL}"."${colL}" = "${objR}"."${colR}"`);
            }

        };

        addButton.addEventListener("click",function () {
            new AddJoinFieldsRow(self.customObjects , self).doo();

            rows = [];
            rows = table.getElementsByTagName('tr') ;

            deleteRowButtons = table.querySelectorAll(".delete-row");
            deleteRowButtons.forEach(e => e.addEventListener("click" , function () {
                rows = table.getElementsByTagName('tr') ;
                sqlHandler();
            }));
            sqlHandler();
        });

        deleteRowButtons.forEach(e => e.addEventListener("click" , function () {
            rows = table.getElementsByTagName('tr') ;
            sqlHandler();
        }));

        table.addEventListener("input" , function () {

            sqlHandler();
        });


        //endregion


        return  {joinFieldsTable : joinFieldsTable}
    }
}