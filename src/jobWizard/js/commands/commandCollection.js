import {CommandAbstract} from "../../../common/js/commandAbstract";
import {TextAreaCommand} from "./textAreaCommand";

export class CommandCollection extends CommandAbstract {
    constructor() {
        super();
        this.commands = [];
        this.addToStack();
    }

    doo() {
        CommandCollection.setTimeOutHandle && clearTimeout(CommandCollection.setTimeOutHandle);
        for (let command of this.commands) {
            command.doo();
        }

    }

    undo() {
        CommandCollection.setTimeOutHandle && clearTimeout(CommandCollection.setTimeOutHandle);
        for (let i = this.commands.length-1; i>=0; i--) {
            this.commands[i].undo();
        }
    }

    addCommand(command) {
        this.commands.push(command);
    }

    static addTextAreaCommand(input, obj) {
        if (CommandCollection.setTimeOutHandle && CommandCollection.previousInput === input) {
            clearTimeout(CommandCollection.setTimeOutHandle);
            CommandCollection.setTimeOutHandle = setTimeout(function () {
                CommandCollection.setTimeOutHandle = false;
            }, 400);
            CommandCollection.commandGroup.addCommand(new TextAreaCommand(input, obj));
        }
        else {
            CommandCollection.setTimeOutHandle && clearTimeout(CommandCollection.setTimeOutHandle);
            CommandCollection.setTimeOutHandle = setTimeout(function () {
                CommandCollection.setTimeOutHandle = false;
            }, 400);

            CommandCollection.commandGroup = new CommandCollection();
            CommandCollection.commandGroup.addCommand(new TextAreaCommand(input, obj));
            CommandCollection.previousInput = input;
        }
    }
}

CommandCollection.timeout = 400;
CommandCollection.commandGroup = null;
CommandCollection.setTimeOutHandle = false;
CommandCollection.previousInput = null;
