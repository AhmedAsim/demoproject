import {Utility} from "../../common/js/utility";
import {JobWizardAbstract} from "./jobWizardAbstract";
import {Action} from "./action";
import {RemoveActionGroupCommand} from "./commands/removeActionGroupCommand";
import {AddActionCommand} from "./commands/addActionCommand";
import {SelectActionGroupCommand} from "./commands/selectActionGroupCommand";
import {CommandCollection} from "./commands/commandCollection";

export class ActionGroup extends JobWizardAbstract {

    constructor(job, actionGroupObj) {
        super();
        this.job = job;
        this.actionGroupObj = actionGroupObj;
        this.actionsArray = [];
        this._ActionArray = [];
        for (let i = 0; i < actionGroupObj.jobActions.length; i++) {
            console.log("hi");
            let action = new Action(this, actionGroupObj.jobActions[i]);
            this._ActionArray.push(action);
            this.actionsArray.push(action.html);
        }
        this._initAction = this._ActionArray[0];
        this.html = this.draw();
        console.log(this.html);
    }

    draw() {

        let self = this;

        //region Right area elements
        let raActionGroupWrapper = Utility.createElement("div", {"class": "action-group-wrapper"});
        let raActionGroupHeader = Utility.createElement("div", {"class": "action-group-header"}, self.actionGroupObj.name);
        let raActionGroupSettings = Utility.createElement("div", {"class": "action-group-settings"});
        let raInputFields = {};
        raActionGroupWrapper.appendChild(raActionGroupHeader);
        raActionGroupWrapper.appendChild(raActionGroupSettings);
        for (let key of Object.keys(self.actionGroupObj)) {
            if (key !== "jobActions" && Utility.typesMaping[key]) {
                let label = Utility.createElement("label", {}, key);
                raActionGroupSettings.appendChild(label);
                raInputFields[key] = Utility.createElement(
                    Utility.typesMaping[key].Tag,
                    Object.assign(
                        (Utility.typesMaping[key].attributes ? Utility.typesMaping[key].attributes : {}), {"value": self.actionGroupObj[key]}
                    )
                );
                raActionGroupSettings.appendChild(raInputFields[key]);
            }
        }
        for (let i = 0; i < self.actionsArray.length; i++) {
            raActionGroupWrapper.appendChild(self.actionsArray[i].right);
        }
        //endregion Right area elements

        //region Drawing area elements
        let daActionGroup = Utility.createElement("div", {"class": "action-group"});
        let daActionGroupHeader = Utility.createElement("div", {"class": "header"});
        let daActionGroupName = Utility.createElement("div", {"class": "name"}, self.actionGroupObj.name);
        let daActionAdd = Utility.createElement("button", {"class": "add icon-plus-B"});
        let daActionGroupRemove = Utility.createElement("button", {"class": "remove icon-trash-alt-B"});
        daActionGroupHeader.appendChild(daActionGroupName);
        daActionGroupHeader.appendChild(daActionAdd);
        daActionGroupHeader.appendChild(daActionGroupRemove);
        daActionGroup.appendChild(daActionGroupHeader);
        let daActions = Utility.createElement("div", {"class": "actions"});
        for (let i = 0; i < self.actionsArray.length; i++) {
            daActions.appendChild(self.actionsArray[i].center);
        }
        daActionGroup.appendChild(daActions);
        //endregion Drawing area elements

        //region Left area elements
        let laActionGroup = Utility.createElement("div", {"class": "action-group"});
        let laActionGroupHeader = Utility.createElement("div", {"class": "header"});
        let laActionGroupCollapse = Utility.createElement("button", {"class": "collapse icon-caret-down-B"});
        let laActionGroupName = Utility.createElement("h4", {}, self.actionGroupObj.name);
        let laActionAdd = Utility.createElement("button", {"class": "add icon-plus-B"});
        let laActionGroupRemove = Utility.createElement("button", {"class": "remove icon-trash-alt-B"});
        laActionGroupHeader.appendChild(laActionGroupCollapse);
        laActionGroupHeader.appendChild(laActionGroupName);
        laActionGroupHeader.appendChild(laActionAdd);
        laActionGroupHeader.appendChild(laActionGroupRemove);
        laActionGroup.appendChild(laActionGroupHeader);
        let laActions = Utility.createElement("div", {"class": "actions"});
        laActionGroup.appendChild(laActions);
        for (let i = 0; i < self.actionsArray.length; i++) {
            laActions.appendChild(self.actionsArray[i].left);
        }
        //endregion Left area elements

        // region Right Area Behaviour
        raActionGroupHeader.addEventListener("click", function (event) {
            event.stopPropagation();
            Utility.slideToggle(raActionGroupSettings);
        });
        raInputFields.name.addEventListener("keydown", function () {
            this.dataset.prevValue = this.value;
        });
        raInputFields.name.addEventListener("input", function () {
            this.dataset.redoValue = this.value;
            CommandCollection.addTextAreaCommand(this, self);
        });

        if (raInputFields.description) {
            raInputFields.description.addEventListener("keydown", function () {
                this.dataset.prevValue = this.value;
            });
            raInputFields.description.addEventListener("input", function () {
                this.dataset.redoValue = this.value;
                CommandCollection.addTextAreaCommand(this, self);
            });

        }
        if (raInputFields.isActive)
            raInputFields.isActive.addEventListener("change", function () {
                self.actionGroupObj.isActive = this.value;
            });
        // endregion Right Area Behaviour

        //region Drawing Area Behaviour
        daActionGroupHeader.addEventListener("click", function (event) {
            event.stopPropagation();
            new SelectActionGroupCommand(self);
        });

        daActionAdd.addEventListener("click", function (event) {
            event.stopPropagation();

            new SelectActionGroupCommand(self);

            new AddActionCommand(self);
            // self.actionGroupObj.Actions.push(newAction.actionObj);
            // daActions.appendChild(newAction.html.center);
            // raActionGroupWrapper.appendChild(newAction.html.right);
            // laActions.appendChild(newAction.html.left);
        });
        daActionGroupRemove.addEventListener("click", function (event) {
            event.stopPropagation();
            new RemoveActionGroupCommand(self);

        });
        //endregion Drawing Area Behaviour

        //region Left Area Behaviour
        laActionGroupHeader.addEventListener("click", function (event) {
            event.stopPropagation();
            new SelectActionGroupCommand(self);
        });
        laActionAdd.addEventListener("click", function (event) {
            event.stopPropagation();
            new SelectActionGroupCommand(self);
            new AddActionCommand(self);
        });
        laActionGroupRemove.addEventListener("click", function (event) {
            event.stopPropagation();
            new RemoveActionGroupCommand(self);
        });
        laActionGroupCollapse.addEventListener("click", function (event) {
            event.stopPropagation();
            Utility.slideToggle(laActions);
            laActionGroupCollapse.classList.toggle("active");
        });
        //endregion Left Area Behaviour

        return {left: laActionGroup, right: raActionGroupWrapper, center: daActionGroup};
    }

    setActive() {
        if (JobWizardAbstract.activeAction) {
            JobWizardAbstract.activeAction.removeActive();
            JobWizardAbstract.activeAction = false;
        }
        if (JobWizardAbstract.activeJob !== this.job)
            this.job.setActive();

        if (JobWizardAbstract.activeActionGroup === false) {
            JobWizardAbstract.activeActionGroup = this;
            this._showHtml();
            Utility.expand(this.html.right.querySelector(".action-group-settings"));
            Utility.collapse(this.job.html.right.querySelector(".job-settings"));
        }
        else if (JobWizardAbstract.activeActionGroup !== this) {
            JobWizardAbstract.activeActionGroup.removeActive();
            JobWizardAbstract.activeActionGroup = this;
            this._showHtml();
            Utility.expand(this.html.right.querySelector(".action-group-settings"));
            Utility.collapse(this.job.html.right.querySelector(".job-settings"));
        }
        else {
            Utility.expand(this.html.right.querySelector(".action-group-settings"));
        }
    }

    removeActive() {
        this._hideHtml();
        Utility.collapse(this.html.right.querySelector(".action-group-settings"))
    }


}
