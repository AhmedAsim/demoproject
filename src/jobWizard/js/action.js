import {Utility} from "../../common/js/utility";
import {JobWizardAbstract} from "./jobWizardAbstract";
import {RemoveActionCommand} from "./commands/removeActionCommand";
import {SelectActionCommand} from "./commands/selectActionCommand";
import {SelectActionGroupCommand} from "./commands/selectActionGroupCommand";
import {TextAreaCommand} from "./commands/textAreaCommand";
import {CommandCollection} from "./commands/commandCollection";

// {"Name": "N11", "Description": "get N11 orders", "Is Active": true}

export class Action extends JobWizardAbstract{
	
	constructor(actionGroup, actionObj){
		super();
		this.actionGroup = actionGroup;
		this.actionObj = actionObj;
		this.html = this.draw();
		console.log(this.html);
	}
	
	draw(){

		let self = this;

		//region Right Area elements
		let raActionWrapper = Utility.createElement("div",{"class":"action-wrapper"});
		let raActionHeader = Utility.createElement("div",{"class":"action-header"},this.actionObj.name);
		let raActionSettings = Utility.createElement("div",{"class":"action-settings"});
		let raInputFields ={};
		for(let key of Object.keys(this.actionObj)){
			if(Utility.typesMaping[key]){
				let label = Utility.createElement("label",{},key);
				raActionSettings.appendChild(label);
				raInputFields[key] = Utility.createElement(
					Utility.typesMaping[key].Tag,
					Object.assign(
						Utility.typesMaping[key].attributes?Utility.typesMaping[key].attributes:{},{"value":this.actionObj[key]}
					)
				);
				raActionSettings.appendChild(raInputFields[key]);
			}
		}
		raActionWrapper.appendChild(raActionHeader);
		raActionWrapper.appendChild(raActionSettings);
		//endregion Right Area elements
		
		//region Drawing Area elements
		
		let daAction = Utility.createElement("div",{"class":"action"});
		let daActionName = Utility.createElement("div",{"class":"name"}, this.actionObj.name);
		let daActionRemove = Utility.createElement("button",{"class":"remove icon-trash-alt-B"});
		daAction.appendChild(daActionName);
		daAction.appendChild(daActionRemove);
		//endregion Drawing Area elements
		
		//region Left Area elements
		let laAction = Utility.createElement("div",{"class":"action"});
		let laActionName = Utility.createElement("h5",{}, this.actionObj.name);
		let laActionRemove = Utility.createElement("button",{"class":"remove icon-trash-alt-B"});
		laAction.appendChild(laActionName);
		laAction.appendChild(laActionRemove);
		//endregion Left Area elements
		
		//region Right Area Behaviour
		raActionHeader.addEventListener("click",function (event) {
			event.stopPropagation();
			Utility.slideToggle(raActionSettings);
		});
        raInputFields.name.addEventListener("keydown", function () {
            this.dataset.prevValue = this.value;
        });
        raInputFields.name.addEventListener("input", function () {
            this.dataset.redoValue = this.value;
            CommandCollection.addTextAreaCommand(this, self);
        });

        if (raInputFields.description) {
            raInputFields.description.addEventListener("keydown", function () {
                this.dataset.prevValue = this.value;
            });
            raInputFields.description.addEventListener("input", function () {
                this.dataset.redoValue = this.value;
                CommandCollection.addTextAreaCommand(this, self);
            });

        }
		if(raInputFields.isActive)
			raInputFields.isActive.addEventListener("change",function () {
				self.actionObj.isActive= this.value;
			});
		//endregion Right Area Behaviour
		
		//region Drawing Area Behaviour
		daAction.addEventListener("click",function (event) {
			event.stopPropagation();
            new SelectActionCommand(self);
        });

        daActionRemove.addEventListener("click", function (event) {
            event.stopPropagation();
            new RemoveActionCommand(self);


		});
		
		//endregion Drawing Area Behaviour
		
		//region Left Area Behaviour
		
		laActionRemove.addEventListener("click",function (event) {
			event.stopPropagation();
			new RemoveActionCommand(self);
		});
		
		laActionName.addEventListener("click",function (event) {
			event.stopPropagation();
			new SelectActionCommand(self);
		});
		//endregion left Area Behaviour
		
		return {left:laAction,right:raActionWrapper,center:daAction};
	}
	
	setActive() {
		if(JobWizardAbstract.activeActionGroup !== this.actionGroup)
			this.actionGroup.setActive();
		if(JobWizardAbstract.activeAction === false){
			JobWizardAbstract.activeAction = this;
			this._showHtml();
			Utility.expand(this.html.right.querySelector(".action-settings"));
			Utility.collapse(this.actionGroup.html.right.querySelector(".action-group-settings"));
			Utility.collapse(this.actionGroup.job.html.right.querySelector(".job-settings"));
		}else if(JobWizardAbstract.activeAction !== this){
			JobWizardAbstract.activeAction.removeActive();
			JobWizardAbstract.activeAction = this;
			this._showHtml();
			Utility.expand(this.html.right.querySelector(".action-settings"));
			Utility.collapse(this.actionGroup.html.right.querySelector(".action-group-settings"));
			Utility.collapse(this.actionGroup.job.html.right.querySelector(".job-settings"));
		}
	}
	
	removeActive(){
		this._hideHtml();
		Utility.collapse(this.html.right.querySelector(".action-settings"))
	}
	
}