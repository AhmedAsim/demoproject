import {Utility} from "../../../../common/js/utility";
import {CaTab} from "./CaTab";
import {CaTable} from "./CaTable";
import {CaField} from "./CaField";
import {ViewDesignerCaAbstract} from "../../abstracts/ViewDesignerCaAbstract";

export class CaSection extends ViewDesignerCaAbstract{
	constructor($json, $parent, parentClass) {
		super();
		this.$json = $json;
		this.$parent = $parent;
		this.parentClass = parentClass;
		
		this.isActive = false;
		
		this.$caElement = null;
		
		this.expandedTab = null;
		
		this.createRaElement();
		
		this.createCaElement();
		
		this.$parent.appendChild(this.$caElement);
	}
	
	static removeActive() {
		CaSection.activeElement && CaSection.activeElement.$caElement.classList.remove("active");
		CaSection.activeElement = null;
	}
	
	static addActive(element) {
		CaSection.activeElement = element;
		// Utility.slideDown(CaSection.activeElement.$body);
		CaSection.activeElement.$caElement.classList.add("active");
	}
	
	createCaElement() {
		this.$caElement = Utility.createElement("DIV", {class: "section" + (this.$json.isCollapsed ? " collapsed" : "")});
		
		this.$header = this.$caElement.appendChild(Utility.createElement("DIV", {class: "header"}));
		
		let $icon = this.$header.appendChild(Utility.createElement("BUTTON", {class: "icon-caret-down-B toggle-body"}));
		this.$name = this.$header.appendChild(Utility.createElement("DIV", {class: "name"}, this.$json.name));
		let $plus = this.$header.appendChild(Utility.createElement("BUTTON", {class: "icon-plus-B add-child"}));
		let $trash = this.$header.appendChild(Utility.createElement("BUTTON", {class: "icon-trash-alt-B remove-me"}));
		
		$icon.addEventListener("click", () => {
			Utility.slideToggle(this.$body);
			this.$caElement.classList.toggle("collapsed");
			this.$json.isCollapsed = !this.$json.isCollapsed;
			
		});
		
		$plus.addEventListener("click", () => {
			this.caTabsArray.push(new CaTab(CaTab.baseJson,this.$tabsHeader, this.$tabsBody, this));
		});
		
		$trash.addEventListener("click", () => {
			const index = this.parentClass.caSectionsArray.indexOf(this);
			this.$parent.removeChild(this.$caElement);
			this.parentClass.caSectionsArray.splice(index, 1);
			this.parentClass.$caJson.sections.splice(this.parentClass.$caJson.sections.indexOf(this.$json));
			this.parentClass.$raSettingWrapper.innerHTML = "";
		});
		
		this.$header.addEventListener("click", () => this.appendRaSettings());
		
		this.$body = this.$caElement.appendChild(Utility.createElement("DIV", {class: "body"}));
		
		this.$tabsHeader = this.$body.appendChild(Utility.createElement("DIV", {class: "tabs-header"}));
		this.$tabsBody = this.$body.appendChild(Utility.createElement("DIV", {class: "tabs-body"}));
		
		this.caTabsArray = this.$json.tabs
				.map((value, index) => new CaTab(value,this.$tabsHeader, this.$tabsBody, this));
	}
	
	appendRaSettings($child) {
		this.$raSettingWrapper.innerHTML = "";
		
		if ($child) {
			this.$raSettingWrapper.appendChild($child);
			this.$raSetting.style.height = 0;
		} else {
			CaTab.removeActive();
			CaTable.removeActive();
			CaField.removeActive();
			this.$raSetting.style.height = 'auto';
			
		}
		
		
		if (CaSection.activeElement === this) {
			return;
		}
		
		if (CaSection.activeElement) {
			CaSection.removeActive();
		}
		
		CaSection.addActive(this);
		
		this.parentClass.$raSettingWrapper.innerHTML = "";
		this.parentClass.$raSettingWrapper.appendChild(this.$raElement);
		
	};
	
	createRaElement() {
		
		this.$raElement = Utility.createElement("DIV", {class: "section-wrapper"});
		
		this.$raHeader = this.$raElement.appendChild(Utility.createElement("DIV", {class: "section-header"}, this.$json.name));
		
		this.$raSetting = this.$raElement.appendChild(Utility.createElement("DIV", {class: "section-settings"}));
		
		this.$raSettingWrapper = this.$raElement.appendChild(Utility.createElement("DIV", {}));
		
		
		this.$raHeader.addEventListener("click", () => Utility.slideToggle(this.$raSetting));
		
		
		ViewBuilder.raSettingsFields.SectionFields().forEach(this.createRaSettingsField.bind(this));
		
	}
}

CaSection.activeElement = null;
CaSection.baseJson = {
	name: "Section Name",
	description: "",
	isCollapsed: false,
	visibilityFormula: "",
	
	tabs: [
		{
			name: "Tab Name",
			description: "",
			isOpen: true,
			visibilityFormula: "",
			
			tabSections: [
				{
					name: "Table Name",
					description: "",
					isCollapsed: false,
					visibilityFormula: "",
					rowCount: 2,
					columnCount: 2,
					
				},
			],
		},
	],
};

