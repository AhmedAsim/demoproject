import "../css/dataFilter.scss";
import {Utility} from "../../common/js/utility";
import {CommonView} from "../../common/js/commonView";
import {CriteriaTable} from "./criteriaTable";
import {JoinFieldsTable} from "./joinFieldsTable";
import {OrderByTable} from "./orderbyTable";
import {SelectFieldsTable} from "./selectFieldsTable";

class DataFilter {
    constructor(customObj, headerJson, appName, options = {}){

        let commonView = new CommonView(headerJson, appName,  options);

        this.customObj = customObj;
        this.ra = commonView.rightArea;
        this.la = commonView.leftArea;
        this.da = commonView.centerArea.appendChild(Utility.createElement('div',{'class':'position-wrapper'}));
        this.draw();
    }

    draw(){
        let self = this;

        //region Right Area Elements
        let raHeader = Utility.createElement("div", {"class": "ra-header"});
        let raName = Utility.createElement("div", {"class": "ra-name"}, "Settings");
        let saveButton = Utility.createElement("button", {"class": "save"}, "Save");

        raHeader.appendChild(raName);
        raHeader.appendChild(saveButton);
        self.ra.appendChild(raHeader);

        let raContainer = Utility.createElement("div", {"class": "ra-container"});
        this.ra.appendChild(raContainer);
        let raSettingsWrapper = Utility.createElement("div", {"class": "settings-wrapper"});
        raContainer.appendChild(raSettingsWrapper);
        let raSettings = Utility.createElement("div", {"class": "settings"});
        raSettingsWrapper.appendChild(raSettings);
        let nameLabel = Utility.createElement("label", {}, "Filter Name");
        let nameInput = Utility.createElement("input", {"type": "text"});
        let descriptionLabel = Utility.createElement("label", {}, "Filter Description");
        let descriptionInput = Utility.createElement("input", {"type": "text"});
        let isActiveLabel = Utility.createElement("label", {}, "Is Active");
        let isActiveInput = Utility.createElement("input", {"type": "checkbox"});
        let sqlLabel = Utility.createElement("label", {}, "SQL");
        let sqlInput = Utility.createElement("textarea",{"rows":"10","cols":"22"});
        raSettings.appendChild(nameLabel);
        raSettings.appendChild(nameInput);
        raSettings.appendChild(descriptionLabel);
        raSettings.appendChild(descriptionInput);
        raSettings.appendChild(isActiveLabel);
        raSettings.appendChild(isActiveInput);
        raSettings.appendChild(sqlLabel);
        raSettings.appendChild(sqlInput);

        //endregion Right Area Elements

        //region Drawing Area Elements
        let daHeader = Utility.createElement("div",{"class":"da-header"});
        self.da.appendChild(daHeader);
        let appName = Utility.createElement("div",{"class":"app-name"},"Data Filter");
        daHeader.appendChild(appName);


        let daContainer = Utility.createElement("div",{"class":"da-container"});
        self.da.appendChild(daContainer);
        let daCanvas = Utility.createElement("div",{"class":"canvas"});
        daContainer.appendChild(daCanvas);
        let sectionsWrapper = Utility.createElement("div",{"class":"sections-wrapper"});
        daCanvas.appendChild(sectionsWrapper);

        let criteriaTable = new CriteriaTable(self.customObj);
        let joinFieldsTable = new JoinFieldsTable(self.customObj);
        let orderByTable = new OrderByTable(self.customObj);
        let selectFieldsTable = new SelectFieldsTable(self.customObj);

        sectionsWrapper.appendChild(criteriaTable.html.criteriaTable);
        sectionsWrapper.appendChild(joinFieldsTable.html.joinFieldsTable);
        sectionsWrapper.appendChild(orderByTable.html.orderByTable);
        sectionsWrapper.appendChild(selectFieldsTable.html.selectFieldsTable);

        //endregion

        //region Right Area Behaviour
        let sqlUpdateFunction = function(){
          try {
              let from = "";
              if (!from){
                  from = joinFieldsTable.joinFieldsFrom;
              }
              if (!from){
                  from = selectFieldsTable.joinFieldsFrom;
              }
              if (!from){
                  from = criteriaTable.joinFieldsFrom;
              }

              let query = 'SELECT';
              if (selectFieldsTable.selectFields.length > 0) {
                  query += ' ' + selectFieldsTable.selectFields.join(',') + ' ';
              } else {
                  query += ' * ';
              }
              query += ' FROM "' + from + '" \n';
              if (joinFieldsTable.joinFields.length > 0) {
                  query += joinFieldsTable.joinFields.join("\n");
              }
              query += ' \n';
              if (!criteriaTable.whereExp) {
                  criteriaTable.whereExp = criteriaTable.criterias.join(" AND ");
              } else {
                  criteriaTable.criterias.forEach((item, i) => {
                      criteriaTable.whereExp =
                          criteriaTable.whereExp.replace('{' + (i + 1) + '}', item); //+ (i == criterias.length - 1 ? '' : ' AND ');
                  });
                  criteriaTable.whereExp = criteriaTable.whereExp.replace(new RegExp("AND " + '$', ""));
              }
              query += "WHERE " + criteriaTable.whereExp + ' \n';
              if (orderByTable.orders.length > 0) {
                  query += 'ORDER BY ' + orderByTable.orders.join(",");
              }
              sqlInput.value = query;
          }
          catch (e) {
              console.log(e);
          }
        };
        sectionsWrapper.addEventListener("input" , function () {
            sqlUpdateFunction();
        })
        //endregion
    }
}

window.DataFilter = DataFilter;