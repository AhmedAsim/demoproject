import {CommandAbstract} from "../../../common/js/commandAbstract";

export class TextAreaCommand extends CommandAbstract{

    constructor(textArea, object){
        super();
        this.textArea = textArea;
        this.prevValue = textArea.dataset.prevValue;
        this.redoValue = textArea.dataset.redoValue;
        this.object = object;
        this.doo();

    }

    doo(){
        if(this.textArea.tagName === "INPUT"){
            this.textArea.value = this.redoValue;
            this.object.html.left.querySelector("h5 , h4 , h3").innerHTML = this.redoValue;
            this.object.html.right.querySelector(".action-header , .action-group-header , .job-header").innerHTML = this.redoValue;
            this.object.html.center.querySelector(".name").innerHTML = this.redoValue;
            (this.object.actionObj && (this.object.actionObj.name = this.redoValue)) ||
            (this.object.actionGroupObj && (this.object.actionGroupObj.name = this.redoValue)) ||
            (this.object.jobObj && (this.object.jobObj.name = this.redoValue));
        }

        if(this.textArea.tagName === "TEXTAREA"){
            this.textArea.value = this.redoValue;
            (this.object.actionObj && (this.object.actionObj.description = this.redoValue)) ||
            (this.object.actionGroupObj && (this.object.actionGroupObj.description = this.redoValue)) ||
            (this.object.jobObj && (this.object.jobObj.description = this.redoValue));
        }

    }

    undo(){
        if(this.textArea.tagName === "INPUT"){
            this.textArea.value = this.prevValue;
            this.object.html.left.querySelector("h5 , h4 , h3").innerHTML = this.prevValue;
            this.object.html.right.querySelector(".action-header , .action-group-header , .job-header").innerHTML = this.prevValue;
            this.object.html.center.querySelector(".name").innerHTML = this.prevValue;
            (this.object.actionObj && (this.object.actionObj.name = this.prevValue)) ||
            (this.object.actionGroupObj && (this.object.actionGroupObj.name = this.prevValue)) ||
            (this.object.jobObj && (this.object.jobObj.name = this.prevValue));

        }

        if(this.textArea.tagName === "TEXTAREA"){
            this.textArea.value = this.prevValue;
            (this.object.actionObj && (this.object.actionObj.description = this.prevValue)) ||
            (this.object.actionGroupObj && (this.object.actionGroupObj.description = this.prevValue)) ||
            (this.object.jobObj && (this.object.jobObj.description = this.prevValue));
        }

    }

}
