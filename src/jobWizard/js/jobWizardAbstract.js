
export class JobWizardAbstract{
	_showHtml(){
		this.html.left.classList.add("active");
		this.html.right.classList.add("active");
		this.html.center.classList.add("active");
	}
	
	_hideHtml(){
		this.html.left.classList.remove("active");
		this.html.right.classList.remove("active");
		this.html.center.classList.remove("active");
	}
	
	_removeHtml(){
		this.html.left.parentNode.removeChild(this.html.left);
		this.html.right.parentNode.removeChild(this.html.right);
		this.html.center.parentNode.removeChild(this.html.center);
	}
	
	static getDefaultAction(){
		return {"name":"Action Name","description":"", "isActive":false};
	}

	static getDefaultActionGroup(){
		return {"name":"Action Group Name", "description":"","isActive": false, "jobActions":[JobWizardAbstract.getDefaultAction()]};
	}

	static getDefaultJob(){

        return {
        	"name": "New Job",
            "description": "",
            "isActive": true,
            "jobActionGroups": [JobWizardAbstract.getDefaultActionGroup()]
        }
    }


}
JobWizardAbstract.activeAction = false;
JobWizardAbstract.activeActionGroup = false;
JobWizardAbstract.activeJob = false;

