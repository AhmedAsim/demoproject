import {Utility} from "../../../../common/js/utility";
import {CaTable} from "./CaTable";
import {CaField} from "./CaField";
import {ViewDesignerCaAbstract} from "../../abstracts/ViewDesignerCaAbstract";

export class CaTab extends ViewDesignerCaAbstract{
	constructor($json,$parentHeader, $parentBody, parentClass) {
		super();
		this.$parentHeader = $parentHeader;
		this.$json = $json;
		this.$parentBody = $parentBody;
		this.parentClass = parentClass;
		
		this.$caElement = null;
		
		this.createRaElement();
		
		this.createCaElement();
		
		this.$parentBody.appendChild(this.$caElement);
		
		this.$json.isOpen && (parentClass.expandedTab=this);
	}
	
	static removeActive() {
		CaTab.activeElement && CaTab.activeElement.$header.classList.remove("active");
		CaTab.activeElement = null;
	}
	
	static addActive(element) {
		element.parentClass.expandedTab.$header.classList.remove("expanded");
		element.parentClass.expandedTab.$caElement.classList.remove("active");
		element.parentClass.expandedTab = element;
		CaTab.activeElement = element;
		CaTab.activeElement.$caElement.classList.add("active");
		CaTab.activeElement.$header.classList.add("active");
		CaTab.activeElement.$header.classList.add("expanded");
	}
	
	createCaElement() {
		this.$caElement = Utility.createElement("DIV", {class: "tab" + (this.$json.isOpen ? ' active': '')});
		
		this.$header = this.$parentHeader.appendChild(Utility.createElement("DIV", {class: `header ${this.$json.isOpen&&'expanded'}`}));
		
		
		// let $icon = this.$header.appendChild(Utility.createElement("BUTTON", {class: "icon-caret-down-B toggle-body"}));
		this.$name = this.$header.appendChild(Utility.createElement("DIV", {class: "name"}, this.$json.name));
		let $plus = this.$header.appendChild(Utility.createElement("BUTTON", {class: "icon-plus-B add-child"}));
		let $trash = this.$header.appendChild(Utility.createElement("BUTTON", {class: "icon-trash-alt-B remove-me"}));
		
		
		// $icon.addEventListener("click", () => {
		// 	Utility.slideToggle(this.$body);
		// 	this.$caElement.classList.toggle("collapsed");
		// 	this.$json.isOpen = !this.$json.isOpen;
		//
		// });
		
		$plus.addEventListener("click", () => {
			this.caTablesArray.push(new CaTable(CaTable.baseJson, this.$tables, this));
		});
		
		$trash.addEventListener("click", () => {
			const index = this.parentClass.caTabsArray.indexOf(this);
			this.$parentBody.removeChild(this.$caElement);
			this.parentClass.caTabsArray.splice(index, 1);
			this.parentClass.$json.tabs.splice(this.parentClass.$json.tabs.indexOf(this.$json));
			this.parentClass.$raSettingWrapper.innerHTML = "";
		});
		
		this.$header.addEventListener("click", () => this.appendRaSettings());
		
		this.$body = this.$caElement.appendChild(Utility.createElement("DIV", {class: "body"}));
		this.$tables = this.$body.appendChild(Utility.createElement("DIV", {class: "tables"}));
		
		this.caTablesArray = this.$json.tabSections
				.map((value, index) => new CaTable(value, this.$tables, this));
		
	}
	
	appendRaSettings($child) {
		
		this.$raSettingWrapper.innerHTML = "";
		
		if ($child) {
			this.$raSettingWrapper.appendChild($child);
			this.$raSetting.style.height = 0;
		} else {
			CaTable.removeActive();
			CaField.removeActive();
			this.$raSetting.style.height = 'auto';
			
		}
		
		if (CaTab.activeElement === this) {
			return;
		}
		
		if (CaTab.activeElement) {
			CaTab.removeActive();
		}
		
		CaTab.addActive(this);
		this.parentClass.appendRaSettings(this.$raElement);
		
	};
	
	createRaElement() {
		this.$raElement = Utility.createElement("DIV", {class: "tab-wrapper"});
		this.$raHeader = this.$raElement.appendChild(Utility.createElement("DIV", {class: "tab-header"}, this.$json.name));
		this.$raSetting = this.$raElement.appendChild(Utility.createElement("DIV", {class: "tab-settings"}));
		this.$raSettingWrapper = this.$raElement.appendChild(Utility.createElement("DIV", {}));
		
		this.$raHeader.addEventListener("click", () => Utility.slideToggle(this.$raSetting));
		
		
		ViewBuilder.raSettingsFields.TabFields().forEach(this.createRaSettingsField.bind(this));
		
	}
	
}

CaTab.activeElement = null;

CaTab.baseJson = {
	name: "Tab Name",
	description: "",
	isOpen: false,
	visibilityFormula: "",
	
	tabSections: [
		{
			name: "Table Name",
			description: "",
			isCollapsed: false,
			visibilityFormula: "",
			rowCount: 2,
			columnCount: 2,
			
			viewItems: [
			],
		},
	],
};