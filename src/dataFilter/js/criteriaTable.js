import {Utility} from "../../common/js/utility";
import {CriteriaConditionRow} from "./TablesRowTemp/criteriaConditionRow";
import {AddCriteriaConditionRow} from "./commands/addCriteriaConditionRow";


export class CriteriaTable {
    constructor(customObjects){
        this.customObjects = customObjects;
        this.table = "";
        this.count = 1 ;
        this.whereExp = "";
        this.criteriaFrom = "" ;
        this.criterias = [];
        this.html = this.draw();
    }

    draw(){
        let self = this;

        let criteriaTable = Utility.createElement("div", {"class":"section criteria-conditions"});

        //region Header Elements
        let header = Utility.createElement("div",{"class":"header"});
        criteriaTable.appendChild(header);
        let collapseButton = Utility.createElement("button",{"class":"collapse icon-caret-down-B"});
        let headerName = Utility.createElement("div",{"class":"name"},"Criteria Conditions" );
        let addCriteriaButton = Utility.createElement("button",{"class":"icon-plus-B"});
        header.appendChild(collapseButton);
        header.appendChild(headerName);
        header.appendChild(addCriteriaButton);
        //endregion

        //region Table Elements
        let tableCollapseWrapper = Utility.createElement("div",{"class":"table-collapse-wrapper"});
        criteriaTable.appendChild(tableCollapseWrapper);
        let tableWrapper = Utility.createElement("div",{"class":"table-wrapper"});
        tableCollapseWrapper.appendChild(tableWrapper);
        let table = Utility.createElement("table");
        tableWrapper.appendChild(table);

        let tableHeaderRow = Utility.createElement("tr");
        self.table = table;
        table.appendChild(tableHeaderRow);
        let seqNumberHeader = Utility.createElement("th");
        let customObjectsHeader = Utility.createElement("th",{},"Custom Objects");
        let fieldHeader = Utility.createElement("th",{},"Field");
        let criteriaOperatorHeader = Utility.createElement("th",{},"Criteria Operator");
        let valueHeader = Utility.createElement("th",{},"Value");
        tableHeaderRow.appendChild(seqNumberHeader);
        tableHeaderRow.appendChild(customObjectsHeader);
        tableHeaderRow.appendChild(fieldHeader);
        tableHeaderRow.appendChild(criteriaOperatorHeader);
        tableHeaderRow.appendChild(valueHeader);


        table.appendChild((new CriteriaConditionRow(self.customObjects ,self )).html.criteriaConditionRow);

        let criteriaTextWrapper = Utility.createElement("tr", {"class": "criteria-text-wrapper"});
        table.appendChild(criteriaTextWrapper);
        let criteriaTextCell = Utility.createElement("td",{"colspan":"5"});
        criteriaTextWrapper.appendChild(criteriaTextCell);
        let criteriaText = Utility.createElement("input" , {"type":"text","class":"criteria-text" , "value":"{1}"});
        criteriaTextCell.appendChild(criteriaText);



        //endregion

        // region Header Behaviour
        collapseButton.addEventListener("click" , function () {
           Utility.slideToggle(tableCollapseWrapper);
           collapseButton.classList.toggle("active");
        });


        //endregion

        //region Table Behaviour
        let rows = table.getElementsByTagName('tr');
        let deleteRowButtons = table.querySelectorAll(".delete-row");
        let sqlHandler = function(){
            self.whereExp = "";
            self.criteriaFrom = "" ;
            self.criterias = [];

            self.whereExp = criteriaText.value;

            for (let  i = 1; i < rows.length - 1; i++){
                let obj = rows[i].querySelector(".custom-object-select-wrapper select").value;
                let col = rows[i].querySelector(".field-select-wrapper select").value;
                let op = rows[i].querySelector(".operator-select-wrapper select").value;
                let val = rows[i].querySelector(".value-wrapper input").value;
                if(!self.criteriaFrom){
                    self.criteriaFrom = obj;
                }
                self.criterias.push(`"${obj}"."${col}" ${op} "${val}" `);
            }
        };
        let criteriaTextHandler = function(rows){
            criteriaText.value = "";
            for (let i = 1; i < rows.length - 1; i++) {
                let count = rows[i].querySelector(".seq-no-wrapper").innerHTML;
                if (rows.length > 1 ) {
                    if (count === "1")
                        criteriaText.value  = "{1}";
                    else
                        criteriaText.value += " AND {" + count + "}";
                }
                else {
                    criteriaText.value  = "{1}";
                }
            }
        };


        //add criteria condition and update sequence number
        addCriteriaButton.addEventListener("click",function () {
            new AddCriteriaConditionRow(self.customObjects , self).doo();

            // rows = [];
            // rows = table.getElementsByTagName('tr');
            deleteRowButtons = table.querySelectorAll(".delete-row");
            deleteRowButtons.forEach(e => e.addEventListener("click" , function () {
                rows = table.getElementsByTagName('tr') ;
                criteriaTextHandler(rows);
                sqlHandler();
            }));
            criteriaTextHandler(rows);
            sqlHandler();
        });

        deleteRowButtons.forEach(e => e.addEventListener("click" , function () {
            rows = table.getElementsByTagName('tr') ;
            criteriaTextHandler(rows);
            sqlHandler();
        }));

        table.addEventListener("input" , function () {
            sqlHandler();
        });
        //endregion

        return {criteriaTable : criteriaTable};
    }
}