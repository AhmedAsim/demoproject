import {Utility} from "../../../../common/js/utility";
import {CaTableCell} from "./CaTableCell";
import {CaField} from "./CaField";
import {ViewDesignerCaAbstract} from "../../abstracts/ViewDesignerCaAbstract";


// $tbody.querySelectorAll('td:nth-child(3)[colspan]:not([colspan="1"])') // is add column after allowed
// $tbody.querySelectorAll('tr:nth-child(3) td[rowspan]:not([rowspan="1"])') // is add row after allowed

export class CaTable extends ViewDesignerCaAbstract {
	constructor($json, $parent, parentClass) {
		super();
		this.$json = $json;
		this.$parent = $parent;
		this.parentClass = parentClass;
		
		this.$caElement = null;
		
		this.caTableFieldsArray = [];
		
		this.mergedRows = [];
		this.mergedColumns = [];
		
		
		this.createCaElement();
		
		this.setDragSelectFunctionality();
		
		this.createRaElement();
		
		this.$parent.appendChild(this.$caElement);
	}
	
	static removeActive() {
		CaTable.activeElement && CaTable.activeElement.$caElement.classList.remove("active");
		CaTable.activeElement = null;
	}
	
	static addActive(element) {
		CaTable.activeElement = element;
		// Utility.slideDown(CaTable.activeElement.$body);
		CaTable.activeElement.$caElement.classList.add("active");
	}
	
	static allowMerge(value) {
		CaTable.mergeAllowed = value;
		ViewBuilder.$mergeButton.classList.toggle("disabled", !value);
	}
	
	static allowUnmerge(value) {
		CaTable.unmergeAllowed = value;
		ViewBuilder.$unmergeButton.classList.toggle("disabled", !value);
	}
	
	static clearSelected() {
		CaTable.selectedCellsSet.forEach((cell) => {
			cell.deSelect();
		});
		CaTable.selectedCellsSet.clear();
		CaTable.allowMerge(false);
	}
	
	static addToSelection(cell, addAllCells = true) {
		
		let
				rowIndex = cell.rowIndex,
				columnIndex = cell.columnIndex,
				rowIndexAndSpan = cell.rowIndex + cell.rowSpan,
				columnIndexAndSpan = cell.columnIndex + cell.colSpan,
				{columnStart, rowStart, columnEnd, rowEnd} = CaTable.marqueeSelectPoints,
				newRowStart = Math.min(rowIndex, rowStart, rowEnd),
				newColumnStart = Math.min(columnIndex, columnStart, columnEnd),
				newRowEnd = Math.max(rowIndexAndSpan, rowStart, rowEnd),
				newColumnEnd = Math.max(columnIndexAndSpan, columnStart, columnEnd);
		
		CaTable.marqueeSelectPoints = {
			rowStart: newRowStart,
			rowEnd: newRowEnd,
			columnStart: newColumnStart,
			columnEnd: newColumnEnd,
		};
		if (addAllCells) {
			for (let rowLoop = newRowStart; rowLoop < newRowEnd; rowLoop++) {
				for (let columnLoop = newColumnStart; columnLoop < newColumnEnd; columnLoop++) {
					let selectedCell = cell.parentClass.getCellFromRowColumn(rowLoop, columnLoop);
					if (!selectedCell.selected) {
						CaTable.selectedCellsSet.add(selectedCell);
						selectedCell.select();
					}
				}
			}
			console.log(CaTable.selectedCellsSet);
			CaTable.allowMerge(CaTable.selectedCellsSet.size > 1);
		} else {
			for (let rowLoop = rowIndex; rowLoop < rowIndexAndSpan; rowLoop++) {
				for (let columnLoop = columnIndex; columnLoop < columnIndexAndSpan; columnLoop++) {
					let selectedCell = cell.parentClass.getCellFromRowColumn(rowLoop, columnLoop);
					if (!selectedCell.selected) {
						CaTable.selectedCellsSet.add(selectedCell);
						selectedCell.select();
					}
				}
			}
			CaTable.allowMerge(CaTable.selectedCellsSet.size === Math.abs((newColumnEnd - newColumnStart) * (newRowEnd - newRowStart)));
		}
		
		
		// if (CaTable.selectedCells.size > 1) {
		//
		// }
		
	}
	
	static removeFromSelected(cell, removeAllCell = true) {
		
		let [columnStart, rowStart, columnEnd, rowEnd] = [Number.POSITIVE_INFINITY, Number.POSITIVE_INFINITY, Number.NEGATIVE_INFINITY, Number.NEGATIVE_INFINITY],
				rowIndexAndSpan = cell.rowIndex + cell.rowSpan,
				columnIndexAndSpan = cell.columnIndex + cell.colSpan;
		
		for (let rowLoop = cell.rowIndex; rowLoop < rowIndexAndSpan; rowLoop++) {
			for (let columnLoop = cell.columnIndex; columnLoop < columnIndexAndSpan; columnLoop++) {
				let selectedCell = cell.parentClass.getCellFromRowColumn(rowLoop, columnLoop);
				console.log(selectedCell.selected);
				if (selectedCell.selected) {
					CaTable.selectedCellsSet.delete(selectedCell);
					cell.deSelect();
				}
			}
		}
		
		CaTable.selectedCellsSet.forEach(cell => {
			rowStart > cell.rowIndex && (rowStart = cell.rowIndex);
			columnStart > cell.columnIndex && (columnStart = cell.columnIndex);
			rowEnd < cell.rowIndex + cell.rowSpan && (rowEnd = cell.rowIndex + cell.rowSpan);
			columnEnd < cell.columnIndex + cell.colSpan && (columnEnd = cell.columnIndex + cell.colSpan);
		});
		CaTable.marqueeSelectPoints = {columnStart, rowStart, columnEnd, rowEnd};
		console.log(columnEnd, CaTable.marqueeSelectPoints);
		CaTable.allowMerge(CaTable.selectedCellsSet.size > 1 && CaTable.selectedCellsSet.size === Math.abs((columnEnd - columnStart) * (rowEnd - rowStart)));
		
	}
	
	static toggleSelection(cell) {
		if (cell.selected) {
			CaTable.removeFromSelected(cell, false);
		} else {
			CaTable.addToSelection(cell, false);
		}
	}
	
	static mergeCells(cell, rowIndex, columnIndex, rowSpan, colSpan) {
		
		if (rowSpan === 1 && colSpan === 1) {
			return;
		}
		
		if (rowIndex + rowSpan > cell.parentClass.$json.rowCount) {
			let errorText = "number of rowSpan{" + rowSpan + "} is not allowed at this rowIndex{" + rowIndex + "}";
			cell.$caElement.innerHTML = errorText;
			console.error(errorText);
			cell.$caElement.style.color = "white";
			cell.$caElement.style.background = "red";
			return;
		}
		if (columnIndex + colSpan > cell.parentClass.$json.columnCount) {
			let errorText = "number of colSpan{" + colSpan + "} is not allowed at this columnIndex{" + columnIndex + "}";
			cell.$caElement.innerHTML = errorText;
			console.error(errorText);
			cell.$caElement.style.color = "white";
			cell.$caElement.style.background = "red";
			return;
		}
		
		cell.setColSpan(colSpan);
		cell.setRowSpan(rowSpan);
		for (let rowLoop = rowIndex; rowLoop < rowIndex + rowSpan; rowLoop++) {
			cell.parentClass.mergedRows[rowLoop] = true;
			for (let columnLoop = columnIndex; columnLoop < columnIndex + colSpan; columnLoop++) {
				cell.parentClass.mergedColumns[columnLoop] = true;
				
				
				let selectedCell = cell.parentClass.getCellFromRowColumn(rowLoop, columnLoop);
				rowLoop !== rowIndex && selectedCell.$caElement.setAttribute("prevent-add-row-before", "");
				columnLoop !== columnIndex && selectedCell.$caElement.setAttribute("prevent-add-column-before", "");
				if (rowLoop !== rowIndex || columnLoop !== columnIndex) {
					selectedCell.hide();
					selectedCell.setColSpan(1);
					selectedCell.setRowSpan(1);
					selectedCell.fieldClass && selectedCell.fieldClass.removeField();
					
				}
				selectedCell.mergedCellsSet.clear();
				selectedCell.mergedTo = cell;
				cell.mergedCellsSet.add(selectedCell);
			}
		}
		
	}
	
	static unMergeCells(cell) {
		cell.mergedCellsSet.forEach(selectedCell => {
			selectedCell.$caElement.removeAttribute("prevent-add-row-before");
			selectedCell.$caElement.removeAttribute("prevent-add-column-before");
			selectedCell.mergedTo = null;
			selectedCell.show();
			selectedCell.setColSpan(1);
			selectedCell.setRowSpan(1);
			// selectedCell.fieldClass && selectedCell.fieldClass.remove();
		});
		cell.mergedCellsSet.clear();
	}
	
	createCaElement() {
		this.$caElement = Utility.createElement("DIV", {class: "table" + (this.$json.isCollapsed ? " collapsed" : "")});
		
		this.$header = this.$caElement.appendChild(Utility.createElement("DIV", {class: "header"}));
		
		let $icon = this.$header.appendChild(Utility.createElement("BUTTON", {class: "icon-caret-down-B toggle-body"}));
		this.$name = this.$header.appendChild(Utility.createElement("DIV", {class: "name"}, this.$json.name));
		let $trash = this.$header.appendChild(Utility.createElement("BUTTON", {class: "icon-trash-alt-B remove-me"}));
		
		
		$icon.addEventListener("click", () => {
			Utility.slideToggle(this.$body);
			this.$caElement.classList.toggle("collapsed");
			this.$json.isCollapsed = !this.$json.isCollapsed;
			
		});
		
		$trash.addEventListener("click", () => {
			const index = this.parentClass.caTablesArray.indexOf(this);
			this.$parent.removeChild(this.$caElement);
			this.parentClass.caTablesArray.splice(index, 1);
			this.parentClass.$json.tabSections.splice(this.parentClass.$json.tabSections.indexOf(this.$json), 1);
			this.parentClass.$raSettingWrapper.innerHTML = "";
		});
		
		this.$header.addEventListener("click", () => this.appendRaSettings());
		
		this.$body = this.$caElement.appendChild(Utility.createElement("DIV", {class: "body"}));
		
		this.$controls = this.$body.appendChild(Utility.createElement("DIV", {class: "controls"}));
		
		this.createTable();
		
		this.caTableFieldsArray = this.$json.viewItems.map((value, index) => new CaField(value, this));
	}
	
	getCellFromRowColumn(row, column) {
		return this.caTableCellsArray[row][column];
	}
	
	getCellFromHTMLTableCellElement($td) {
		let column = $td.cellIndex - 1;
		let row = $td.parentNode.rowIndex - 1;
		return this.getCellFromRowColumn(row, column);
	}
	
	createTable() {
		this.$table = this.$body.appendChild(Utility.createElement("TABLE", {}, "" +
				"<thead><tr><th><i class='icon-plus-circle-B add-row'></i><i class='icon-plus-circle-B add-column'></i></th></tr></thead>" +
				"<tbody></tbody>"));
		this.$table.querySelector("i.add-row").addEventListener("click", () => this.createRow(0));
		this.$table.querySelector("i.add-column").addEventListener("click", () => this.createColumn(0));
		this.caTableCellsArray = [];
		this.$thead = this.$table.getElementsByTagName("THEAD")[0];
		this.$tbody = this.$table.getElementsByTagName("TBODY")[0];
		let theadRow = this.$thead.children[0];
		for (let rowIndex = 0; rowIndex < this.$json.rowCount; rowIndex++) {
			this.mergedRows.push(false);
			const rowArray = [];
			const $tr = this.$tbody.appendChild(Utility.createElement("TR", {}));
			let $rowTh = $tr.appendChild(Utility.createElement("TH", {}, `<div class="name">${rowIndex + 1}</div>`));
			let $addRow = $rowTh.appendChild(Utility.createElement("I", {class: "icon-plus-circle-B add-row"}));
			let $removeRow = $rowTh.appendChild(Utility.createElement("I", {class: "icon-times-circle-B remove-row"}));
			$addRow.addEventListener("click", () => {this.checkToCreateRow($tr);});
			$removeRow.addEventListener("click", () => this.checkToRemoveRow($tr));
			
			
			for (let columnIndex = 0; columnIndex < this.$json.columnCount; columnIndex++) {
				rowArray.push(new CaTableCell($tr, this));
				if (rowIndex === 0) {
					this.mergedColumns.push(false);
					let $theadTh = theadRow.appendChild(Utility.createElement("TH", {}, `<div class="name">${Utility.convertNumberToLetterIndex(columnIndex)}</div>`));
					let $addColumn = $theadTh.appendChild(Utility.createElement("I", {class: "icon-plus-circle-B add-column"}));
					let $removeColumn = $theadTh.appendChild(Utility.createElement("I", {class: "icon-times-circle-B remove-column"}));
					$addColumn.addEventListener("click", () => {this.checkToCreateColumn($theadTh);});
					$removeColumn.addEventListener("click", () => this.checkToRemoveColumn(columnIndex));
				}
			}
			this.caTableCellsArray.push(rowArray);
		}
		
	}
	
	appendRaSettings($child) {
		this.$raSettingWrapper.innerHTML = "";
		if ($child) {
			this.$raSettingWrapper.appendChild($child);
			this.$raSetting.style.height = 0;
		} else {
			CaField.removeActive();
			this.$raSetting.style.height = "auto";
		}
		if (CaTable.activeElement === this) {
			return;
		}
		
		if (CaTable.activeElement) {
			CaTable.removeActive();
		}
		
		CaTable.addActive(this);
		this.parentClass.appendRaSettings(this.$raElement);
		
	};
	
	createRaElement() {
		this.$raElement = Utility.createElement("DIV", {class: "table-wrapper"});
		this.$raHeader = this.$raElement.appendChild(Utility.createElement("DIV", {class: "table-header"}, this.$json.name));
		this.$raSetting = this.$raElement.appendChild(Utility.createElement("DIV", {class: "table-settings"}));
		this.$raSettingWrapper = this.$raElement.appendChild(Utility.createElement("DIV", {}));
		
		this.$raHeader.addEventListener("click", () => Utility.slideToggle(this.$raSetting));
		
		
		ViewBuilder.raSettingsFields.TabSectionFields().forEach(this.createRaSettingsField.bind(this));
		
	}
	
	setDragSelectFunctionality() {
		this.$tbody.addEventListener("mousedown", (event) => {
			let $td;
			if (event.target.closest(".ca-field")
					|| !($td = event.target.closest("td"))) {return;}
			
			let cellClass = this.getCellFromHTMLTableCellElement($td);
			
			if (event.ctrlKey) {
				if (!CaTable.selectedCellsSet.size) {
					CaTable.marqueeSelectPoints = {
						rowStart: cellClass.rowIndex,
						rowEnd: cellClass.rowIndex + cellClass.rowSpan,
						columnStart: cellClass.columnIndex,
						columnEnd: cellClass.columnIndex + cellClass.colSpan,
					};
				}
				CaTable.toggleSelection(cellClass);
				
			} else if (event.shiftKey) {
				if (!CaTable.selectedCellsSet.size) {
					CaTable.marqueeSelectPoints = {
						rowStart: cellClass.rowIndex,
						rowEnd: cellClass.rowIndex + cellClass.rowSpan,
						columnStart: cellClass.columnIndex,
						columnEnd: cellClass.columnIndex + cellClass.colSpan,
					};
				}
				CaTable.addToSelection(cellClass);
				
			} else {
				CaTable.clearSelected();
				CaTable.marqueeSelectPoints = {
					rowStart: cellClass.rowIndex,
					rowEnd: cellClass.rowIndex + cellClass.rowSpan,
					columnStart: cellClass.columnIndex,
					columnEnd: cellClass.columnIndex + cellClass.colSpan,
				};
				CaTable.addToSelection(cellClass);
				CaTable.marqueeSelectMode = true;
			}
			this.checkForUnmerge();
			
			
		});
		this.$tbody.addEventListener("mouseup", (event) => {
			CaTable.marqueeSelectMode = false;
		});
		this.$tbody.addEventListener("mousemove", this.marqueeSelectHandler.bind(this));
	}
	
	marqueeSelectHandler(event) {
		// let $td;
		// if (!CaTable.marqueeSelectMode || !($td = event.target.closest("td"))) {return;}
		
		// let cellClass = this.getCellFromHTMLTableCellElement($td);
		// if(cellClass.rowIndex>)
		// console.log(event);
	};
	
	checkForUnmerge() {
		let mergedCells = [];
		CaTable.selectedCellsSet.forEach(cell => cell.mergedCellsSet.size > 1 && mergedCells.push(cell));
		if ((mergedCells).length === 1) {
			if (Utility.isSetsEqual(mergedCells[0].mergedCellsSet, CaTable.selectedCellsSet)) {
				CaTable.allowUnmerge(true);
			} else {
				CaTable.allowUnmerge(false);
			}
		}
	}
	
	checkToCreateRow($aboveTr) {
		let rowIndex = $aboveTr.rowIndex;
		let mergedCell;
		if ((mergedCell = this.$tbody.querySelector(`tr:nth-child(${rowIndex + 1}) td[prevent-add-row-before]`))) {
			
			while (mergedCell) {
				mergedCell = this.getCellFromHTMLTableCellElement(mergedCell);
				CaTable.unMergeCells(mergedCell.mergedTo || mergedCell);
				mergedCell = this.$tbody.querySelector(`tr:nth-child(${rowIndex + 1}) td[prevent-add-row-before]`);
			}
			this.createRow(rowIndex, $aboveTr);
			
			window.createPopup("caution", "add_row_will_unmerge_one_or_more_cells", true, () => {
				while (mergedCell) {
					mergedCell = this.getCellFromHTMLTableCellElement(mergedCell);
					CaTable.unMergeCells(mergedCell.mergedTo || mergedCell);
					mergedCell = this.$tbody.querySelector(`tr:nth-child(${rowIndex + 1}) td[prevent-add-row-before]`);
				}
				
				this.createRow(rowIndex, $aboveTr);
			});
			
		} else {
			this.createRow(rowIndex, $aboveTr);
		}
	}
	
	checkToRemoveRow($tr) {
		let mergedCell;
		if ((mergedCell = $tr.querySelector(`td[prevent-add-row-before],td[rowspan]:not([rowspan="1"])`))) {
			while (mergedCell) {
				mergedCell = this.getCellFromHTMLTableCellElement(mergedCell);
				CaTable.unMergeCells(mergedCell.mergedTo || mergedCell);
				mergedCell = $tr.querySelector(`td[prevent-add-row-before],td[rowspan]:not([rowspan="1"])`);
			}
			
			this.removeRow($tr);
			window.createPopup("caution", "remove_row_will_unmerge_one_or_more_cells_and_may_delete_fields", true, () => {
				while (mergedCell) {
					mergedCell = this.getCellFromHTMLTableCellElement(mergedCell);
					CaTable.unMergeCells(mergedCell.mergedTo || mergedCell);
					mergedCell = $tr.querySelector(`td[prevent-add-row-before],td[rowspan]:not([rowspan="1"])`);
				}
				
				this.removeRow($tr);
			});
			
		} else {
			this.removeRow($tr);
		}
	}
	
	createRow(rowIndex, $aboveTr) {
		this.$json.rowCount++;
		this.$raSetting.querySelector("[name=\"row_count\"]").value++;
		const rowArray = [];
		const $newTr = Utility.createElement("TR");
		let $rowTh = $newTr.appendChild(Utility.createElement("TH", {}, `<div class="name">${rowIndex + 1}</div>`));
		let $addRow = $rowTh.appendChild(Utility.createElement("I", {class: "icon-plus-circle-B add-row"}));
		let $removeRow = $rowTh.appendChild(Utility.createElement("I", {class: "icon-times-circle-B remove-row"}));
		
		$addRow.addEventListener("click", () => this.checkToCreateRow($newTr));
		$removeRow.addEventListener("click", () => this.checkToRemoveRow($newTr));
		
		if (rowIndex === this.$json.rowCount + 1) {
			this.$tbody.appendChild($newTr);
		} else if (rowIndex === 0) {
			this.$tbody.prepend($newTr);
		} else {
			$newTr.appendAfter($aboveTr);
		}
		for (let columnIndex = 0; columnIndex < this.$json.columnCount; columnIndex++) {
			rowArray.push(new CaTableCell($newTr, this));
		}
		for (let rowLoop = rowIndex + 2; rowLoop < this.$json.rowCount + 1; rowLoop++) {
			this.$tbody.querySelector(`tr:nth-child(${rowLoop}) th .name`).innerHTML = rowLoop;
		}
		this.caTableCellsArray.splice(rowIndex, 0, rowArray);
	}
	
	removeRow($tr) {
		let rowIndex = $tr.rowIndex;
		this.$json.rowCount--;
		this.$raSetting.querySelector("[name=\"row_count\"]").value--;
		let removedRow = this.caTableCellsArray.splice(rowIndex - 1, 1)[0];
		for (let i = 0; i < removedRow.length; i++) {
			removedRow[i].fieldClass && removedRow[i].fieldClass.removeField();
		}
		$tr.parentNode.removeChild($tr);
		for (let rowLoop = rowIndex; rowLoop < this.$json.rowCount + 1; rowLoop++) {
			this.$tbody.querySelector(`tr:nth-child(${rowLoop}) th .name`).innerHTML = rowLoop;
		}
		
	}
	
	checkToCreateColumn($prevCell) {
		let columnIndex = $prevCell.cellIndex;
		let mergedCell;
		console.log(columnIndex + 2);
		if ((mergedCell = this.$tbody.querySelector(`tr>td:nth-child(${columnIndex + 2})[prevent-add-column-before]`))) {
			console.log(mergedCell);
			
			while (mergedCell) {
				mergedCell = this.getCellFromHTMLTableCellElement(mergedCell);
				CaTable.unMergeCells(mergedCell.mergedTo || mergedCell);
				mergedCell = this.$tbody.querySelector(`td:nth-child(${columnIndex + 1})[prevent-add-column-before]`);
			}
			this.createColumn(columnIndex);
			
			window.createPopup("caution", "add_row_will_unmerge_one_or_more_cells", true, () => {
				
				while (mergedCell) {
					mergedCell = this.getCellFromHTMLTableCellElement(mergedCell);
					CaTable.unMergeCells(mergedCell.mergedTo || mergedCell);
					mergedCell = this.$tbody.querySelector(`td:nth-child(${columnIndex + 1})[prevent-add-column-before]`);
				}
				this.createColumn(columnIndex);
			});
			
		} else {
			this.createColumn(columnIndex);
		}
	}
	
	checkToRemoveColumn(columnIndex) {
		let mergedCell;
		if ((mergedCell = this.$tbody.querySelector(`td:nth-child(${columnIndex})[prevent-add-row-before],td[colspan]:not([colspan="1"])`))) {
			
			while (mergedCell) {
				mergedCell = this.getCellFromHTMLTableCellElement(mergedCell);
				CaTable.unMergeCells(mergedCell.mergedTo || mergedCell);
				mergedCell = this.$tbody.querySelector(`td:nth-child(${columnIndex})[prevent-add-row-before],td[colspan]:not([colspan="1"])`);
			}
			this.removeColumn(columnIndex);
			
			window.createPopup("caution", "remove_row_will_unmerge_one_or_more_cells_and_may_delete_fields", true, () => {
				
				while (mergedCell) {
					mergedCell = this.getCellFromHTMLTableCellElement(mergedCell);
					CaTable.unMergeCells(mergedCell.mergedTo || mergedCell);
					mergedCell = this.$tbody.querySelector(`td:nth-child(${columnIndex})[prevent-add-row-before],td[colspan]:not([colspan="1"])`);
				}
				this.removeColumn(columnIndex);
				
			});
			
		} else {
			this.removeColumn(columnIndex);
		}
	}
	
	createColumn(columnIndex) {
		this.$json.columnCount++;
		this.$raSetting.querySelector("[name=\"column_count\"]").value++;
		let $columnTh = Utility.createElement("TH", {},
				`<div class="name">${Utility.convertNumberToLetterIndex(columnIndex)}</div>`);
		$columnTh.appendAfter(this.$thead.querySelector(`th:nth-child(${columnIndex + 1})`));
		let $addColumn = $columnTh.appendChild(Utility.createElement("I", {class: "icon-plus-circle-B add-column"}));
		let $removeColumn = $columnTh.appendChild(Utility.createElement("I", {class: "icon-times-circle-B remove-column"}));
		
		$addColumn.addEventListener("click", () => this.checkToCreateColumn($columnTh));
		$removeColumn.addEventListener("click", () => this.checkToRemoveColumn(columnIndex + 1));
		
		for (let rowIndex = 0; rowIndex < this.$json.rowCount; rowIndex++) {
			let $tr = this.$tbody.children[rowIndex];
			let cell = new CaTableCell($tr, this);
			$tr.removeChild($tr.lastChild);
			cell.$caElement = $tr.insertCell(columnIndex + 1);
			this.caTableCellsArray[rowIndex].splice(columnIndex, 0, cell);
		}
		let $theadTr = this.$thead.firstChild;
		for (let columnLoop = columnIndex + 2; columnLoop < this.$json.columnCount + 2; columnLoop++) {
			$theadTr.querySelector(`th:nth-child(${columnLoop}) .name`).innerHTML = Utility.convertNumberToLetterIndex(columnLoop - 2);
		}
		
		
	}
	
	removeColumn(columnIndex) {
		console.log(columnIndex);
		this.$json.columnCount--;
		this.$raSetting.querySelector("[name=\"column_count\"]").value--;
		let $theadTr = this.$thead.firstChild;
		let removedCell;
		$theadTr.deleteCell(columnIndex+1);
		
		for (let i = 0; i < this.$json.rowCount; i++) {
			
			(removedCell = this.caTableCellsArray[i].splice(columnIndex, 1)[0]).fieldClass && removedCell.fieldClass.removeField();
			removedCell.$caElement.parentNode.deleteCell(columnIndex+1);
		}
		for (let columnLoop = columnIndex+2; columnLoop < this.$json.columnCount + 2; columnLoop++) {
			$theadTr.querySelector(`th:nth-child(${columnLoop}) .name`).innerHTML = Utility.convertNumberToLetterIndex(columnLoop - 2);
		}
		
	}
	
}

CaTable.selectedCellsSet = new Set();

CaTable.activeElement = null;

CaTable.marqueeSelectMode = false;

CaTable.marqueeSelectPoints = {columnStart: 0, rowStart: 0, columnEnd: 0, rowEnd: 0};

CaTable.StartCell = null;

CaTable.mergeAllowed = true;

CaTable.unmergeAllowed = false;

CaTable.baseJson = {
	name: "Table Name",
	description: "",
	isCollapsed: false,
	visibilityFormula: "",
	rowCount: 2,
	columnCount: 2,
	
	viewItems: [],
};
