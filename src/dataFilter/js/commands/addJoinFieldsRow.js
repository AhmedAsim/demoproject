import {JoinFieldsRow} from "../TablesRowTemp/joinFieldsRow";

export class AddJoinFieldsRow {
    constructor(customObjects , parentTable){
        this.parentTable = parentTable;
        this.newJoinFieldsRow = new JoinFieldsRow(customObjects , parentTable);
    }

    doo() {
        this.parentTable.table.appendChild(this.newJoinFieldsRow.html.joinFieldsRow);
    }

    undo(){
        this.parentTable.table.removeChild(this.newJoinFieldsRow.html.joinFieldsRow);
    }
}