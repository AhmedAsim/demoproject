import {Utility} from "../../common/js/utility";
import {SelectFieldsRow} from "./TablesRowTemp/selectFieldsRow";
import {AddSelectFieldsRow} from "./commands/addSelectFieldsRow";


export class SelectFieldsTable {
    constructor(customObjects) {
        this.customObjects = customObjects;
        this.table = "";
        this.selectFieldsFrom = "" ;
        this.selectFields = [];
        this.html = this.draw();
    }

    draw() {
        let self = this;
        let selectFieldsTable = Utility.createElement("div", {"class": "section select-fields"});

        //region Header Elements
        let header = Utility.createElement("div", {"class": "header"});
        selectFieldsTable.appendChild(header);
        let collapseButton = Utility.createElement("button", {"class": "collapse icon-caret-down-B"});
        let headerName = Utility.createElement("div", {"class": "name"}, "Select Fields");
        let addButton = Utility.createElement("button", {"class": "icon-plus-B"});
        header.appendChild(collapseButton);
        header.appendChild(headerName);
        header.appendChild(addButton);
        //endregion

        //region Table Elements
        let tableCollapseWrapper = Utility.createElement("div",{"class":"table-collapse-wrapper"});
        selectFieldsTable.appendChild(tableCollapseWrapper);
        let tableWrapper = Utility.createElement("div",{"class":"table-wrapper"});
        tableCollapseWrapper.appendChild(tableWrapper);
        let table = Utility.createElement("table");
        tableWrapper.appendChild(table);
        this.table = table;

        let tableHeaderRow = Utility.createElement("tr");
        table.appendChild(tableHeaderRow);
        let customObjectsHeader = Utility.createElement("th",{},"Custom Objects");
        let fieldHeader = Utility.createElement("th",{},"Field");
        let verticalAlignmentHeader = Utility.createElement("th",{},"Vertical Alignment");
        let horizontalAlignmentHeader = Utility.createElement("th",{},"Horizontal Alignment");
        let widthHeader = Utility.createElement("th",{},"Value");
        tableHeaderRow.appendChild(customObjectsHeader);
        tableHeaderRow.appendChild(fieldHeader);
        tableHeaderRow.appendChild(verticalAlignmentHeader);
        tableHeaderRow.appendChild(horizontalAlignmentHeader);
        tableHeaderRow.appendChild(widthHeader);

        table.appendChild((new SelectFieldsRow(self.customObjects ,self )).html.selectFieldsRow);
        //endregion


        // region Header Behaviour
        collapseButton.addEventListener("click" , function () {
            Utility.slideToggle(tableCollapseWrapper);
            collapseButton.classList.toggle("active");
        });
        //endregion

        //region Table Behaviour
        let rows = table.getElementsByTagName('tr');
        let deleteRowButtons = table.querySelectorAll(".delete-row");
        let sqlHandler = function(){
            self.selectFieldsFrom = "" ;
            self.selectFields = [];
            for (let  i = 1; i < rows.length ; i++){
                let obj = rows[i].querySelector(".custom-object-select-wrapper select").value;
                let col = rows[i].querySelector(".field-select-wrapper select").value;

                self.selectFieldsFrom = obj;
                self.selectFields.push(`"${obj}"."${col}"`);
            }
        };

        addButton.addEventListener("click",function () {
            new AddSelectFieldsRow(self.customObjects , self).doo();
            rows = [];
            rows = table.getElementsByTagName('tr') ;

            deleteRowButtons = table.querySelectorAll(".delete-row");
            deleteRowButtons.forEach(e => e.addEventListener("click" , function () {
                rows = table.getElementsByTagName('tr') ;
                sqlHandler();
            }));
            sqlHandler();
        });
        deleteRowButtons.forEach(e => e.addEventListener("click" , function () {
            rows = table.getElementsByTagName('tr') ;
            sqlHandler();
        }));

        table.addEventListener("input" , function () {

            sqlHandler();
        });

        //endregion

        return {selectFieldsTable : selectFieldsTable}
    }
}