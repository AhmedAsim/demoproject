import {Utility} from "../../../../common/js/utility";
import {CaTable} from "./CaTable";
import {ViewDesignerCaAbstract} from "../../abstracts/ViewDesignerCaAbstract";

export class CaField extends ViewDesignerCaAbstract {
	constructor($json, parentTableClass, laField) {
		super();
		this.$json = $json;
		
		this.laField = laField;
		
		this.parentTableClass = parentTableClass;
		
		this.parentCellClass = this.parentTableClass.getCellFromRowColumn($json.rowIndex, $json.columnIndex);
		
		this.parentCellClass.fieldClass = this;
		
		this.parentCellClass.dropAllowed = false;
		
		this.$parent = this.parentCellClass.$caElement;
		
		this.$caElement = null;
		
		this.$raElement = null;
		
		this.createCaElement();
		
		this.createRaElement();
		
		let {rowIndex, columnIndex, rowSpan, colSpan} = this.$json;
		
		CaTable.mergeCells(this.parentCellClass, rowIndex, columnIndex, rowSpan, colSpan);
		
		this.$parent.appendChild(this.$caElement);
		
		CaField.addedFieldsUid[this.$json.fieldUid] = this;
	}
	
	static removeActive() {
		CaField.activeElement && CaField.activeElement.$field.classList.remove("active");
		CaField.activeElement = null;
	}
	
	static addActive(element) {
		CaField.activeElement = element;
		CaField.activeElement.$field.classList.add("active");
	}
	
	
	createCaElement() {
		this.$caElement = Utility.createElement("DIV", {class: "td-content"});
		this.$field = this.$caElement.appendChild(Utility.createElement("DIV", {
			class: "ca-field",
			draggable: true,
		}));
		let $icon = this.$field.appendChild(Utility.createElement("BUTTON", {class: "icon-star-B"}));
		this.$name = this.$field.appendChild(Utility.createElement("DIV", {class: "name"}, this.$json.formulaName));
		let $trash = this.$field.appendChild(Utility.createElement("BUTTON", {class: "icon-trash-alt-B"}));
		
		this.$caElement.addEventListener("click", () => this.appendRaSettings());
		
		$trash.addEventListener("click", () => {
			window.showPopup("are_you_sure", "it_will_go_to_left_side", true, () => this.removeField.call(this));
			
		});
		
		
		this.$field.addEventListener("dragstart", (e) => {
			ViewBuilder.draggedField = this;
			console.log("drag start");
		});
		
		this.$field.addEventListener("dragend", (e) => {
			ViewBuilder.draggedField = null;
			console.log("drag end");
			e.preventDefault();
		});
		
	}
	
	removeField() {
		console.log("field");
		try { window.hidePopup();} catch (e) { }
		let jsonIndex = this.parentTableClass.$json.viewItems.indexOf(this.$json);
		this.parentTableClass.$json.viewItems.splice(jsonIndex, 1);
		let caFieldIndex = this.parentTableClass.caTableFieldsArray.indexOf(this);
		this.parentTableClass.caTableFieldsArray.splice(caFieldIndex, 1);
		this.parentCellClass.$caElement.innerHTML = "";
		this.laField && this.laField.show();
		this.parentCellClass.dropAllowed = true;
		requestAnimationFrame(() => this.parentTableClass.$raSettingWrapper.innerHTML = "");
		
	};
	
	appendRaSettings() {
		if (CaField.activeElement === this) {
			return;
		}
		
		if (CaField.activeElement) {
			CaField.removeActive();
		}
		
		CaField.addActive(this);
		this.parentTableClass.appendRaSettings(this.$raElement);
		this.$raSetting.style.height = "auto";
	};
	
	createRaElement() {
		this.$raElement = Utility.createElement("DIV", {class: "field-wrapper"});
		this.$raHeader = this.$raElement.appendChild(Utility.createElement("DIV", {class: "field-header"}, this.$json.label));
		this.$raSetting = this.$raElement.appendChild(Utility.createElement("DIV", {class: "field-settings"}));
		this.$raSettingWrapper = this.$raElement.appendChild(Utility.createElement("DIV"));
		
		this.$raHeader.addEventListener("click", () => Utility.slideToggle(this.$raSetting));
		
		ViewBuilder.raSettingsFields.ViewItemFields().forEach(this.createRaSettingsField.bind(this));
		switch (this.$json.viewItemType) {
			case "Field":
				ViewBuilder.raSettingsFields.FieldViewItemFields().forEach(this.createRaSettingsField.bind(this));
				break;
			case "Template":
				ViewBuilder.raSettingsFields.TemplateViewItemFields().forEach(this.createRaSettingsField.bind(this));
				break;
			case "FxField":
				ViewBuilder.raSettingsFields.FxFieldViewItemFields().forEach(this.createRaSettingsField.bind(this));
				break;
		}
		
	}
	
	
}

CaField.activeElement = null;
CaField.addedFieldsUid = {};