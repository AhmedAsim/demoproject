import {Job} from "../job";
import {CommandAbstract} from "../../../common/js/commandAbstract";
import {JobWizardAbstract} from "../jobWizardAbstract";
import {Utility} from "../../../common/js/utility";


export class AddJobCommand extends CommandAbstract{

    constructor(jobParent , addToStack = true){
        super();
        this.jobParent =  jobParent;
        this.newJob = new Job(jobParent.jobsObj, JobWizardAbstract.getDefaultJob());
        this.leftParent = this.jobParent.la.children[1];
        this.rightParent = this.jobParent.ra.children[2];
        this.centerParent = this.jobParent.da.children[1].children[0].children[0];
        this.doo();
        if (addToStack){
            this.addToStack();
        }

    }


    doo(){

        this.newJob.parentObj.push(this.newJob.jobObj);
        this.leftParent.appendChild(this.newJob.html.left);
        this.rightParent.appendChild(this.newJob.html.right);
        this.centerParent.appendChild(this.newJob.html.center);


    }

    undo(){
        this.newJob.parentObj.splice(-1,1);
        this.leftParent.removeChild(this.newJob.html.left);
        this.rightParent.removeChild(this.newJob.html.right);
        this.centerParent.removeChild(this.newJob.html.center);


    }
}