import {Utility} from "../../common/js/utility";
import {OrderByRow} from "./TablesRowTemp/orderByRow";
import {AddOrderByRow} from "./commands/addOrderByRow";

export class OrderByTable {
    constructor(customObjects) {
        this.customObjects = customObjects;
        this.table = "";
        this.orders = [];
        this.html = this.draw();
    }

    draw() {
        let self = this;
        let orderByTable = Utility.createElement("div", {"class": "section"});

        //region Header Elements
        let header = Utility.createElement("div", {"class": "header"});
        orderByTable.appendChild(header);
        let collapseButton = Utility.createElement("button", {"class": "collapse icon-caret-down-B"});
        let headerName = Utility.createElement("div", {"class": "name"}, "Orderby Fields");
        let addButton = Utility.createElement("button", {"class": "icon-plus-B"});
        header.appendChild(collapseButton);
        header.appendChild(headerName);
        header.appendChild(addButton);
//endregion

        //region Table Elements
        let tableCollapseWrapper = Utility.createElement("div",{"class":"table-collapse-wrapper"});
        orderByTable.appendChild(tableCollapseWrapper);
        let tableWrapper = Utility.createElement("div",{"class":"table-wrapper"});
        tableCollapseWrapper.appendChild(tableWrapper);
        let table = Utility.createElement("table");
        this.table = table;
        tableWrapper.appendChild(table);

        let tableHeaderRow = Utility.createElement("tr");
        table.appendChild(tableHeaderRow);
        let orderByHeader = Utility.createElement("th",{},"Order by");
        let ascDescHeader = Utility.createElement("th",{},"asc desc");
        tableHeaderRow.appendChild(orderByHeader);
        tableHeaderRow.appendChild(ascDescHeader);

        table.appendChild((new OrderByRow(self.customObjects ,self )).html.orderByRow);
        //endregion

        // region Header Behaviour
        collapseButton.addEventListener("click" , function () {
            Utility.slideToggle(tableCollapseWrapper);
            collapseButton.classList.toggle("active");
        });
        //endregion

        //region Table Behaviour
        let rows = table.getElementsByTagName('tr');
        let deleteRowButtons = table.querySelectorAll(".delete-row");
        let sqlHandler = function(){
            self.orders = [];

            for (let  i = 1; i < rows.length ; i++){
                let obj = rows[i].querySelector(".order-by-wrapper select").value;
                let col = rows[i].querySelector(".asc-desc-wrapper select").value;
                self.orders.push(`"${obj}"."${col}"`);
            }
        };
        addButton.addEventListener("click",function () {
            new AddOrderByRow(self.customObjects , self).doo();

            rows = [];
            rows = table.getElementsByTagName('tr') ;

            deleteRowButtons = table.querySelectorAll(".delete-row");
            deleteRowButtons.forEach(e => e.addEventListener("click" , function () {
                rows = table.getElementsByTagName('tr') ;
                sqlHandler();
            }));
            sqlHandler();
        });
        deleteRowButtons.forEach(e => e.addEventListener("click" , function () {
            rows = table.getElementsByTagName('tr') ;
            sqlHandler();
        }));

        table.addEventListener("input" , function () {

            sqlHandler();
        });

        //endregion

        return {orderByTable: orderByTable}
    }
}