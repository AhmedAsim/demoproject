import {Utility} from "../../../common/js/utility";

export class SelectFieldsRow {
    constructor(customObjects , parentTable) {
        this.customObjects = customObjects;
        this.parentTable = parentTable;
        this.html = this.draw();
    }

    draw() {
        let self = this;

        //region Elements Creation

        let selectFieldsRow = Utility.createElement("tr"); //ToDo conditions json or array to decide number of table rows????!!

        let customObjectSelectCell = Utility.createElement("td");
        selectFieldsRow.appendChild(customObjectSelectCell);
        let deleteRowButton = Utility.createElement("div",{"class":"delete-row icon-times-B"});
        customObjectSelectCell.appendChild(deleteRowButton);
        let customObjectSelectWrapper = Utility.createElement("div",{"class":"custom-object-select-wrapper"});
        customObjectSelectCell.appendChild(customObjectSelectWrapper);
        let customObjectSelect = Utility.createElement("select");
        customObjectSelectWrapper.appendChild(customObjectSelect);
        let dummyCustomOption = Utility.createElement("option",{"disabled":"", "selected":"", "value":""},"-- select option --");
        customObjectSelect.appendChild(dummyCustomOption);
        for (let customItem of self.customObjects.item){
            let customItemOption = Utility.createElement("option",{"value":customItem.name}, customItem.name);
            customObjectSelect.appendChild(customItemOption);
        }

        let fieldSelectCell = Utility.createElement("td");
        selectFieldsRow.appendChild(fieldSelectCell);
        let fieldSelectWrapper = Utility.createElement("div",{"class":"field-select-wrapper"});
        fieldSelectCell.appendChild(fieldSelectWrapper);
        let fieldSelect = Utility.createElement("select");
        fieldSelectWrapper.appendChild(fieldSelect);
        let dummyFieldOption = Utility.createElement("option",{"disabled":"", "selected":"", "value":""},"-- select option --");
        fieldSelect.appendChild(dummyFieldOption);
        customObjectSelect.addEventListener("change", function () {
            fieldSelect.innerHTML = "";
            fieldSelect.appendChild(dummyFieldOption);
            let activeFieldsItems = [];
            for (let customItem of self.customObjects.item){
                if (customItem.name === customObjectSelect.value){
                    activeFieldsItems = customItem.fields;
                }
            }
            if (activeFieldsItems.length > 0){
                for (let fieldItem of activeFieldsItems){
                    let fieldItemOption = Utility.createElement("option",{"value":fieldItem.name},fieldItem.name);
                    fieldSelect.appendChild(fieldItemOption);
                }
            }
        });


        let verticalAlignmentCell = Utility.createElement("td");
        selectFieldsRow.appendChild(verticalAlignmentCell);
        let verticalAlignmentWrapper = Utility.createElement("div",{"class":"vertical-alignment-wrapper"});
        verticalAlignmentCell.appendChild(verticalAlignmentWrapper);
        let leftVerticalLabel = Utility.createElement("label",{},"Left");
        let leftVerticalInput = Utility.createElement("input",{"type":"radio","name":"vertical-alignment"});
        let centerVerticalLabel = Utility.createElement("label",{},"Center");
        let centerVerticalInput = Utility.createElement("input",{"type":"radio","name":"vertical-alignment"});
        let rightVerticalLabel = Utility.createElement("label",{},"Right");
        let rightVerticalInput = Utility.createElement("input",{"type":"radio","name":"vertical-alignment"});
        verticalAlignmentWrapper.appendChild(leftVerticalLabel);
        verticalAlignmentWrapper.appendChild(leftVerticalInput);
        verticalAlignmentWrapper.appendChild(centerVerticalLabel);
        verticalAlignmentWrapper.appendChild(centerVerticalInput);
        verticalAlignmentWrapper.appendChild(rightVerticalLabel);
        verticalAlignmentWrapper.appendChild(rightVerticalInput);

        let horizontalAlignmentCell = Utility.createElement("td");
        selectFieldsRow.appendChild(horizontalAlignmentCell);
        let horizontalAlignmentWrapper = Utility.createElement("div",{"class":"horizontal-alignment-wrapper"});
        horizontalAlignmentCell.appendChild(horizontalAlignmentWrapper);
        let leftHorizontalLabel = Utility.createElement("label",{},"Left");
        let leftHorizontalInput = Utility.createElement("input",{"type":"radio","name":"horizontal-alignment"});
        let centerHorizontalLabel = Utility.createElement("label",{},"Center");
        let centerHorizontalInput = Utility.createElement("input",{"type":"radio","name":"horizontal-alignment"});
        let rightHorizontalLabel = Utility.createElement("label",{},"Right");
        let rightHorizontalInput = Utility.createElement("input",{"type":"radio","name":"horizontal-alignment"});
        horizontalAlignmentWrapper.appendChild(leftHorizontalLabel);
        horizontalAlignmentWrapper.appendChild(leftHorizontalInput);
        horizontalAlignmentWrapper.appendChild(centerHorizontalLabel);
        horizontalAlignmentWrapper.appendChild(centerHorizontalInput);
        horizontalAlignmentWrapper.appendChild(rightHorizontalLabel);
        horizontalAlignmentWrapper.appendChild(rightHorizontalInput);


        let widthCell = Utility.createElement("td");
        selectFieldsRow.appendChild(widthCell);
        let widthWrapper = Utility.createElement("div",{"class":"width-wrapper"});
        widthCell.appendChild(widthWrapper);
        let widthInput = Utility.createElement("input",{"type":"text"});
        widthWrapper.appendChild(widthInput);
        //endregion

        //region Behaviour
        deleteRowButton.addEventListener("click" , function () {
            self.parentTable.table.removeChild(self.html.selectFieldsRow);
        });
        //endregion

        return {selectFieldsRow : selectFieldsRow}
    }
}