import {AddJobCommand} from "./addJobCommand";
import {SelectActionCommand} from "./selectActionCommand";
import {SelectJobCommand} from "./selectJobCommand";
import {SelectActionGroupCommand} from "./selectActionGroupCommand";
import {CommandAbstract} from "../../../common/js/commandAbstract";

export class AddJobWithSelection extends CommandAbstract{

    constructor(jobParent){
        super();
        this.jobParent =  jobParent;
        this.newAddedJobCommand = new AddJobCommand(this.jobParent, false);
        this.newSelectedJob = new SelectJobCommand(this.newAddedJobCommand.newJob, false);
        this.newSelectedAG = new SelectActionGroupCommand(this.newAddedJobCommand.newJob._initAG , false);
        this.newSelectedAction = new SelectActionCommand(this.newAddedJobCommand.newJob._initAG._initAction , false);
        this.doo();
        this.addToStack();


    }

    doo(){
        this.newAddedJobCommand.doo();
        this.newSelectedJob.doo();
        this.newSelectedAG.doo();
        this.newSelectedAction.doo();
    }

    undo(){
        this.newSelectedAction.undo();
        this.newSelectedAG.undo();
        this.newSelectedJob.undo();
        this.newAddedJobCommand.undo();
    }
}