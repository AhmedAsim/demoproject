import "../../fonts/fa-solid-900.woff";
import "../css/commonView.scss";
import {Header} from "./header";
import {Content} from "./content";
import {Footer} from "./footer";

export class CommonView {
  
  constructor(headerObj, appName, options = {}) {
    
    this.options = {
      activeAreas: Object.assign({right: true, left: true}, options.activeAreas),
      containers: Object.assign(
        {
          header: document.getElementsByTagName("header")[0],
          footer: document.getElementsByTagName("footer")[0],
          content: document.querySelector("body>.main"),
          body: document.getElementsByTagName("body")[0],
        }, options.containers),
    };
    
    this.headerObj = headerObj;
    this.appName = appName;
    
    this.$body = this.options.containers.body;
    this.$header = this.options.containers.header;
    this.$footer = this.options.containers.footer;
    this.$content = this.options.containers.content;
    
    this.$body.style.height = "100vh";
    this.$body.style.display = "flex";
    this.$body.style.flexDirection = "column";
    
    this._header = this.$header && new Header(this.$header, this.headerObj, this.appName);
    if (this.$content) {
      this._content = new Content(this.$content, this.options.activeAreas.right, this.options.activeAreas.left);
      this.centerArea = this._content.center;
      this.leftArea = this._content.left; // style: flex, column
      this.rightArea = this._content.right; // style: flex, column
    }
    
    this._footer = this.$footer && new Footer(this.$footer, this.$header, this.leftArea, this.rightArea);
    
    this._header && this._header.onLoad();
    window.addEventListener("resize", () => this._header && this._header.onResize());
  }
  
  addNavItem(itemObj) {
    this._header.addItemToNav(itemObj);
    this._header.onResize();
  }
  
  
}

window.CommonView = CommonView;

