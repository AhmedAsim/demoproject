export class SettingsFields {

    _hidden(name) {
        return {
            'name': name,
            'data': name,
            'type': 'hidden'
        };
    }

    _textConditional(label, data, conditionalField) {
        return {
            'label': label,
            'data': data,
            'type': 'text',
            'conditionalField': conditionalField
        };
    }

    _text(label, data, readonly) {
        return {
            'label': label,
            'data': data,
            'type': 'text',
            'readonly': readonly
        };
    }

    _textarea(label, data, readonly) {
        return {
            'label': label,
            'data': data,
            'type': 'textarea',
            'readonly': readonly
        };
    }

    _number(label, data, readonly) {
        return {
            'label': label,
            'data': data,
            'type': 'number',
            'readonly': readonly
        };
    }

    _checkbox(label, data, readonly) {
        return {
            'label': label,
            'data': data,
            'type': 'checkbox',
            'readonly': readonly
        };
    }

    _selectConditional(label, data, dataText, url, parent, conditionalField, selectType) {
        return {
            'label': label,
            'data': data,
            'dataText': dataText,
            'type': 'select',
            'url': url,
            'parent': parent,
            'conditionalField': conditionalField,
            'selectType': selectType
        };
    }

    _select(label, dataPrefix, url, parent) {
        return {
            'label': label,
            'dataPrefix': dataPrefix,
            'type': 'select',
            'url': url,
            'parent': parent
        };
    }

    _dataTable(label, data, rowInputs) {
        return {
            'label': label,
            'data': data,
            'type': 'datatable',
            'rowInputs': rowInputs
        };
    }

    name() { return this._text('name', 'Name'); }
    description() { return this._textarea('description', 'Description'); }
}