import {Utility} from "../../common/js/utility";
import {ActionGroup} from "./actionGroup";
import {JobWizardAbstract} from "./jobWizardAbstract";
import {RemoveJobCommand} from "./commands/removeJobCommand";
import {AddActionGroupCommand} from "./commands/addActionGroupCommand";
import {SelectJobCommand} from "./commands/selectJobCommand";
import {CommandCollection} from "./commands/commandCollection";

export class Job extends JobWizardAbstract {

    constructor(parentObj, jobObj) {
        super();
        this.parentObj = parentObj;
        this.jobObj = jobObj;
        this.actionGroupsArray = [];
        this._AGArray = [];
        for (let i = 0; i < jobObj.jobActionGroups.length; i++) {
            let actionGroup = new ActionGroup(this, jobObj.jobActionGroups[i]);
            this._AGArray.push(actionGroup);
            this.actionGroupsArray.push(actionGroup.html);
        }
        this._initAG = this._AGArray[0];
        this.html = this.draw();
        console.log(this.html);
    }

    draw() {

        let self = this;

        //region Right Area Elements
        let raJobWrapper = Utility.createElement("div", {"class": "job-wrapper"});
        let raJobHeader = Utility.createElement("div", {"class": "job-header"}, self.jobObj.name);
        let raJobSettings = Utility.createElement("div", {"class": "job-settings"});
        let raInputFields = {};
        raJobWrapper.appendChild(raJobHeader);
        raJobWrapper.appendChild(raJobSettings);
        for (let key of Object.keys(self.jobObj)) {
            if (key !== "jobActionGroups" && Utility.typesMaping[key]) {
                let label = Utility.createElement("label", {}, key);
                raJobSettings.appendChild(label);
                raInputFields[key] = Utility.createElement(
                    Utility.typesMaping[key].Tag,
                    Object.assign(
                        (Utility.typesMaping[key].attributes ? Utility.typesMaping[key].attributes : {}), {"value": self.jobObj[key]}
                    )
                );
                raJobSettings.appendChild(raInputFields[key]);
            }
        }
        for (let i = 0; i < self.actionGroupsArray.length; i++) {
            raJobWrapper.appendChild(self.actionGroupsArray[i].right);
        }
        //endregion Right Area Elements

        //region Draw Area Elements
        let daJob = Utility.createElement("div", {"class": "job"});
        let daHeader = Utility.createElement("div", {"class": "header"});
        let daJobName = Utility.createElement("div", {"class": "name"}, self.jobObj.name);
        let daActionGroupAdd = Utility.createElement("button", {"class": "add icon-plus-B"});
        let daJobRemove = Utility.createElement("button", {"class": "remove icon-trash-alt-B"});
        daHeader.appendChild(daJobName);
        daHeader.appendChild(daActionGroupAdd);
        daHeader.appendChild(daJobRemove);
        daJob.appendChild(daHeader);
        let daActionGroups = Utility.createElement("div", {"class": "action-groups"});
        daJob.appendChild(daActionGroups);
        for (let i = 0; i < self.actionGroupsArray.length; i++) {
            daActionGroups.appendChild(self.actionGroupsArray[i].center);
        }
        //endregion Draw Area Elements

        //region Left Area Elements
        let laJob = Utility.createElement("div", {"class": "la-job"});
        let laHeader = Utility.createElement("div", {"class": "header"});
        let laCollapse = Utility.createElement("button", {"class": "collapse icon-caret-down-B"});
        let laJobName = Utility.createElement("h3", {}, self.jobObj.name);
        let laActionGroupAdd = Utility.createElement("button", {"class": "add icon-plus-B"});
        let laJobRemove = Utility.createElement("button", {"class": "remove icon-trash-alt-B"});
        laJob.appendChild(laHeader);
        laHeader.appendChild(laCollapse);
        laHeader.appendChild(laJobName);
        laHeader.appendChild(laActionGroupAdd);
        laHeader.appendChild(laJobRemove);
        let laActionGroups = Utility.createElement("div", {"class": "action-groups"});
        laJob.appendChild(laActionGroups);
        for (let i = 0; i < self.actionGroupsArray.length; i++) {
            laActionGroups.appendChild(self.actionGroupsArray[i].left);
        }
        //endregion Left Area Elements


        //region Right Area Behaviour
        raJobHeader.addEventListener("click", function (event) {
            event.stopPropagation();
            Utility.slideToggle(raJobSettings);
        });
        raInputFields.name.addEventListener("keydown", function () {
            this.dataset.prevValue = this.value;
        });
        raInputFields.name.addEventListener("input", function () {
            this.dataset.redoValue = this.value;
            CommandCollection.addTextAreaCommand(this, self);
        });

        if (raInputFields.description) {
            raInputFields.description.addEventListener("keydown", function () {
                this.dataset.prevValue = this.value;
            });
            raInputFields.description.addEventListener("input", function () {
                this.dataset.redoValue = this.value;
                CommandCollection.addTextAreaCommand(this, self);
            });

        }
        if (raInputFields.isActive)
            raInputFields.isActive.addEventListener("change", function () {
                self.jobObj.isActive = this.value;
            });
        //endregion Right Area Behaviour

        //region Draw Area Behaviour
        daHeader.addEventListener("click", function (event) {
            event.stopPropagation();

            new SelectJobCommand(self);
        });

        daActionGroupAdd.addEventListener("click", function (event) {
            event.stopPropagation();

            new SelectJobCommand(self);

            new AddActionGroupCommand(self);

            // self.jobObj["Action Groups"].push(newActionGroup.actionGroupObj);
            // daActionGroups.appendChild(newActionGroup.html.center);
            // raJobWrapper.appendChild(newActionGroup.html.right);
            // laActionGroups.appendChild(newActionGroup.html.left);
        });

        daJobRemove.addEventListener("click", function (event) {
            event.stopPropagation();
            new RemoveJobCommand(self);
        });
        //endregion Draw Area Behaviour

        //region Left Area Behaviour
        laHeader.addEventListener("click", function (event) {
            event.stopPropagation();
            new SelectJobCommand(self);
        });
        laActionGroupAdd.addEventListener("click", function (event) {
            event.stopPropagation();
            new SelectJobCommand(self);
            new AddActionGroupCommand(self);
        });
        laJobRemove.addEventListener("click", function (event) {
            event.stopPropagation();
            new RemoveJobCommand(self);
        });
        laCollapse.addEventListener("click", function (event) {
            event.stopPropagation();
            Utility.slideToggle(laActionGroups);
            laCollapse.classList.toggle("active");
        });
        //endregion Left Area Behaviour

        return {left: laJob, right: raJobWrapper, center: daJob};

    }

    setActive() {
        if (JobWizardAbstract.activeActionGroup) {
            JobWizardAbstract.activeActionGroup.removeActive();
            JobWizardAbstract.activeActionGroup = false;
        }
        if (JobWizardAbstract.activeAction) {
            JobWizardAbstract.activeAction.removeActive();
            JobWizardAbstract.activeAction = false;
        }
        if (JobWizardAbstract.activeJob === false) {
            JobWizardAbstract.activeJob = this;
            this._showHtml();
            Utility.expand(this.html.right.querySelector(".job-settings"));
        } else if (JobWizardAbstract.activeJob !== this) {
            JobWizardAbstract.activeJob.removeActive();
            JobWizardAbstract.activeJob = this;
            this._showHtml();
            Utility.expand(this.html.right.querySelector(".job-settings"));
        } else {
            Utility.expand(this.html.right.querySelector(".job-settings"));
        }
    }

    removeActive() {
        this._hideHtml();
        Utility.collapse(this.html.right.querySelector(".job-settings"));
    }


}