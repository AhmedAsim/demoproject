/**
 * Add event listener to the element with the same functionality as in jQuery.on();
 *
 * to use it
 * @example
 * element.on(event, selector, callbackFn);
 * //or
 * element.on(event, callbackFn);
 * @param event {string} the event name[s] separated with spaces
 * @param selector {string|function} optional the descendant elements which will receive the event and the {this} variable will be assigned to it
 * @param handler {function} the callback function that will be passed to the addEventListener
 */
Element.prototype.on = function (event, selector, handler) {
	if (!handler) {
		handler = selector;
		selector = null;
	}
	
	let self = this;
	event.split(" ").forEach(eventType => self.addEventListener(eventType, function (event) {
		if (selector) {
			self = event.target.closest(selector);
			if (!self) { return; }
		}
		handler.call(self, event);
	}, false));
};

Element.prototype.appendBefore = function (element) {
	element.parentNode.insertBefore(this, element);
};

Element.prototype.appendAfter = function (element) {
	element.parentNode.lastChild === element ? element.parentNode.appendChild(this) : this.appendBefore(element.nextSibling);
};

export class Utility {
	
	static slideToggle(element, time = 400, callback, callbackThis) {
		let elementHeight = element.scrollHeight;
		let state = parseInt(element.offsetHeight) === 0 ? "collapsed" : "expanded";
		element.style.transition = "height " + time + "ms";
		element.style.overflow = "hidden";
		switch (state) {
			case "collapsed":
				element.style.height = elementHeight + "px";
				element.addEventListener("transitionend", Utility._setHeightAuto);
				break;
			case "expanded":
				element.style.height = elementHeight + "px";
				requestAnimationFrame(function () {
					requestAnimationFrame(function () {
						element.style.height = "0";
					});
				});
				break;
		}
		if (callback) {
			let bindCallback = callback.bind(callbackThis || this);
			element.addEventListener("transitionend", bindCallback);
			setTimeout(() => element.removeEventListener(bindCallback), time + 10);
		}
	}
	
	static slideDown(element, time = 400, callback, callbackThis) {
		let elementHeight = element.scrollHeight;
		if (parseInt(element.offsetHeight) !== 0) { return; }
		element.style.transition = "height " + time + "ms";
		element.style.overflow = "hidden";
		element.style.height = elementHeight + "px";
		element.addEventListener("transitionend", Utility._setHeightAuto);
		if (callback) {
			let bindCallback = callback.bind(callbackThis || this);
			element.addEventListener("transitionend", bindCallback);
			setTimeout(() => element.removeEventListener(bindCallback), time + 10);
		}
	}
	
	static slideUp(element, time = 400, callback, callbackThis) {
		let elementHeight = element.scrollHeight;
		if (parseInt(element.offsetHeight) === 0) { return; }
		element.style.transition = "height " + time + "ms";
		element.style.overflow = "hidden";
		element.style.height = elementHeight + "px";
		requestAnimationFrame(function () {
			requestAnimationFrame(function () {
				element.style.height = "0";
			});
		});
		if (callback) {
			let bindCallback = callback.bind(callbackThis || this);
			element.addEventListener("transitionend", bindCallback);
			setTimeout(() => element.removeEventListener(bindCallback), time + 10);
		}
	}
	
	static _setHeightAuto() {
		if (parseInt(this.style.height) !== 0) {
			this.style.height = "auto";
		}
		this.removeEventListener("transitionend", Utility._setHeightAuto);
	}
	
	static createElement(tag, attributes, text) {
		let returnElement = document.createElement(tag);
		if (attributes) {
			for (let key of Object.keys(attributes)) {
				returnElement.setAttribute(key, attributes[key]);
			}
		}
		if (text !== undefined && text !== null)
			returnElement.innerHTML = text;
		return returnElement;
	}
	
	static expand(element) {
		element.style.height = "auto";
	}
	
	static collapse(element) {
		element.style.height = "0";
	}
	
	static offset(element) {
		let rect = element.getBoundingClientRect(),
				scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
				scrollTop = window.pageYOffset || document.documentElement.scrollTop;
		return {top: rect.top + scrollTop, left: rect.left + scrollLeft};
	}
	
	static convertNumberToLetterIndex(number) {
		let columnString = "";
		let columnNumber = number + 1;
		while (columnNumber > 0) {
			const currentLetterNumber = (columnNumber - 1) % 26;
			const currentLetter = String.fromCharCode(currentLetterNumber + 65);
			columnString = currentLetter + columnString;
			columnNumber = (columnNumber - (currentLetterNumber + 1)) / 26;
		}
		return columnString;
	}
	
	
	static isXBetweenRange = (x, range) => (x - range[0] ^ x - range[1]) < 0;
	
	static isSetsEqual(set1, set2) {
		if (set1.size !== set2.size) return false;
		for (let a of set1) if (!set2.has(a)) return false;
		return true;
	};
	
	static createSelectElement(baseName, value, text, url, type) {
		if (type === undefined) {
			type = "single";
		}
		
		return Utility.createElement("div", {
			"class": "select " + type + "-select",
			"id": "select-" + baseName,
			"name": "select-" + baseName,
			"data-name": baseName + "Uid",
			"data-name-text": baseName + "Name",
			"data-value": value,
			"data-text": text,
			"data-url": url,
		});
	}
	
	
	static createField($json, label, type, options) {
		// (fld, actionObj, raActionSettings, raInputFields) {
		// 	let label = Utility.createElement("label", { "data-translation": fld.label }, fld.label);
		// 	raActionSettings.appendChild(label);
		//
		// 	if (fld.type === "select") {
		// 		let selectElement = Utility.createSelectElement(fld.dataPrefix, actionObj[fld.dataPrefix + "Uid"] || '', actionObj[fld.dataPrefix + "Name"], fld.url);
		// 		raInputFields[fld.label] = selectElement;
		// 	} else if (fld.type === "textarea") {
		// 		let textareaInput = Utility.createElement('textarea');
		// 		textareaInput.innerHTML = actionObj[fld.data] || '';
		// 		raInputFields[fld.label] = textareaInput;
		// 	} else if (fld.type === "datatable") {
		//
		// 		let tInput = Utility.createElement('table');
		// 		tInput.style.border = 'solid 1px #333';
		// 		let tHead = Utility.createElement('thead');
		// 		let trHead = Utility.createElement('tr');
		// 		tHead.appendChild(trHead);
		// 		tInput.appendChild(tHead);
		// 		tInput.appendChild(Utility.createElement('tbody'));
		//
		// 		fld.rowInputs.forEach(function (x) {
		// 			let td = Utility.createElement('td');
		// 			td.appendChild(Utility.createElement('label', { 'data-translation': x.label }, x.label));
		//
		// 			if (x.type === 'select') {
		// 				let tSelect = Utility.createSelectElement(x.dataPrefix, '', '', x.url);
		// 				td.appendChild(tSelect);
		// 			} else if (x.type === 'text') {
		// 				td.appendChild(Utility.createElement('input', Object.assign({ "type": fld.type, "value": '' })));
		// 			} else {
		// 				throw x.type + " not supported!";
		// 			}
		//
		// 			trHead.appendChild(td);
		// 		});
		//
		// 		let theData = actionObj[fld.data];
		// 		//todo:fill data to tbody
		// 		//console.log(theData);
		// 		raInputFields[fld.label] = tInput;
		// 	} else if (fld.type === "radio") {
		//
		// 		//todo radio
		//
		// 	} else {
		// 		raInputFields[fld.label] = Utility.createElement('input', Object.assign({ "type": fld.type, "value": actionObj[fld.data] || '' }));
		// 	}
		//
		// 	raActionSettings.appendChild(raInputFields[fld.label]);
		// }
		return true;
	}
	
	
}


Utility.typesMaping = {
	"name": {"Tag": "input", "attributes": {"type": "text"}},
	"description": {"Tag": "textarea"},
	"isActive": {"Tag": "input", "attributes": {"type": "checkbox"}},
};

Utility.SETTINGS_FIELDS_TYPES = {
	textArea: "textarea",
	checkbox: "checkbox",
	
	
};
