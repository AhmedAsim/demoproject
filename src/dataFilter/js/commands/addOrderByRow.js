import {OrderByRow} from "../TablesRowTemp/orderByRow";

export class AddOrderByRow {
    constructor(customObjects , parentTable){
        this.parentTable = parentTable;
        this.newOrderByRow = new OrderByRow(customObjects , parentTable);
    }

    doo() {
        console.log("asim");
        this.parentTable.table.appendChild(this.newOrderByRow.html.orderByRow);
    }

    undo(){
        this.parentTable.table.removeChild(this.newOrderByRow.html.orderByRow);
    }
}