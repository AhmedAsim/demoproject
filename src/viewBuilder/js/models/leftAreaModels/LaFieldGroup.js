import {Utility} from "../../../../common/js/utility";
import {LaField} from "./LaField";

export class LaFieldGroup {
	constructor($json, index, expanded, hasHeader, parent) {
		this.$json = $json;
		this.tabName = $json["formulaName"];
		this.tabNumber = this.$json.fieldCount;
		this.$element = parent.appendChild(Utility.createElement("div", {"class": "la-field-group " + (expanded && "active"), "data-index": index}));
		this.$header = hasHeader ? this.$element.appendChild(Utility.createElement("div", {"class": "la-field-group-header"}, this.tabName)) : null;
		this.$tabNumber = this.$header && this.$header.appendChild(Utility.createElement("div", {"class": "number"}, this.tabNumber));
		this.$content = this.$element.appendChild(Utility.createElement("div", {"class": "la-field-group-content"}), this.tabName);
		this.children = this.$json["fields"]
				.filter(value => Object.keys(value).length)
				.map((value, index) => new LaField(value, this.$content, this));
	}
	
	changeTabNumber(value) {
		console.log(value);
		this.$tabNumber && (this.$tabNumber.innerHTML = value);
		this.tabName = value;
	}
	
}