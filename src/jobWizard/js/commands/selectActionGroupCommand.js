import {JobWizardAbstract} from "../jobWizardAbstract";
import {Utility} from "../../../common/js/utility";
import {CommandAbstract} from "../../../common/js/commandAbstract";

export class SelectActionGroupCommand extends CommandAbstract{

    constructor(actionGroup , addToStack = true){
        super();
        this.actionGroup = actionGroup;
        this.previousActive = false;
        this.previousActiveAction  = JobWizardAbstract.activeAction;
        this.previousactivejob = JobWizardAbstract.activeJob;
        this.isNew = this.doo();
        if(addToStack){
            this.addToStack(this.isNew);
        }
    }

    doo(){

        if(JobWizardAbstract.activeAction){
            JobWizardAbstract.activeAction._hideHtml();
            Utility.collapse(JobWizardAbstract.activeAction.html.right.querySelector(".action-settings"));
            JobWizardAbstract.activeAction = false;
        }
        if(JobWizardAbstract.activeJob !== this.actionGroup.job){
            if (JobWizardAbstract.activeJob){
                JobWizardAbstract.activeJob._hideHtml();
                Utility.collapse(JobWizardAbstract.activeJob.html.right.querySelector(".job-settings"));
            }
            this.actionGroup.job._showHtml();
            JobWizardAbstract.activeJob = this.actionGroup.job;
        }


        if(JobWizardAbstract.activeActionGroup === false){
            this.previousActive = JobWizardAbstract.activeActionGroup;
            JobWizardAbstract.activeActionGroup = this.actionGroup;
            this.actionGroup._showHtml();
            Utility.expand(this.actionGroup.html.right.querySelector(".action-group-settings"));
            Utility.collapse(this.actionGroup.job.html.right.querySelector(".job-settings"));
            return true;
        }

        else if(JobWizardAbstract.activeActionGroup !== this.actionGroup){
            this.previousActive = JobWizardAbstract.activeActionGroup;
            JobWizardAbstract.activeActionGroup._hideHtml();
            Utility.collapse(JobWizardAbstract.activeActionGroup.html.right.querySelector(".action-group-settings"));
            JobWizardAbstract.activeActionGroup = this.actionGroup;
            this.actionGroup._showHtml();
            Utility.expand(this.actionGroup.html.right.querySelector(".action-group-settings"));
            Utility.collapse(this.actionGroup.job.html.right.querySelector(".job-settings"));
            return true;
        }

        else{
            this.actionGroup._showHtml();
            Utility.expand(this.actionGroup.html.right.querySelector(".action-group-settings"));
            return false;
        }
    }

    undo(){


        this.actionGroup._hideHtml();
        Utility.collapse(this.actionGroup.html.right.querySelector(".action-group-settings"));

        JobWizardAbstract.activeActionGroup = this.previousActive;
        JobWizardAbstract.activeJob = this.previousActive.job;

        if (JobWizardAbstract.activeActionGroup){
            JobWizardAbstract.activeActionGroup._showHtml();
            Utility.expand(JobWizardAbstract.activeActionGroup.html.right.querySelector(".action-group-settings"));
            Utility.collapse(JobWizardAbstract.activeJob.html.right.querySelector(".job-settings"));
            JobWizardAbstract.activeActionGroup.job._showHtml();
        }
        else
        {
            this.actionGroup.job._showHtml();
            Utility.expand(this.actionGroup.job.html.right.querySelector(".job-settings"));
        }

        if (this.previousActiveAction){
            this.previousActiveAction._showHtml();
            Utility.expand(this.previousActiveAction.html.right.querySelector(".action-settings"));
            Utility.collapse(this.previousActive.html.right.querySelector(".action-group-settings"));
        }
    }
}