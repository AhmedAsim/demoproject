const caJson = {
	name: "View Designer",
	description: "bla bla",
	viewSource: "",
	editSource: "",
	sections: [
		{
			name: "Section 1",
			description: "section 1 decription sdfgsdfghs",
			isCollapsed: false,
			visibilityFormula: "",
			
			tabs: [
				{
					name: "Tab 111111",
					description: "bla bla",
					isOpen: true,
					visibilityFormula: "",
					
					tabSections: [
						{
							name: "Table 1",
							description: "bla bla",
							isCollapsed: false,
							visibilityFormula: "",
							rowCount: 5,
							columnCount: 3,
							
							viewItems: [
								{
									fieldUid: "",
									formulaTypeName: "",
									formulaName: "",
									label: "",
									description: "",
									visibilityFormula: "",
									viewItemType: "",
									rowIndex: 3,
									columnIndex: 1,
									rowSpan: 2,
									isCollapsed: false,
									fieldName: "",
									customObjectUid: "",
									customObjectName: "",
									
									templateUid: "",
									templateName: "",
									
									fxFieldTypename: "",
									fxFieldTypeUid: "",
									fxFieldFormula: "",
									fxFieldName: "",
								},
								{
									label: "product",
									description: "bla lba",
									visibilityFormula: "",
									
									rowIndex: 0,
									columnIndex: 0,
									rowSpan: 1,
									colSpan: 1,
									
									customObjectUid: "nidmsadfjsbfdsd",
									fieldUid: "b68f5e14-ff43-4ead-9ecc-3e08c83a5fde",
									
									templateUid: "sd,jfbsfdh",
									
									fxFieldTypeUid: "skhdfgjsd",
									fxFieldName: "sjjgfsdmnfsd",
									formula: "s,mdhfvsjdj",
								},
								{
									label: "No",
									description: "bla lba",
									visibilityFormula: "",
									
									rowIndex: 1,
									columnIndex: 0,
									rowSpan: 1,
									colSpan: 2,
									
									customObjectUid: "nidmsadfjsbfdsd",
									fieldUid: "cd286d7c-f1a1-4b92-94c5-f7a07a778479",
									
									templateUid: "sd,jfbsfdh",
									
									fxFieldTypeUid: "skhdfgjsd",
									fxFieldName: "sjjgfsdmnfsd",
									formula: "s,mdhfvsjdj",
								},
							],
						},
						{
							name: "Table 2",
							description: "bla bla",
							isCollapsed: true,
							visibilityFormula: "",
							rowCount: 5,
							columnCount: 3,
							
							viewItems: [
								{
									label: "products",
									description: "bla lba",
									visibilityFormula: "",
									
									rowIndex: 0,
									columnIndex: 0,
									rowSpan: 1,
									colSpan: 1,
									
									customObjectUid: "nidmsadfjsbfdsd",
									fieldUid: ",jsdfb sdkjfbsdfs",
									
									templateUid: "sd,jfbsfdh",
									
									fxFieldTypeUid: "skhdfgjsd",
									fxFieldName: "sjjgfsdmnfsd",
									formula: "s,mdhfvsjdj",
								},
							],
						},
					],
				},
				{
					name: "Tab 2",
					description: "bla bla",
					isOpen: false,
					visibilityFormula: "",
					
					tabSections: [
						{
							name: "Table 1",
							description: "bla bla",
							isCollapsed: true,
							visibilityFormula: "",
							rowCount: 5,
							columnCount: 3,
							
							viewItems: [
								{
									label: "products",
									description: "bla lba",
									visibilityFormula: "",
									
									rowIndex: 0,
									columnIndex: 0,
									rowSpan: 1,
									colSpan: 1,
									
									customObjectUid: "nidmsadfjsbfdsd",
									fieldUid: ",jsdfb sdkjfbsdfs",
									
									templateUid: "sd,jfbsfdh",
									
									fxFieldTypeUid: "skhdfgjsd",
									fxFieldName: "sjjgfsdmnfsd",
									formula: "s,mdhfvsjdj",
								},
							],
						},
					],
				},
				{
					name: "Tab 3",
					description: "bla bla",
					isOpen: false,
					visibilityFormula: "",
					
					tabSections: [
						{
							name: "Table 1",
							description: "bla bla",
							isCollapsed: true,
							visibilityFormula: "",
							rowCount: 5,
							columnCount: 3,
							
							viewItems: [
								{
									label: "products",
									description: "bla lba",
									visibilityFormula: "",
									
									rowIndex: 0,
									columnIndex: 0,
									rowSpan: 1,
									colSpan: 1,
									
									customObjectUid: "nidmsadfjsbfdsd",
									fieldUid: ",jsdfb sdkjfbsdfs",
									
									templateUid: "sd,jfbsfdh",
									
									fxFieldTypeUid: "skhdfgjsd",
									fxFieldName: "sjjgfsdmnfsd",
									formula: "s,mdhfvsjdj",
								},
							],
						},
					],
				},
			],
		},
		{
			name: "Section 2",
			description: "bla bla",
			isCollapsed: false,
			visibilityFormula: "",
			
			tabs: [
				{
					name: "Tab 1",
					description: "bla bla",
					isOpen: true,
					visibilityFormula: "",
					
					tabSections: [
						{
							name: "Table 1",
							description: "bla bla",
							isCollapsed: true,
							visibilityFormula: "",
							rowCount: 5,
							columnCount: 3,
							
							viewItems: [
								{
									label: "products",
									description: "bla lba",
									visibilityFormula: "",
									
									rowIndex: 0,
									columnIndex: 0,
									rowSpan: 1,
									colSpan: 1,
									
									customObjectUid: "nidmsadfjsbfdsd",
									fieldUid: ",jsdfb sdkjfbsdfs",
									
									templateUid: "sd,jfbsfdh",
									
									fxFieldTypeUid: "skhdfgjsd",
									fxFieldName: "sjjgfsdmnfsd",
									formula: "s,mdhfvsjdj",
								},
							],
						},
					],
				},
			],
		},
	],
};