import {Utility} from "../../../../common/js/utility";
import {CaField} from "../centerAreaModels/CaField";

export class LaField {
	constructor($json, $parent, parentClass) {
		this.tabName = $json["formulaName"];
		this.$json = $json;
		this.$parent = $parent;
		this.parentClass = parentClass;
		this.$element = null;
		this.createElement();
		this.$parent.appendChild(this.$element);
		
		let caField = CaField.addedFieldsUid[this.$json.uid];
		console.log(caField);
		if (caField) {
			caField.laField = this;
			this.hide();
		}
		
	}
	
	createElement() {
		this.$element = Utility.createElement("div", {
			class: "la-field",
			draggable: true,
		}, this.tabName);
		
		this.$element.prepend(Utility.createElement("BUTTON", {class: "icon-star-B"}));
		
		this.$element.addEventListener("dragstart", (e) => {
			// e.preventDefault();
			ViewBuilder.draggedField = this;
			console.log("drag start");
		});
		this.$element.addEventListener("dragend", (e) => {
			e.preventDefault();
			ViewBuilder.draggedField = null;
			console.log("drag end");
		});
		
	}
	
	hide() {
		this.$element.classList.add("d-none");
		this.parentClass.changeTabNumber(--this.parentClass.tabNumber);
		
	}
	
	show() {
		this.$element.classList.remove("d-none");
		this.parentClass.changeTabNumber(++this.parentClass.tabNumber);
		
	}
	
	toggleVisibility() {
		this.$element.classList.toggle("d-none");
	}
	
	
}