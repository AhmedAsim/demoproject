import {SettingsFields} from "../../../../common/js/settingsFields";

export class ViewSettingsFields extends SettingsFields {
    isCollapsed() { return super._checkbox('is_collapsed', 'IsCollapsed'); }
    visibilityFormula() { return super._textarea('visibility_formula', 'VisibilityFormula'); }

    ViewFields() {
        return [super.name(), super.description()];
    }

    SectionFields() {
        return [super.name(), super.description(), this.isCollapsed(), this.visibilityFormula()];
    }

    TabFields() {
        return [super.name(), super.description(), super._checkbox('is_open', 'IsOpen'), this.visibilityFormula()];
    }

    TabSectionFields() {
        return [super.name(), super.description(), this.isCollapsed(), this.visibilityFormula(),
        super._number('row_count', 'RowCount', true),
        super._number('column_count', 'ColumnCount', true)];
    }

    ViewItemFields() {
        return [
            super._text('view_item_type', 'ViewItemType', true),
            super._text('label', 'Label'),
            super.description(), this.isCollapsed(), this.visibilityFormula(),
            super._number('row_index', 'RowIndex', true),
            super._number('column_index', 'ColumnIndex', true),
            super._number('row_span', 'RowSpan', true),
            super._number('col_span', 'ColSpan', true)
        ];
    }

    TemplateViewItemFields() {
        return [
            super._text('template', 'TemplateName', true)
        ];
    }

    FieldViewItemFields() {
        return [
            super._text('custom_object', 'CustomObjectName', true),
            super._text('field', 'FieldName', true)
        ];
    }

    FxFieldViewItemFields() {
        return [
            super._text('fx_field_type', 'FxFieldTypeName', true),
            super._text('fx_field_name', 'FxFieldName'),
            super._textarea('fx_field_formula', 'FxFieldFormula')
        ];
    }
}