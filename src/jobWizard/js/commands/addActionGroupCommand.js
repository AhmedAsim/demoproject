import {ActionGroup} from "../actionGroup";
import {Job} from "../job";
import {CommandAbstract} from "../../../common/js/commandAbstract";
import {SelectActionGroupCommand} from "./selectActionGroupCommand";

export class AddActionGroupCommand extends CommandAbstract{

    constructor(job){
        super();
        this.newActionGroup = new ActionGroup(job , Job.getDefaultActionGroup());
        this.leftParent = this.newActionGroup.job.html.left.children[1];
        this.rightParent = this.newActionGroup.job.html.right;
        this.centerParent = this.newActionGroup.job.html.center.children[1];
        this.doo();
        this.addToStack();


    }

    doo(){
        // console.log(this.newActionGroup);
        this.newActionGroup.job.jobObj.jobActionGroups.push(this.newActionGroup.actionGroupObj);
        this.leftParent.appendChild(this.newActionGroup.html.left);
        this.rightParent.appendChild(this.newActionGroup.html.right);
        this.centerParent.appendChild(this.newActionGroup.html.center);


    }

    undo(){

        this.newActionGroup.job.jobObj.jobActionGroups.splice(-1 ,1);
        this.leftParent.removeChild(this.newActionGroup.html.left);
        this.rightParent.removeChild(this.newActionGroup.html.right);
        this.centerParent.removeChild(this.newActionGroup.html.center);


    }
}