import {Utility} from "./utility";

export class Content {
  constructor(contentContainer, isRight, isLeft) {
    this.html = contentContainer;
    this.html.classList.add('cv-content');
    this.html.style.flexGrow = '1';
    this.center = Utility.createElement("div", {"class": "center"});
    this.left = isLeft && Utility.createElement("div", {"class": "left"});
    this.right = isRight && Utility.createElement("div", {"class": "right"});
    this.draw();
    
  }
  
  draw() {
    let transition = "flex-basis 1s";
    let self = this;
    
    //region create elements
  
    if (this.left) {
      let leftDragHandler = Utility.createElement("div", {"id": "left-drag-handler"});
      
      self.html.appendChild(self.left);
      self.html.appendChild(leftDragHandler);
      
      let leftDragHandlerFunc = function (event) {
        if (window.innerWidth - self.right.getBoundingClientRect().width - 260 > event.clientX) {
          self.left.style.flexBasis = event.clientX + "px";
        }
      };
      
      let isLeftHandleClicked=false;
      
      document.addEventListener("mouseup", function () {
        if (isLeftHandleClicked) {
          self.left.style.transition = transition;
          document.removeEventListener("mousemove", leftDragHandlerFunc);
          isLeftHandleClicked = false;
        }
        
      });
      
      leftDragHandler.addEventListener("mousedown", function () {
        if (!self.left.classList.contains("collapse")) {
          self.left.style.transition = "flex-basis 0s";
          document.addEventListener("mousemove", leftDragHandlerFunc);
          isLeftHandleClicked = true;
        }
      });
    }
  
    self.html.appendChild(self.center);
  
  
    if (this.right) {
      
      let rightDragHandler = Utility.createElement("div", {"id": "right-drag-handler"});
      
      self.html.appendChild(rightDragHandler);
      self.html.appendChild(self.right);
      
      let rightDragHandlerFunc = function (event) {

          if ((self.left && self.left.getBoundingClientRect().width) + 260 < event.clientX) {
          self.right.style.flexBasis = (window.innerWidth - event.clientX - 3) + "px";
        }
      };
      
      let isRightHandlerClicked = false;
      
      document.addEventListener("mouseup", function () {
        if (isRightHandlerClicked) {
          self.right.style.transition = transition;
          document.removeEventListener("mousemove", rightDragHandlerFunc);
          isRightHandlerClicked = false;
        }
        
      });
      
      rightDragHandler.addEventListener("mousedown", function () {
        if (!self.right.classList.contains("collapse")) {
          self.right.style.transition = "flex-basis 0s";
          document.addEventListener("mousemove", rightDragHandlerFunc);
          isRightHandlerClicked = true;
        }
      });
    }
    //endregion create elements
  }
}