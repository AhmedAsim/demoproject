const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');


module.exports = {
    entry: {
        jobWizard: "./src/jobWizard/js/app.js",
        viewBuilder: "./src/viewBuilder/js/index.js",
		dataFilter: "./src/dataFilter/js/dataFilter.js",
        commonView: "./src/common/js/commonView.js",
    },
    output: {
        path: path.resolve(__dirname, './dist/'),
        filename: "[name]/[name]-bundle.js"
    },
	// mode: 'production', // Switch for production
	mode: 'development',
	watch: true,
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/env'],
						plugins: [
							'@babel/plugin-proposal-class-properties',
							'@babel/plugin-proposal-object-rest-spread',
							'@babel/plugin-proposal-export-default-from',
							'@babel/plugin-proposal-export-namespace-from',
						],
					},
				},
			}, {
				test: /\.s?css$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
						options: {
							hmr: process.env.WEBPACK_MODE === 'development',
							publicPath: '../',
						}
					},
					{loader: 'css-loader', options: {importLoaders: 1, sourceMap: true}},
					{loader: 'postcss-loader', options: {}},
					{loader: 'sass-loader', options: {sourceMap: true}},
				],
			},
			{
				test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
				use: [{
					loader: 'file-loader',
					options: {
						name: 'fonts/[name].[ext]',
					}
				}]
			}
		]
	},
	optimization: {
		sideEffects: false, // to allow GSAP with production mode
		minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
	},
	plugins: [
		new webpack.SourceMapDevToolPlugin({
			filename: '[file].map',
			exclude: ['/vendor/']
		}),
		new MiniCssExtractPlugin({
			filename: '[name]/[name].css',
			
		}),
	],
	// devtool: 'source-map', // ModySwitch
	devtool: 'cheap-module-eval-source-map',
	devServer: {
		// contentBase: path.resolve(__dirname, './public'),
		historyApiFallback: true,
	}
};
