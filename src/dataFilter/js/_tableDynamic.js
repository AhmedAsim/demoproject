export class TableDynamic {

    constructor(tableId, rowTemplate, onAddRow, onRemovedRow, onChange) {
        this.rowNo = 0;
        this.tableId = tableId;
        this.rowTemplate = rowTemplate;
        this.onAddRow = onAddRow || function () { };
        this.onRemovedRow = onRemovedRow || function () { };
        this.onChange = onChange || function () { };
        document.getElementById(tableId).querySelector(".add-row").addEventListener('click',
            (e) => {
                this.addRow();
            });
    }

    getSelectHtml(name, rowNo, url, parent, selectFirst) {
        //  data-url="${url}"
        // ${parent ? 'data-parent="' + parent + '"' : 'data-set-first-item="True"'}
        return `<div class="select single-select" data-type="text"
                                         id="select-${name}-${rowNo}" name="select-${name}-${rowNo}"
                                         data-name="${name}-${rowNo}" data-name-text="${name}-${rowNo}-text"
                                         ${selectFirst ? 'data-set-first-item="True"' : ''}
                                         data-value="" data-text=""></div>`;
    }

    getElement(selector) {
        return document.querySelector(selector || '#' + this.tableId);
    }

    addRow() {
        ++this.rowNo;
        let template = this.rowTemplate(this);
        let row = document.createElement('tr');
        row.setAttribute('id', this.tableId + '-row-' + this.rowNo);
        row.classList.add('row');
        row.innerHTML = template;
        row.dataset.rowNumber = this.rowNo;
        this.getElement(this.tableId).appendChild(row);
        row.querySelector(".delete").addEventListener('click',
            e => {
                this.removeRow(row.dataset.rowNumber);
            });
        row.querySelectorAll(".changeable").forEach(e => {
            e.addEventListener('change',
                evt => {
                    this.onChange();
                });
        });
        this.updateSeqNo();
        setTimeout(() => {
                this.onAddRow(row, this);
                this.onChange();
            },
            300);
    }

    removeRow(num) {
        let element = this.getElement(this.tableId + '-row-' + num);
        element.parentNode.removeChild(element);
        this.updateSeqNo();
        this.onChange();
    }
    updateSeqNo() {
        this.getElement(this.tableId).querySelectorAll("tr td .seq-no").forEach((item, i) => {
            item.innerHTML = i + 1;
        });
    }

}