import {Utility} from "../../../common/js/utility";

export class ViewDesignerCaAbstract {
	$name;
	raSettingsFieldsObject = {};
	$raSetting;
	$json = {};
	$raHeader;
	
	updateName(newName){
		this.$name.innerHTML = newName;
		this.$raHeader.innerHTML = newName;
	}
	
	createRaSettingsField(fieldObject) {
		let fieldInput;
		let {label, data, readonly, ...otherData} = fieldObject;
		let fieldLabel = this.$raSetting.appendChild(Utility.createElement("LABEL", {"data-translation": label}, label));
		this.raSettingsFieldsObject[label] = fieldLabel;
		data = data[0].toLowerCase()+data.slice(1);
		switch (fieldObject.type) {
			case Utility.SETTINGS_FIELDS_TYPES.textArea:
				fieldInput = Utility.createElement("TEXTAREA", otherData, this.$json[data]);
				fieldInput.readOnly = readonly;
				fieldInput.disabled = readonly;
				break;
			case Utility.SETTINGS_FIELDS_TYPES.checkbox:
				this.raSettingsFieldsObject[label].classList.add("checkbox");
				fieldInput = Utility.createElement("INPUT", {checked: this.$json[data], ...otherData});
				fieldInput.readOnly = readonly;
				fieldInput.disabled = readonly;
				break;
			default :
				fieldInput = Utility.createElement("INPUT", {name:label,value: this.$json[data], ...otherData});
				fieldInput.readOnly = readonly;
				fieldInput.disabled = readonly;
				break;
		}
		
		fieldInput.addEventListener("input", ()=> this.$json[data] = fieldInput.type !== "checkbox" ? fieldInput.value : fieldInput.checked);
		label =label.toLowerCase();
		if (label==='name'||label==='label'){
			fieldInput.addEventListener("input", ()=> this.updateName(fieldInput.value));
		}
		fieldLabel.appendChild(fieldInput);
		
	}
	
}