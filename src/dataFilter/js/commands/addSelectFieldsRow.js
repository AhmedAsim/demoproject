import {SelectFieldsRow} from "../TablesRowTemp/selectFieldsRow";

export class AddSelectFieldsRow {
    constructor(customObjects , parentTable){
        this.parentTable = parentTable;
        this.newSelectFieldsRow = new SelectFieldsRow(customObjects , parentTable);
    }

    doo() {
        console.log("uuuu");
        this.parentTable.table.appendChild(this.newSelectFieldsRow.html.selectFieldsRow);
    }

    undo(){
        this.parentTable.table.removeChild(this.newSelectFieldsRow.html.selectFieldsRow);
    }
}