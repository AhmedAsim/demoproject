import {Utility} from "./utility";

export class Header {

    constructor(headerContainer, navObj, appName) {
        this.navObj = navObj.item;
        this.html = headerContainer;
        this.navItemsWrapper = "";
        this.appName = appName;
        this._onResize = [];
        this._onLoad = [];
        this._nav = "";
        this._search = "";
        this._dropdownBackground = "";
        this.draw();
    }

    draw() {
        let self = this;
        let headerWrapper = Utility.createElement("div", {"class": "header-wrapper"});
        self.html.appendChild(headerWrapper);

        //region Draw logo
        headerWrapper.appendChild(Utility.createElement("div", {"class": "site-logo icon-cubes-B"}));
        //endregion Draw logo

        //region Application Name
        if (self.appName)
            headerWrapper.appendChild(Utility.createElement("h2", {}, self.appName));

        //endregion Application Name

        //region NavBar

        //region Elements creation
        let leftButton = Utility.createElement("button", {"class": "left-nav-arrow icon-chevron-left-B"});
        let nav = Utility.createElement("nav");
        self._nav = nav;
        let rightButton = Utility.createElement("button", {"class": "right-nav-arrow icon-chevron-right-B"});
        headerWrapper.appendChild(leftButton);
        headerWrapper.appendChild(nav);
        headerWrapper.appendChild(rightButton);
        let navItemsWrapper = Utility.createElement("div", {"class": "nav-items-wrapper"});
        nav.appendChild(navItemsWrapper);
        self.navItemsWrapper = navItemsWrapper;
        for (let i = 0; i < self.navObj.items.length; i++) {
            // console.log("hello items only no drop menu");
            self.addItemToNav(self.navObj.items[i]);
        }

        let dropDownBackground = Utility.createElement("div",{"class":"dropdwon-background"});
        self._dropdownBackground = dropDownBackground;
        self.html.appendChild(self._dropdownBackground);
        for (let i = 0; i < self.navObj.groups.length; i++) {
            // console.log("drop menu");
            self.addItemToNav(self.navObj.groups[i]);
        }
        //endregion Elements creation

        //region Behaviour
        let borderWidth = 0;
        let arrowsHandler = function () {
            if (nav.getBoundingClientRect().width - 2 * borderWidth < navItemsWrapper.offsetWidth - 4 ) {
                leftButton.classList.add("active");
                rightButton.classList.add("active");
                nav.classList.add("active");
            } else {
                leftButton.classList.remove("active");
                rightButton.classList.remove("active");
                nav.classList.remove("active");
                navItemsWrapper.style.transform = "translateX(0)";
            }

        };
        self._onLoad.push(arrowsHandler);
        self._onResize.push(arrowsHandler);
        self._onLoad.push(() => navItemsWrapper.style.transform = "translateX(0)");

        let translateStep = 70;
        rightButton.addEventListener("click", function () {
            let maxTranslate = navItemsWrapper.offsetWidth - nav.getBoundingClientRect().width + 2 * borderWidth;
            let currentTranslate = parseInt(navItemsWrapper.style.transform.match(/[0-9]+/)[0]);
            navItemsWrapper.style.transform = "translateX(" + ((currentTranslate + translateStep) > maxTranslate ? -1 * maxTranslate : -1 * (currentTranslate + translateStep)) + "px)";
        });
        leftButton.addEventListener("click", function () {
            let currentTranslate = parseInt(navItemsWrapper.style.transform.match(/[0-9]+/)[0]);
            navItemsWrapper.style.transform = "translateX(" + ((currentTranslate - translateStep) < 0 ? 0 : -1 * (currentTranslate - translateStep)) + "px)";
        });
        nav.addEventListener("wheel", function (event) {
            let currentTranslate = parseInt(navItemsWrapper.style.transform.match(/[0-9]+/)[0]);
            let maxTranslate = navItemsWrapper.offsetWidth - nav.getBoundingClientRect().width + 2 * borderWidth;
            if (Math.abs(event.deltaX) > Math.abs(event.deltaY)) {
                let newTranslate = currentTranslate + 0.5 * event.deltaX;
                if (newTranslate > maxTranslate) {
                    newTranslate = maxTranslate;
                }
                if (newTranslate < 0) {
                    newTranslate = 0;
                }
                navItemsWrapper.style.transform = "translateX(" + (-1 * newTranslate) + "px)";
            } else {
                let newTranslate = currentTranslate + 0.5 * event.deltaY;
                if (newTranslate > maxTranslate) {
                    newTranslate = maxTranslate;
                }
                if (newTranslate < 0) {
                    newTranslate = 0;
                }
                navItemsWrapper.style.transform = "translateX(" + (-1 * newTranslate) + "px)";
            }
        });

        dropDownBackground.addEventListener("click", function () {
           Header.currentActiveMenu.classList.remove("active");
           Header.currentActiveItem.classList.remove("active");
           this.classList.remove("active");
            Header.currentActiveMenu = "";
            Header.currentActiveItem = "";
        });

        //endregion

        //endregion NavBar

        //region Search
        let searchWrapper = Utility.createElement("span", {"class": "search icon-search-B"});
        let search = Utility.createElement("input", {"type": "text", "placeholder": "search"});
        self._search = search;
        searchWrapper.appendChild(search);
        headerWrapper.appendChild(searchWrapper);
        //endregion Search


    }

    onResize() {
        for (let i = 0; i < this._onResize.length; i++) {
            this._onResize[i]();
        }
    }

    onLoad() {
        for (let i = 0; i < this._onLoad.length; i++) {
            this._onLoad[i]();
        }
    }

    addItemToNav(itemObj) {

        let self = this;

        //region Create nav item
        let navItem = Utility.createElement("a",
            {
                "href": (itemObj.url ? itemObj.url : (itemObj.items ? "#" + itemObj.name.replace(/ +/g, "") : "")),
                "class": (itemObj.iconClass ? "icon-" + itemObj.iconClass + "-B " : "") + (itemObj.items || itemObj.groups ? "icon-angle-down-A" : ""),
                "data-uid": itemObj.uid,
                "target": (itemObj.isOpenInNewTab ? "_blank" : "")
            }, itemObj.name);
        if(itemObj.items || itemObj.groups)
        {
            if (itemObj.items.length || itemObj.groups.length){
                self.navItemsWrapper.appendChild(navItem);
            }
        }
        else {
            self.navItemsWrapper.appendChild(navItem);
        }

        //endregion Create nav item

        //region Creat dropdown menu
        if (itemObj.items) {
            if (Header._isNested(itemObj)) {
                // console.log("helloo neseted dropdown ");
                self.html.appendChild(self._createNestedDropdown(itemObj, itemObj.name.replace(/ +/g, "")));
            } else {
                self.html.appendChild(Header._createDropdown(itemObj.items, itemObj.name.replace(/ +/g, "")));
            }
        }
        //endregion Creat dropdown menu

        navItem.addEventListener("click", function (event) {
            let hrefValue = navItem.getAttribute("href");
            if (hrefValue[0] === "#") {
                let targetMenu = document.getElementById(hrefValue.substr(1));
                event.stopPropagation();
                event.preventDefault();
                if (Header.currentActiveMenu === targetMenu) {
                    Header.currentActiveMenu = "";
                    Header.currentActiveItem = "";
                    targetMenu.classList.remove("active");
                    navItem.classList.remove("active");
                    self._dropdownBackground.classList.remove("active");

                } else {
                    if (Header.currentActiveMenu) {
                        Header.currentActiveMenu.classList.remove("active");
                        Header.currentActiveItem.classList.remove("active");
                        self._dropdownBackground.classList.add("active");

                    }
                    Header.currentActiveMenu = targetMenu;
                    Header.currentActiveItem = navItem;
                    targetMenu.classList.add("active");
                    navItem.classList.add("active");
                    self._dropdownBackground.classList.add("active");
                    if (targetMenu.classList.contains("dropdown")) {
                        targetMenu.style.left = Utility.offset(navItem).left + "px";
                    }

                    // else {
                    //     targetMenu.getElementsByTagName("span")[0].style.left = Utility.offset(navItem).left + "px";
                    // }
                }
            }
        })

    }

    static _isNested(itemObj) {
        return (itemObj.groups.length > 0);
    }

    static _createDropdown(items, id) {
        let dropdown = Utility.createElement("div", {"class": "dropdown", "id": id});
        for (let i = 0; i < items.length; i++) {
            dropdown.appendChild(Utility.createElement("a", {
                "href": items[i].url,
                "data-uid": items[i].uid,
                "class": items[i].isSeparator ? "separator" : (items[i].iconClass ? "icon-" + items[i].iconClass + "-B" : ""),
                "target": (items[i].isOpenInNewTab ? "_blank" : "")
            }, items[i].isSeparator ? "" : items[i].name));
        }
        return dropdown;
    }

    _createNestedDropdown(itemObj, id) {
        let self = this;
        let dropDown = Utility.createElement("div", {"class": "wide-dropdown", "id": id});
        let wideDropDownFunction = function () {
            dropDown.style.left = Utility.offset(self._nav).left + "px";
            dropDown.style.width = Utility.offset(self._search).left - Utility.offset(self._nav).left + "px";
        };
        self._onLoad.push(wideDropDownFunction);
        self._onResize.push(wideDropDownFunction);

        let menuFilterWrapper = Utility.createElement("div", {"class": "menu-filter"});
        let menuFilter = Utility.createElement("input", {"type": "text", "placeholder": "filter"});
        menuFilterWrapper.appendChild(menuFilter);
        let filterClearBtn = Utility.createElement("div", {"class": "filter-clear-btn icon-times-B"});
        menuFilterWrapper.appendChild(filterClearBtn);
        dropDown.appendChild(menuFilterWrapper);
        let groups = Utility.createElement("div", {"class": "groups"});

        for (let i = 0; i < itemObj.groups.length; i++) {

                groups.appendChild(Header._createGroup(itemObj.groups[i]));

        }

        for (let i = 0; i < itemObj.items.length; i++) {
            groups.appendChild(Header._createGroup(itemObj.items[i]));
        }
        dropDown.appendChild(groups);

        //region filter Behaviour
        let menuItems = dropDown.querySelectorAll(".group-item:not(.separator)");
        let oldMatched = [];
        menuFilter.addEventListener("keyup",function () {
            let value = this.value.trim();
            if(value){
                dropDown.classList.add("filtering-mode");
                filterClearBtn.classList.add("active");
                for(let matchedElement of oldMatched){
                    matchedElement.classList.remove("matched");
                }
                for (let item of menuItems){
                    if(item.textContent.includes(value)){
                        oldMatched.push(item);
                        item.classList.add("matched");
                        let parent = item.closest(".group");
                        while(parent){
                            if(parent.classList.contains("matched"))
                                break;
                            parent.classList.add("matched");
                            oldMatched.push(parent);
                            parent = parent.closest(".group");
                        }
                    }
                }
            }else{
                dropDown.classList.remove("filtering-mode");
                filterClearBtn.classList.remove("active");
            }
        });
        filterClearBtn.addEventListener("click", () => {
           filterClearBtn.classList.remove("active");
           dropDown.classList.remove("filtering-mode");
           menuFilter.value = "";
           for(let matchedElement of oldMatched){
               matchedElement.classList.remove("matched");
           }
        });

        //endregion


        return dropDown;


    }

    static _createGroup(item) {
        let group = Utility.createElement("div", {"class": "group"});
        if (item.items || item.groups) {
            let groupHeader = Utility.createElement("h4", {"class": item.iconClass ? "icon-" + item.iconClass + "-A" : ""}, item.name);
            group.appendChild(groupHeader);
            for (let i = 0; i < item.items.length; i++) {
                if (item.items[i].items) {
                    group.appendChild(Header._createGroup(item.items[i]));
                } else {
                    group.appendChild(Utility.createElement("a", {
                        "class": item.items[i].isSeparator ? "group-item separator" : ("group-item " + (item.items[i].iconClass ? "icon-" + item.items[i].iconClass + "-B" : "")),
                        "href": item.items[i].url,
                        "target": (item.items[i].isOpenInNewTab ? "_blank" : "")
                    }, item.items[i].isSeparator ? "" : item.items[i].name))
                }
            }

            for (let i = 0; i < item.groups.length; i++) {
                if (item.groups[i].groups) {
                    group.appendChild(Header._createGroup(item.groups[i]));
                } else {
                    group.appendChild(Utility.createElement("a", {
                        "class": "group-item " + (item.groups[i].iconClass ? "icon-" + item.groups[i].iconClass + "-B" : ""),
                        "href": item.groups[i].url,
                        "target": (item.groups[i].isOpenInNewTab ? "_blank" : "")
                    }, item.groups[i].name))
                }
            }
        } else {

            let groupItem = Utility.createElement("a", {"class": item.isSeparator ? "group-item single-group-item separator" : ("group-item single-group-item " + (item.iconClass ? "icon-" + item.iconClass + "-B" : "")),
                "href": item.url,
                "data-uid": item.uid,
                "target": (item.isOpenInNewTab ? "_blank" : "")
            }, item.isSeparator ? "" : item.name);
            group.appendChild(groupItem);
        }
        return group;
    }

}

Header.currentActiveMenu = "";
Header.currentActiveItem = "";
Header.widemenustart = "";
Header.widemenuend = "";
